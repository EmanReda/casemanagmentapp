import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AssetModel } from './asset-type.model';
import { AssetTypeService } from './asset-type.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
    selector: 'app-asset-type',
    templateUrl: './asset-type.component.html'
})
export class AssetTypeComponent {
    assetOperation: boolean;
    assetTypes: any;
    modalRef: BsModalRef;
    search: any;
    model: AssetModel;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private assetTypeSer: AssetTypeService,
        private modalService: BsModalService) {
        this.getAll();
    }

    getAll() {
        this.assetTypeSer.GetAll()
            .subscribe((response) => {
                this.assetTypes = response;
            }, () => {
                this.global.notificationMessage(4);
            });
    }

    openModal(template: any) {
        this.assetOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }

    saveAsset() {
        this.assetTypeSer.Operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.assetOperation = false;
                        this.model = new AssetModel();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.assetTypeSer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.assetOperation = false;
                        this.model = new AssetModel();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    operation(data?) {
        this.model = (data != null) ?
            new AssetModel(data.assetTypeId, data.name, data.notes, data.isDeleted) :
            new AssetModel();
        this.assetOperation = true;
    }
}
