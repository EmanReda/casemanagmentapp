export class AssetModel {
  constructor(
    public assetTypeId: number = null,
    public name: string = null,
    public notes: string = null,
    public isDeleted: boolean = null
  ) { }
}
