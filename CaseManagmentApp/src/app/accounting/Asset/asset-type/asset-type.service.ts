import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AssetTypeService {

  constructor(private http: HttpClient) {}

  GetAll(): Observable<any> {
    return this.http.get('AssetType/GetAll/');
  }

  Operation(data): Observable<any> {
    return this.http.post('AssetType/Operation' , data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete('AssetType/delete/' + id);
  }

}
