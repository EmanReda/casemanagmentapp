import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AssetService } from './asset.service';
import { AssetModel, FilterAsset } from './asset.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { BsLocaleService } from 'ngx-bootstrap';
@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html'
})
export class AssetComponent {
  searchModel: FilterAsset;
  treeType: any;
  model: AssetModel;
  checkCode = false;
  assets: any;
  modalRef: BsModalRef;
  total: number;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public assetSer: AssetService,
    private translate: TranslateService,
    private modalService: BsModalService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'Asset.PageHeader', url: '/accounting/asset' }
      ],
      pageHeader: 'Asset.PageHeader'
    };
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterAsset();
    this.getAll();
    this.treeType = TreeType;
  }

  openModal(template: TemplateRef<any>, item: AssetModel) {
    this.model = new AssetModel();
    if (item != null) {
      this.model = new AssetModel(item.assetId, item.moduleTreeId, item.assetTypeId, item.code, item.name,item.purchaseDate, item.notes);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAll() {
    this.assetSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.assets = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkAssetCode(code) {
    if (!(code == null || code == '')) {
      this.assetSer.CheckAssetCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, () => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveAsset(): void {
    this.model.purchaseDate = this.global.formatDate(this.model.purchaseDate);
    this.assetSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteAsset(id) {
    this.assetSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
