export class AssetModel {
    constructor(
        public assetId: number = null,
        public moduleTreeId: number = null,
        public assetTypeId: number = null,
        public code: string = null,
        public name: string = null,
        public purchaseDate:any=null,
        public notes: string = null) {
    }
}

export class FilterAsset {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public name: any = null,
        public assetTypeId: number = 0,
        public moduleTreeId: number = 0) {
    }
}
