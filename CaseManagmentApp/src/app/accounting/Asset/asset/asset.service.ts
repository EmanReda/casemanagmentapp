import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AssetService {

  constructor(private http: HttpClient) { }

  CheckAssetCode(Code): any {
    return this.http.get('Asset/CheckAssetCode?Code=' + Code);
  }

  getAll(model): Observable<any> {
    return this.http.post(`Asset/GetAll`, model);
  }
  Save(model): any {
    return this.http.post('Asset/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('Asset/Delete/' + id);
  }
}
