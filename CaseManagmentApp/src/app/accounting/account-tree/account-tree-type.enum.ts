export enum AccountTreeType {
    Assets = 1,
    Liabilities = 2,
    Revenue = 3,
    Expenses = 4
}
