import { Component, ViewChild } from '@angular/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AccountTree } from './account-tree.model';
import { AccountTreeService } from './account-tree.service';
import deepEqual from 'deep-equal';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-account-tree',
  templateUrl: './account-tree.component.html'
})
export class AccountTreeComponent {
  treeType: any;
  model: AccountTree;
  shadowModel: AccountTree;
  checkCode = false;
  tempData: any;
  refreshTree: boolean;
  @ViewChild('confirmChanges') confirmChanges: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private translate: TranslateService,
    private accountTreeSer: AccountTreeService) {
    this.accountTreeSer.AccountTreeType();
    this.model = new AccountTree();
    this.shadowModel = new AccountTree();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'AccountTree.PageHeader', url: '/accounting/general-ledger/account-tree' }
      ],
      pageHeader: 'AccountTree.PageHeader'
    };
    this.treeType = TreeType;
  }

  treeChanges(data?, skip?: boolean) {
    if (deepEqual(this.model, this.shadowModel) || skip) {
      this.model = (data != null) ? this.generateModel(data) : new AccountTree();
      this.shadowModel = (data != null) ? this.generateModel(data) : new AccountTree();
    } else {
      this.confirmChanges.show();
      this.tempData = data;
    }
  }

  generateModel(data: AccountTree) {
    return new AccountTree(data.accountTreeId, data.name, data.code, data.accountTreeParentId,
      data.levelNo, data.accountTreeTypeId, data.finish, data.hasAssistant, data.hasCostCenter,
      data.cash, data.isDeleted);
  }

  accountParentChange(data) {
    let preventedParent = [];
    if (this.model.accountTreeId != null) {
      preventedParent = this.global.treeChild(this.fixed.accounting.AccountTree, 'accountTreeId',
        'accountTreeParentId', [this.model.accountTreeId]);
    }
    if (data == null) {
      this.model.accountTreeParentId = null;
      this.model.levelNo = 1;
    } else if (this.model.accountTreeId != null && preventedParent.indexOf(data.accountTreeId) > -1) {
      this.model.accountTreeParentId = undefined;
      this.model.levelNo = undefined;
      this.translate.get(`AccountTree.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.accountTreeParentId = this.shadowModel.accountTreeParentId;
        this.model.levelNo = this.shadowModel.levelNo;
      }, 50);
    } else {
      this.model.accountTreeParentId = data.accountTreeId;
      this.model.accountTreeTypeId = data.accountTreeTypeId;
      this.model.levelNo = data.levelNo + 1;
    }
  }

  checkIfHaveChild() {
    const child = this.fixed.accounting.AccountTree.filter(s => s.accountTreeParentId == this.model.accountTreeId);
    if (child.length > 0) {
      this.translate.get(`AccountTree.Cann'tCheckHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.cash = false;
        this.model.finish = false;
        this.model.hasAssistant = false;
        this.model.hasCostCenter = false;
      }, 50);
    }
  }

  checkAccountCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.accountTreeSer.CheckAccountCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveAccountTree() {
    this.accountTreeSer.Save(this.model)
      .subscribe(
        (response) => {
          if (response.type === ResponseEnum.Success) {
            this.refreshTree = !this.refreshTree;
            this.model = this.generateModel(response.data);
            this.shadowModel = this.generateModel(response.data);
          }
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }, (error) => {
          this.global.notificationMessage(4);
        });
  }

  deleteAccountTree() {
    const chlidern = this.fixed.accounting.AccountTree.filter(s => s.accountTreeParentId === this.model.accountTreeId);
    if (chlidern.length === 0) {
      this.accountTreeSer.Delete(this.model.accountTreeId)
        .subscribe(
          (response) => {
            if (response.type === ResponseEnum.Success) {
              this.refreshTree = !this.refreshTree;
              this.model = new AccountTree();
              this.shadowModel = new AccountTree();
            }
            if (response.name == null) {
              this.global.notificationMessage(response.type);
            } else {
              this.translate.get(response.name)
                .subscribe(
                  (val) => {
                    this.global.notificationMessage(response.type, null, val);
                  });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    } else {
      this.translate.get(`AccountTree.Cann'tDeleteHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
    }
  }
}
