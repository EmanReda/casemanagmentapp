export class AccountTree {
    constructor(
        public accountTreeId: number = null,
        public name: string = null,
        public code: string = null,
        public accountTreeParentId: number = null,
        public levelNo: number = 1,
        public accountTreeTypeId: number = null,
        public finish: boolean = false,
        public hasAssistant: boolean = false,
        public hasCostCenter: boolean = false,
        public cash: boolean = false,
        public isDeleted: boolean = false
    ) { }
}
