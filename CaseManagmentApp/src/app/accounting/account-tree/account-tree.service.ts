import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';

@Injectable({ providedIn: 'root' })
export class AccountTreeService {

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`AccountTree/GetAll`);
  }

  AccountTreeType() {
    if (this.fixed.accounting.AccountTreeType.length === 0) {
      this.http.get('AccountTree/TreeTypes')
        .subscribe(
          (data) => {
            this.fixed.accounting.AccountTreeType = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  CheckAccountCode(Code): any {
    return this.http.get('AccountTree/CheckAccountCode?Code=' + Code);
  }

  Save(model): any {
    return this.http.post('AccountTree/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('AccountTree/Delete/' + id);
  }
}
