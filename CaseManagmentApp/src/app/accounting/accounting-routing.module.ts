import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountTreeComponent } from './account-tree/account-tree.component';
import { ModuleTreeComponent } from './module-tree/module-tree.component';
import { CostCenterComponent } from './cost-center/cost-center.component';
import { FinantialYearComponent } from './finantial-year/finantial-year.component';
import { BanksComponent } from './banks/banks.component';
import { AssetComponent } from './asset/asset/asset.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { TransactionOperationComponent } from './transaction/transaction-operation/transaction-operation.component';
import { AccountingSettingComponent } from './accounting-setting/accounting-setting.component';
import { ReportComponent } from './report/report.component';
const routes: Routes = [
  { path: 'general-ledger/account-tree', component: AccountTreeComponent },
  { path: 'general-ledger/module-tree', component: ModuleTreeComponent },
  { path: 'general-ledger/cost-center', component: CostCenterComponent },
  { path: 'general-ledger/finantial', component: FinantialYearComponent },
  { path: 'banks', component: BanksComponent },
  { path: 'asset', component: AssetComponent },
  { path: 'general-ledger/transaction-list', component: TransactionListComponent },
  { path: 'general-ledger/transaction-operation', component: TransactionOperationComponent },
  { path: 'general-ledger/transaction-operation/:id', component: TransactionOperationComponent },
  { path: 'general-ledger/setting', component: AccountingSettingComponent },
  { path: 'report', component: ReportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }
