export class AccountingSettingModel {
    constructor(
        public accountingSettingId: number = null,
        public employeeModuleTreeId: number = null,
        public clientModuleTreeId: number = null,
        public vendorModuleTreeId: number = null,
        public assetModuleTreeId: number = null,
        public departmentModuleTreeId: number = null,
        public currencyId: number = null,
        public trueData: boolean = false) { }
}
