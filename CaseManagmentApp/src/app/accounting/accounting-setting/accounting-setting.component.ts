import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { AccountingSettingService } from './accounting-setting.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { AccountingSettingModel } from './account-setting.model';

@Component({
  selector: 'app-accounting-setting',
  templateUrl: './accounting-setting.component.html'
})
export class AccountingSettingComponent {
  treeType: any;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public accountingSettingSer: AccountingSettingService,
    private translate: TranslateService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'Accounting-setting.PageHeader', url: '/AccountSetting/setting' }
      ],
      pageHeader: 'Accounting-setting.PageHeader'
    };
    this.accountingSettingSer.getAccountingSetting();
    this.treeType = TreeType;
  }

  saveSetting() {
    this.accountingSettingSer.Save(this.fixed.accounting.AccountingSetting)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.fixed.accounting.AccountingSetting = new AccountingSettingModel();
          this.accountingSettingSer.getAccountingSetting();
          this.global.notificationMessage(1);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  reset() {
    this.fixed.accounting.AccountingSetting = new AccountingSettingModel();
    this.accountingSettingSer.getAccountingSetting();
  }
}
