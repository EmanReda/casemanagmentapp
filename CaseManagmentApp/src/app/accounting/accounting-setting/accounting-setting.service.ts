import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { AccountingSettingModel } from './account-setting.model';

@Injectable({ providedIn: 'root' })
export class AccountingSettingService {

  constructor(
    private http: HttpClient,
    public fixed: FixedService) { }

  getAccountingSetting() {
    if (!this.fixed.accounting.AccountingSetting.trueData) {
      this.http.get(`AccountingSetting/GetData`)
        .subscribe((response: any) => {
          this.fixed.accounting.AccountingSetting = new AccountingSettingModel(response.accountingSettingId,
            response.employeeModuleTreeId, response.clientModuleTreeId, response.vendorModuleTreeId,
            response.assetModuleTreeId,response.departmentModuleTreeId, response.currencyId, true);
        });
    }
  }

  Save(model): Observable<any> {
    return this.http.post('AccountingSetting/Operation', model);
  }
}
