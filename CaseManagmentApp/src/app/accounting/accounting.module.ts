import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingRoutingModule } from './accounting-routing.module';
import { FinantialYearComponent } from './finantial-year/finantial-year.component';
import { CurrencyComponent } from './currency/currency.component';
import { SharedModule } from '../shared/shared.module';
import { AccountTreeComponent } from './account-tree/account-tree.component';
import { ModuleTreeComponent } from './module-tree/module-tree.component';
import { CostCenterComponent } from './cost-center/cost-center.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AssetTypeComponent } from './asset/asset-type/asset-type.component';
import { TransactionTypeComponent } from './transaction/transaction-type/transaction-type.component';
import { BanksComponent } from './banks/banks.component';
import { AssetComponent } from './asset/asset/asset.component';
import { TransactionListComponent } from './transaction/transaction-list/transaction-list.component';
import { TransactionOperationComponent } from './transaction/transaction-operation/transaction-operation.component';
import { AccountingSettingComponent } from './accounting-setting/accounting-setting.component';
import { ReportComponent } from './report/report.component';
@NgModule({
  declarations: [
    FinantialYearComponent,
    CurrencyComponent,
    AccountTreeComponent,
    ModuleTreeComponent,
    CostCenterComponent,
    AssetTypeComponent,
    TransactionTypeComponent,
    BanksComponent,
    AssetComponent,
    TransactionListComponent,
    TransactionOperationComponent,
    AccountingSettingComponent,
    ReportComponent
  ],
  imports: [
    CommonModule,
    AccountingRoutingModule,
    SharedModule.forRoot(),
    ReactiveFormsModule
  ],
  exports: [
    TransactionListComponent
  ]
})
export class AccountingModule { }
