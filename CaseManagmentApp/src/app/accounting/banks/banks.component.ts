import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { BanksService } from './banks.service';
import { BankModel, BankAccountModel } from './banks.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html'
})
export class BanksComponent {
  model: BankModel;
  shadowModel: BankModel;
  checkCode = false;
  banks: any;
  modalRef: BsModalRef;
  lockGridEdit = false;
  bankAccount = new BankAccountModel();

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public banksSer: BanksService,
    private translate: TranslateService,
    private modalService: BsModalService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'Banks.PageHeader', url: '/accounting/banks' }
      ],
      pageHeader: 'Banks.PageHeader'
    };
    this.getAll();
  }

  openModal(template: TemplateRef<any>, item?) {
    this.model = new BankModel();
    if (item != null) {
      item.acC_BankAccount.forEach(element => {
        element.operation = 's';
      });
      this.model = new BankModel(item.bankId, item.code, item.name, item.notes, item.acC_BankAccount);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAll() {
    this.banksSer.getAll()
      .subscribe((response) => {
        this.banks = response;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkBankCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.banksSer.CheckBankCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveBank(): void {
    this.banksSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteBank(id) {
    this.banksSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  accountOperation(data?: BankAccountModel) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.aCC_BankAccount == null) {
        this.model.aCC_BankAccount = [];
      }
      if (data == null) {
        this.model.aCC_BankAccount.push(new BankAccountModel());
      } else {
        this.bankAccount = new BankAccountModel(data.bankAccountId,
          data.bankId, data.accountTreeId, data.accountName, data.accountCode, data.isDeleted, 's');
        data.operation = 'e';
      }
    } else {
      this.translate.get('Banks.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  accountFinish(type, data, index?) {
    if (type === 'close') {
      data = this.bankAccount;
      this.lockGridEdit = false;
      this.model.aCC_BankAccount[index] = this.bankAccount;
    } else {
      if (data.accountTreeId == null || data.accountName == null || data.accountName == ''
        || data.accountCode == null || data.accountCode == '') {
        this.translate.get('Banks.CompletePeriod')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
      } else {
        this.lockGridEdit = false;
        data.operation = 's';
      }
    }
  }

  deleteAccount(item, index) {
    if (item.bankAccountId > 0) {
      item.isDeleted = true;
    } else {
      this.model.aCC_BankAccount.splice(index, 1);
    }
  }
}
