export class BankModel {
    constructor(
        public bankId: number = null,
        public code: string = null,
        public name: string = null,
        public notes: string = null,
        public aCC_BankAccount: BankAccountModel[] = []) { }
}

export class BankAccountModel {
    constructor(
        public bankAccountId: number = 0,
        public bankId: number = null,
        public accountTreeId: number = null,
        public accountName: string = null,
        public accountCode: string = null,
        public isDeleted: boolean = false,
        public operation: string = 'e') { }
}
