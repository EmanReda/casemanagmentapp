import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class BanksService {

  constructor(private http: HttpClient) { }

  CheckBankCode(Code): any {
    return this.http.get('Bank/CheckBankCode?Code=' + Code);
  }

  getAll(): Observable<any> {
    return this.http.get(`Bank/GetAll`);
  }
  Save(model): any {
    return this.http.post('Bank/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('Bank/Delete/' + id);
  }
}
