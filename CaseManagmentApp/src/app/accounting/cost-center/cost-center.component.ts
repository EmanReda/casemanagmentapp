import { Component, ViewChild } from '@angular/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { CostCenterTree } from './cost-center.model';
import { CostCenterService } from './cost-center.service';
import deepEqual from 'deep-equal';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-cost-center',
  templateUrl: './cost-center.component.html'
})

export class CostCenterComponent {
  treeType: any;
  model: CostCenterTree;
  shadowModel: CostCenterTree;
  checkCode = false;
  tempData: any;
  refreshTree: boolean;
  @ViewChild('confirmChanges') confirmChanges: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private translate: TranslateService,
    private centerTreeSer: CostCenterService) {
    this.model = new CostCenterTree();
    this.shadowModel = new CostCenterTree();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'CostCenterTree.PageHeader', url: '/accounting/general-ledger/cost-center' }
      ],
      pageHeader: 'CostCenterTree.PageHeader'
    };
    this.treeType = TreeType;
  }

  treeChanges(data?, skip?: boolean) {
    if (deepEqual(this.model, this.shadowModel) || skip) {
      this.model = (data != null) ? this.generateModel(data) : new CostCenterTree();
      this.shadowModel = (data != null) ? this.generateModel(data) : new CostCenterTree();
    } else if (data.costCenterId === this.model.costCenterId) {
      this.model.costCenterParentId = null;
      this.translate.get(`CostCenterTree.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.costCenterParentId = this.shadowModel.costCenterParentId;
      }, 100);
    } else {
      this.confirmChanges.show();
      this.tempData = data;
    }
  }

  generateModel(data: CostCenterTree) {
    return new CostCenterTree(data.costCenterId, data.code, data.name, data.costCenterParentId, data.notes);
  }

  centerParentChange(data) {
    let preventedParent = [];
    if (this.model.costCenterId != null) {
      preventedParent = this.global.treeChild(this.fixed.accounting.CostCenter, 'costCenterId',
        'costCenterParentId', [this.model.costCenterId]);
    }
    if (data == null) {
      this.model.costCenterParentId = null;
    } else if (this.model.costCenterId != null && preventedParent.indexOf(data.costCenterId) > -1) {
      this.model.costCenterParentId = undefined;
      this.translate.get(`CostCenterTree.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.costCenterParentId = this.shadowModel.costCenterParentId;
      }, 50);
    } else {
      this.model.costCenterParentId = data.costCenterId;
    }
  }

  checkCostCenterCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.centerTreeSer.CheckCostCenterCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveCostCenterTree() {
    this.centerTreeSer.Save(this.model)
      .subscribe(
        (response) => {
          if (response.type === ResponseEnum.Success) {
            this.refreshTree = !this.refreshTree;
            this.model = this.generateModel(response.data);
            this.shadowModel = this.generateModel(response.data);
          }
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }, (error) => {
          this.global.notificationMessage(4);
        });
  }

  deleteCostCenterTree() {
    const chlidern = this.fixed.accounting.CostCenter.filter(s => s.costCenterParentId === this.model.costCenterId);
    if (chlidern.length === 0) {
      this.centerTreeSer.Delete(this.model.costCenterId)
        .subscribe(
          (response) => {
            if (response.type === ResponseEnum.Success) {
              this.refreshTree = !this.refreshTree;
              this.model = new CostCenterTree();
              this.shadowModel = new CostCenterTree();
            }
            if (response.name == null) {
              this.global.notificationMessage(response.type);
            } else {
              this.translate.get(response.name)
                .subscribe(
                  (val) => {
                    this.global.notificationMessage(response.type, null, val);
                  });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    } else {
      this.translate.get(`CostCenterTree.Cann'tDeleteHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
    }
  }

}
