export class CostCenterTree {
    constructor(
        public costCenterId: number = null,
        public code: string = null,
        public name: string = null,
        public costCenterParentId: number = null,
        public notes: string = null,
    ) { }
}
