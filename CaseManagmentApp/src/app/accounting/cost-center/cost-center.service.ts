import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class CostCenterService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`CostCenter/GetAll`);
  }

  CheckCostCenterCode(Code): any {
    return this.http.get('CostCenter/CheckCostCenterCode?Code=' + Code);
  }

  Save(model): any {
    return this.http.post('CostCenter/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('CostCenter/Delete/' + id);
  }
}
