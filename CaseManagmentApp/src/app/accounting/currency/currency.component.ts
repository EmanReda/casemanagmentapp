import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { CurrencyService } from './currency.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html'
})
export class CurrencyComponent implements OnInit {
  @Input() selectedValue: boolean;
  @Output() selectedValueChanges = new EventEmitter<any>();
  currency: any;

  constructor(
    public fixed: FixedService,
    private currencySer: CurrencyService) { }

  ngOnInit() {
    this.currencySer.GetCurrencyList()
      .subscribe((currency) => {
        this.currency = currency;
      });
  }
}
