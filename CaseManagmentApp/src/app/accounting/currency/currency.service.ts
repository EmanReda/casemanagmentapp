import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FixedService } from 'src/app/shared/services/fixed.service';

@Injectable({ providedIn: 'root' })
export class CurrencyService {

  constructor(
    public fixed: FixedService,
    private http: HttpClient) { }

  GetCurrencyList(): Observable<any> {
    return this.http.get('Currency/GetAll/');
  }
}
