import { Component, TemplateRef } from '@angular/core';
import { FinantialYear, AccountingPeriod } from './finantial-year.model';
import { FinantialYearService } from './finantial-year.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-finantial-year',
  templateUrl: './finantial-year.component.html'
})
export class FinantialYearComponent {
  finantialYears: any;
  selectedModel: FinantialYear;
  model = new FinantialYear();
  accountingPeriod = new AccountingPeriod();
  lockGridEdit = false;
  modalRef: BsModalRef;

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private finantialYearSer: FinantialYearService,
    private modalService: BsModalService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'FinantialYear.PageHeader', url: '/accounting/general-ledger/finantial' }
      ],
      pageHeader: 'FinantialYear.PageHeader'
    };
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.getAllData();
  }

  periodOperation(data?: AccountingPeriod) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.aCC_AccountingPeriod == null) {
        this.model.aCC_AccountingPeriod = [];
      }
      if (data == null) {
        this.model.aCC_AccountingPeriod.push(new AccountingPeriod());
      } else {
        this.accountingPeriod = new AccountingPeriod(data.accountingPeriodId,
          data.finantialYearId, data.startDate, data.endDate, data.status, data.isDeleted, 's');
        data.operation = 'e';
      }
    } else {
      this.translate.get('FinantialYear.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  periodFinish(type, data, index?) {
    if (type === 'close') {
      data = this.accountingPeriod;
      this.lockGridEdit = false;
      this.model.aCC_AccountingPeriod[index] = this.accountingPeriod;
    } else {
      if (data.endDate == null || data.endDate === 'Invalid Date' ||
        data.startDate == null || data.startDate === 'Invalid Date') {
        this.translate.get('FinantialYear.CompletePeriod')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
      } else {
        this.lockGridEdit = false;
        data.operation = 's';
      }
    }
  }

  deletePeriod(item, index) {
    if (item.accountingPeriodId > 0) {
      item.isDeleted = true;
    } else {
      this.model.aCC_AccountingPeriod.splice(index, 1);
    }
  }

  openModal(template: TemplateRef<any>, item: FinantialYear) {
    this.model = new FinantialYear();
    if (item != null) {
      item.aCC_AccountingPeriod.forEach(element => {
        element.operation = 's';
      });
      this.model = new FinantialYear(item.finantialYearId, item.companyId, item.year,
        item.startDate, item.endDate, item.status, item.isDeleted, item.aCC_AccountingPeriod);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAllData(): void {
    this.finantialYearSer.getAll()
      .subscribe((response) => {
        response.forEach(ele => {
          ele.aCC_AccountingPeriod = ele.acC_AccountingPeriod;
        });
        this.finantialYears = response;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  saveFinantialYear(): void {
    this.model.startDate = this.global.formatDate(this.model.startDate);
    this.model.endDate = this.global.formatDate(this.model.endDate);
    this.model.aCC_AccountingPeriod.forEach(element => {
      element.startDate = this.global.formatDate(element.startDate);
      element.endDate = this.global.formatDate(element.endDate);
    });
    this.finantialYearSer.saveFinantialYear(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAllData();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteFinantialYear(id) {
    this.finantialYearSer.deleteFinantialYear(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAllData();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
