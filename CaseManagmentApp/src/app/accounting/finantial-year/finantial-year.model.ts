export class FinantialYear {
   constructor(
      public finantialYearId: number = null,
      public companyId: number = null,
      public year: number = null,
      public startDate: any = null,
      public endDate: any = null,
      public status: boolean = false,
      public isDeleted: boolean = false,
      public aCC_AccountingPeriod: any = [],
   ) { }
}

export class AccountingPeriod {
   constructor(
      public accountingPeriodId: number = null,
      public finantialYearId: number = null,
      public startDate: any = null,
      public endDate: any = null,
      public status: boolean = false,
      public isDeleted: boolean = false,
      public operation: string = 'e'
   ) { }
}
