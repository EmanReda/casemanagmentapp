import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class FinantialYearService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`FinantialYear/GetAll`);
  }

  saveFinantialYear(model: any): Observable<any> {
    return this.http.post(`FinantialYear/Post`, model);
  }

  deleteFinantialYear(id): Observable<any> {
    return this.http.delete(`FinantialYear/Delete/${id}`);
  }
}
