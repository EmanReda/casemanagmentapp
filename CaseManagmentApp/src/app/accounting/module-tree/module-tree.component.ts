import { Component, ViewChild } from '@angular/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ModuleTree, ModuleAccountModel } from './module-tree.model';
import { ModuleTreeService } from './module-tree.service';
import deepEqual from 'deep-equal';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-module-tree',
  templateUrl: './module-tree.component.html'
})
export class ModuleTreeComponent {
  treeType: any;
  model: ModuleTree;
  shadowModel: ModuleTree;
  checkCode = false;
  tempData: any;
  refreshTree: boolean;
  @ViewChild('confirmChanges') confirmChanges: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private translate: TranslateService,
    private moduleTreeSer: ModuleTreeService) {
    this.model = new ModuleTree();
    this.shadowModel = new ModuleTree();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'ModuleTree.PageHeader', url: '/accounting/general-ledger/module-tree' }
      ],
      pageHeader: 'ModuleTree.PageHeader'
    };
    this.treeType = TreeType;
  }

  treeChanges(data?, skip?: boolean) {
    if (deepEqual(this.model, this.shadowModel) || skip) {
      this.model = (data != null) ? this.generateModel(data) : new ModuleTree();
      this.shadowModel = (data != null) ? this.generateModel(data) : new ModuleTree();
    } else if (data.moduleTreeId === this.model.moduleTreeId) {
      this.model.moduleParentId = null;
      this.translate.get(`ModuleTree.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.moduleParentId = this.shadowModel.moduleParentId;
      }, 50);
    } else {
      this.confirmChanges.show();
      this.tempData = data;
    }
  }

  checkIfHaveChild() {
    const child = this.fixed.accounting.ModuleTree.filter(s => s.moduleParentId == this.model.moduleTreeId);
    if (child.length > 0) {
      this.translate.get(`ModuleTree.Cann'tCheckHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.final = false;
      }, 50);
    }
  }

  generateModel(data) {
    const list = [];
    data.acC_ModuleAccount.forEach((value) => {
      list.push(value.accountTreeId);
    });
    data.tempModuleAccount = list;
    return new ModuleTree(data.moduleTreeId, data.moduleParentId,
      data.code, data.name, data.final, data.notes, data.tempModuleAccount, data.acC_ModuleAccount);
  }

  moduleParentChange(data) {
    let preventedParent = [];
    if (this.model.moduleTreeId != null) {
      preventedParent = this.global.treeChild(this.fixed.accounting.AccountTree, 'moduleTreeId',
        'moduleParentId', [this.model.moduleTreeId]);
    }
    if (data == null) {
      this.model.moduleParentId = null;
    } else if (this.model.moduleTreeId != null && preventedParent.indexOf(data.moduleTreeId) > -1) {
      this.model.moduleParentId = undefined;
      this.translate.get(`ModuleTree.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.moduleParentId = this.shadowModel.moduleParentId;
      }, 500);
    } else {
      this.model.moduleParentId = data.moduleTreeId;
      const list = [];
      data.acC_ModuleAccount.forEach((value) => {
        list.push(value.accountTreeId);
      });
      this.model.tempModuleAccount = list;
    }
  }

  checkModuleCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.moduleTreeSer.CheckModuleCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveModuleTree() {
    this.moduleTreeSer.Save(this.model)
      .subscribe(
        (response) => {
          this.refreshTree = !this.refreshTree;
          this.model = this.generateModel(response.data);
          this.shadowModel = this.generateModel(response.data);
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }, (error) => {
          this.global.notificationMessage(4);
        });
  }

  deleteModuleTree() {
    const chlidern = this.fixed.accounting.ModuleTree.filter(s => s.moduleParentId == this.model.moduleTreeId);
    if (chlidern.length == 0) {
      this.moduleTreeSer.Delete(this.model.moduleTreeId)
        .subscribe(
          (response) => {
            if (response.type === ResponseEnum.Success) {
              this.refreshTree = !this.refreshTree;
              this.model = new ModuleTree();
              this.shadowModel = new ModuleTree();
            }
            if (response.name == null) {
              this.global.notificationMessage(response.type);
            } else {
              this.translate.get(response.name)
                .subscribe(
                  (val) => {
                    this.global.notificationMessage(response.type, null, val);
                  });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    } else {
      this.translate.get(`ModuleTree.Cann'tDeleteHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
    }
  }

  selectedAccounts(data) {
    this.model.tempModuleAccount = data;
    data.forEach((value) => {
      const check = this.model.aCC_ModuleAccount.filter(s => s.accountTreeId === value)[0];
      if (check == null) {
        const account = new ModuleAccountModel();
        account.accountTreeId = value;
        this.model.aCC_ModuleAccount.push(account);
      }
    });
    for (let i = 0; i < this.model.aCC_ModuleAccount.length; i++) {
      const remove = this.model.tempModuleAccount.filter(s => s == this.model.aCC_ModuleAccount[i].accountTreeId)[0];
      if (remove == null) {
        (this.model.aCC_ModuleAccount[i].moduleAccountId === 0) ?
          this.model.aCC_ModuleAccount.splice(i, 1) :
          this.model.aCC_ModuleAccount[i].isDeleted = true;
      }
    }
  }
}
