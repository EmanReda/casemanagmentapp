export class  ModuleTree {
    constructor(
        public moduleTreeId: number = null,
        public moduleParentId: number = null,
        public code: string = null,
        public name: string = null,
        public final: boolean=false,
        public notes: string = null,
        public tempModuleAccount: any = [],
        public aCC_ModuleAccount: ModuleAccountModel[] = []
    ) { }
}

export class ModuleAccountModel {
    constructor(
        public moduleAccountId: number = 0,
        public accountTreeId: number = null,
        public moduleTreeId: number = null,
        public isDeleted: boolean = false) { }
}
