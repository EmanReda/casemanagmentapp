import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class ModuleTreeService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`ModuleTree/GetAll`);
  }

  CheckModuleCode(Code): any {
    return this.http.get('ModuleTree/CheckModuleCode?Code=' + Code);
  }

  Save(model): any {
    return this.http.post('ModuleTree/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('ModuleTree/Delete/' + id);
  }
}
