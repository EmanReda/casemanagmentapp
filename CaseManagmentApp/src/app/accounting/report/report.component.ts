import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { ReportService } from './report.service';
import { FilterReportModel } from './report.model';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html'
})

export class ReportComponent {
  model = new FilterReportModel();
  reportTypes: any[];
  url: any;
  treeType: any;

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private reportSer: ReportService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Report', url: null },
        { name: 'Report.PageHeader', url: '/accounting/report' }
      ],
      pageHeader: 'Report.PageHeader'
    };
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.treeType = TreeType;
    this.initializeReportTypes();
  }
  initializeReportTypes() {
    this.reportTypes = [
      { Id: 1, ENname: 'StatementOfAccount', ARname: 'كشف حساب' },
      { Id: 2, ENname: 'ModuleBalance', ARname: 'حساب ارصدة الحسابات المساعدة' },
      { Id: 3, ENname: 'CheckList', ARname: 'ميزان المراجعة' },
      { Id: 4, ENname: 'CostCenterList', ARname: 'قائمة المركز المالى' },
      { Id: 5, ENname: 'IncomeList', ARname: 'قائمة الدخل' }
    ];
  }

  exportReport() {
    this.model.fromDate = this.global.formatDate(this.model.fromDate);
    this.model.toDate = this.global.formatDate(this.model.toDate);
    if (this.model.reportType === 1) {
      this.url = 'AccountingReport/StatementOfAccount';
    } else if (this.model.reportType === 3) {
      this.url = 'AccountingReport/CheckList';
    } else if (this.model.reportType === 4) {
      this.url = 'AccountingReport/CostCenterList';
    } else if (this.model.reportType === 5) {
      this.url = 'AccountingReport/IncomeList';
    }
    this.reportSer.Report(this.model, this.url)
      .subscribe((response) => {
        this.global.notificationMessage(1);
      }, (error) => {
        this.global.notificationMessage(4);
      });
  }

  Reset() {
    this.model = new FilterReportModel();
  }
}
