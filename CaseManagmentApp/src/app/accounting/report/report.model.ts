export class FilterReportModel{
    constructor(
        public reportType: number =null,
        public fromDate: any = null,
        public toDate: any = null, 
        public accountTreeId :number =null,
        public moduleTreeId :number =null, 
        public language :string ="en"
    ){}
    }
