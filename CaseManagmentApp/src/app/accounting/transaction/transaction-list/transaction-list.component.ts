import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FilterTransaction } from './transaction-list.model';
import { TransactionService } from '../transaction.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html'
})

export class TransactionListComponent {
  searchModel: FilterTransaction;
  transactions: any;
  total: number;
  cases: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public transactionSer: TransactionService,
    private translate: TranslateService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'Transaction.TransactionList', url: '/accounting/general-ledger/transaction-list' }
      ],
      pageHeader: 'Transaction.TransactionList'
    };
    this.transactionSer.getTransactionCase();
    this.searchModel = new FilterTransaction();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.dateFrom = this.global.formatDate(this.searchModel.dateFrom);
    this.searchModel.dateTo = this.global.formatDate(this.searchModel.dateTo);
    this.transactionSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.transactions = response.data;
        this.transactions.forEach(element => {
          element.year = new Date(element.date).getFullYear();
        });
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  deleteTransaction(id) {
    this.transactionSer.deleteTransction(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  resetSearch() {
    this.searchModel = new FilterTransaction();
    this.total = undefined;
    this.transactions = undefined;
  }
}
