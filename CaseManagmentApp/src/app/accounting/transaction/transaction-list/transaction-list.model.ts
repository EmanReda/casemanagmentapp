export class FilterTransaction {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public transactionTypeId: number = null,
        public currencyId: number = null,
        public transactionCaseId: number = 0,
        public dateFrom: any = null,
        public dateTo: any = null) {
    }
}
