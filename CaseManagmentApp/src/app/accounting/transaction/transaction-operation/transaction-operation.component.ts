import { Component, OnInit } from '@angular/core';
import { TransactionModel, TransactionsDetails } from '../transaction.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TransactionService } from '../transaction.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { TransactionCase } from 'src/app/shared/enums/transaction-case.enum';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-transaction-operation',
  templateUrl: './transaction-operation.component.html'
})

export class TransactionOperationComponent implements OnInit {
  model: TransactionModel;
  sub: any;
  lockGridEdit = false;
  transactionsDetails: TransactionsDetails;
  treeType: any;
  checkDate = false;
  savedTransactionNo: any;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private translate: TranslateService,
    private transactionSer: TransactionService,
    private localeService: BsLocaleService) {
    this.transactionSer.getTransactionCase();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Accounting', url: null },
        { name: 'Transaction.TransactionList', url: '/accounting/general-ledger/transaction-list' }
      ],
      pageHeader: ''
    };
    this.model = new TransactionModel();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.treeType = TreeType;
  }

  ngOnInit() {
    this.sub = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Transaction.Add', url: '/accounting/general-ledger/transaction-operation' });
          this.fixed.subHeader.pageHeader = 'Transaction.Add';
          this.model.transactionId = null;
          this.model.currencyId = this.fixed.accounting.AccountingSetting.currencyId;
        } else {
          this.fixed.subHeader.data.push({ name: 'Transaction.Edit', url: '/accounting/general-ledger/transaction-operation/' + id });
          this.fixed.subHeader.pageHeader = 'Transaction.Edit';
          this.model.transactionId = Number(id);
          this.getTransaction(Number(id));
        }
      });
  }

  getTransaction(id: number) {
    this.transactionSer.getTransactionById(id)
      .subscribe((Response) => {
        this.model = Response;
        this.model.acC_TransactionsDetails.forEach(element => {
          element.operation = 's';
          if (element.moduleTreeId != null) {
            const event = { moduleTreeId: element.moduleTreeId, acC_ModuleAccount: element.acC_ModuleAccount };
            this.moduleTreeChange(event, element);
          }
        });
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  moduleTreeChange(event, item, from?) {
    item.moduleTreeId = (event == null) ? null : event.moduleTreeId;
    const list = [];
    if (event != null) {
      event.acC_ModuleAccount.forEach(element => {
        list.push(element.accountTreeId);
      });
    }
    item.accountTreeList = (event == null) ? undefined : list;
    if (from === 'html') {
      item.accountTreeId = null;
    }
  }

  getTransactionNo(data) {
    if (data != null) {
      if (this.model.transactionId == null) {
        this.transactionSer.getTransactionNo(this.global.formatDate(data))
          .subscribe((response) => {
            this.model.transactionNo = response;
            this.model.showTransactionNo = response.substring(6) + ' - ' + response.substring(0, 4);
          }, () => {
            this.global.notificationMessage(4);
          });
      } else {
        this.model.showTransactionNo = this.model.transactionNo.substring(6) + ' - ' + this.model.transactionNo.substring(0, 4);
      }
      this.transactionSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  detailsOperation(data?: TransactionsDetails) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.acC_TransactionsDetails == null) {
        this.model.acC_TransactionsDetails = [];
      }
      if (data == null) {
        this.model.acC_TransactionsDetails.push(new TransactionsDetails());
      } else {
        this.transactionsDetails = new TransactionsDetails(data.transactionsDetailsId,
          data.transactionId, data.accountTreeId, data.moduleTreeId, data.debit, data.credit, data.costCenterId, data.note,
          data.isDeleted, 's');
        data.operation = 'e';
      }
    } else {
      this.translate.get('Transaction.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  detailsFinish(type, data, index?) {
    if (type === 'close') {
      (data.transactionsDetailsId == null) ?
        this.deleteDetails(data, index) :
        this.model.acC_TransactionsDetails[index] = this.transactionsDetails;
      this.lockGridEdit = false;
    } else {
      if (data.accountTreeId == null || (data.credit === 0 && data.debit === 0)) {
        this.translate.get('Transaction.CompleteDetails')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
      } else {
        this.lockGridEdit = false;
        data.operation = 's';
        if (type === 'save&new') {
          this.detailsOperation();
        }
      }
    }
  }

  saveTransaction() {
    this.model.date = this.global.formatDate(this.model.date);
    this.transactionSer.saveTransaction(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteTransaction(id) {
    this.transactionSer.deleteTransction(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  sumDebit(type) {
    if (type === 'debit') {
      this.model.totalDepit = 0;
      this.model.acC_TransactionsDetails.forEach(element => {
        this.model.totalDepit += element.debit;
      });
    } else {
      this.model.totalCredit = 0;
      this.model.acC_TransactionsDetails.forEach(element => {
        this.model.totalCredit += element.credit;
      });
    }
    (this.model.totalDepit != this.model.totalCredit) ?
      this.model.transactionCaseId = TransactionCase.Temporary :
      this.model.transactionCaseId = TransactionCase.Unposted;
  }

  deleteDetails(data, index) {
    if (data.transactionsDetailsId > 0) {
      data.isDeleted = true;
    } else {
      this.model.acC_TransactionsDetails.splice(index, 1);
    }
  }
}
