import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TransactionTypeModel } from './transaction-type.model';
import { TransactionTypeService } from './transaction-type.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
    selector: 'app-transaction-type',
    templateUrl: './transaction-type.component.html'
})
export class TransactionTypeComponent {
    transactionOperation: boolean;
    transactionTypes: any;
    modalRef: BsModalRef;
    search: any;
    model: TransactionTypeModel;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private transactionTypeSer: TransactionTypeService,
        private modalService: BsModalService) {
        this.getAll();
    }

    getAll() {
        this.transactionTypeSer.GetAll()
            .subscribe((response) => {
                this.transactionTypes = response;
            }, () => {
                this.global.notificationMessage(4);
            });
    }

    openModal(template: any) {
        this.transactionOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    }

    saveTransactionType() {
        this.transactionTypeSer.Operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.transactionOperation = false;
                        this.model = new TransactionTypeModel();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.transactionTypeSer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.transactionOperation = false;
                        this.model = new TransactionTypeModel();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    operation(data?) {
        this.model = (data != null) ?
            new TransactionTypeModel(data.transactionTypeId, data.nameAr, data.nameEn, data.isDeleted) :
            new TransactionTypeModel();
        this.transactionOperation = true;
    }
}
