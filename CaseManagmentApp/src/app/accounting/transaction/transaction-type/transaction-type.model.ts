export class TransactionTypeModel {
    constructor(
      public transactionTypeId: number = null,
      public nameAr: string = null,
      public nameEn: string = null,
      public isDeleted: boolean = null
    ) { }
  }
