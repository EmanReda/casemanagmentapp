import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class TransactionTypeService {

  constructor(private http: HttpClient) {}

  GetAll(): Observable<any> {
    return this.http.get('TransactionType/GetAll/');
  }

  Operation(data): Observable<any> {
    return this.http.post('TransactionType/Operation' , data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete('TransactionType/delete/' + id);
  }

}
