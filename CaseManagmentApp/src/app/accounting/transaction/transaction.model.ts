export class TransactionModel {
    constructor(
        public transactionId: number = null,
        public transactionTypeId: number = null,
        public currencyId: number = null,
        public date: any = null,
        public transactionNo: string = null,
        public transactionCaseId: number = 2,
        public totalDepit: number = 0,
        public totalCredit: number = 0,
        public showTransactionNo: string = null,
        public acC_TransactionsDetails: any = []) {
    }
}

export class TransactionsDetails {
    constructor(
        public transactionsDetailsId: number = null,
        public transactionId: number = null,
        public accountTreeId: number = null,
        public moduleTreeId: number = null,
        public debit: number = 0,
        public credit: number = 0,
        public costCenterId: number = null,
        public note: string = null,
        public isDeleted: boolean = false,
        public operation: string = 'e') {
    }
}
