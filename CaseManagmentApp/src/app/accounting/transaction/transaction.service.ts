import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';

@Injectable({ providedIn: 'root' })
export class TransactionService {

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private http: HttpClient) { }

  getAll(model): Observable<any> {
    return this.http.post(`Transaction/GetAll`, model);
  }

  getTransactionNo(date): Observable<any> {
    return this.http.get('Transaction/getTransactionNo?date=' + date);
  }

  getTransactionCase() {
    if (this.fixed.accounting.TransactionCase.length === 0) {
      this.http.get('Transaction/GetTransactionCase')
        .subscribe(
          (data) => {
            this.fixed.accounting.TransactionCase = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  getTransactionById(id: number): any {
    return this.http.get(`Transaction/GetTransactionById/${id}`);
  }

  saveTransaction(model: any): Observable<any> {
    return this.http.post(`Transaction/Operation`, model);
  }

  deleteTransction(id): Observable<any> {
    return this.http.delete(`Transaction/Delete/${id}`);
  }

  CheckTransactionDate(date): Observable<any> {
    return this.http.get(`Transaction/CheckTransactionDate?date=` + date);
  }
}
