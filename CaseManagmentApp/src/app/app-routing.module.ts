import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './shared/theme/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './shared/theme/simple-layout/simple-layout.component';
export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'full'
    },
    children: [
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'security',
        loadChildren: './security/security.module#SecurityModule'
      },
      {
        path: 'accounting',
        loadChildren: './accounting/accounting.module#AccountingModule'
      },
      {
        path: 'vendor',
        loadChildren: './vendor/vendor.module#VendorModule'
      },
      {
        path: 'client',
        loadChildren: './client/client.module#ClientModule'
      },
      {
        path: 'hr',
        loadChildren: './hr/hr.module#HrModule'
      },
      {
        path: 'store',
        loadChildren: './store/store.module#StoreModule'
      },
      {
        path: 'maintenance',
        loadChildren: './maintenance/maintenance.module#MaintenanceModule'
      },
      {
        path: 'case',
        loadChildren: './case/case.module#CaseModule'
      }
    ]
  }
  ,
  {
    path: '',
    component: SimpleLayoutComponent,
    data: {
      title: 'simple'
    },
    children: [
      {
        path: '',
        loadChildren: './login/login.module#LoginModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
