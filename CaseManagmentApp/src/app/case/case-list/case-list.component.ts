import { BsLocaleService } from 'ngx-bootstrap';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FilterCaseList } from './case-list.model';
import { FixedService } from '../../shared/services/fixed.service';
import { GlobalService } from '../../shared/services/global.service';
import { CaseService } from '../case.service';

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.css']
})
export class CaseListComponent {
  searchModel: FilterCaseList;
  data: any;
  CaseTypes : any;
  constructor(public fixed: FixedService,
    public global: GlobalService,
    public CaseSer: CaseService,
    private localeService: BsLocaleService)
    { 
      this.fixed.subHeader = {
        display: true,
        data: [
          { name: 'Menu.Case', url: null },
          { name: 'Case.List', url: '/case/list' }
        ],
        pageHeader: 'Case.List'
      };
      this.searchModel = new FilterCaseList();
      this.getAll();
      this.getCaseTypes();
    }
    getAll() {
      this.searchModel.Date = this.global.formatDate(this.searchModel.Date);
      this.CaseSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.data = response.data;
        }, (err) => {
          this.global.notificationMessage(4);
        })
    }
    getCaseTypes() {
      this.CaseTypes = [
        { caseTypeId: 1, nameAr:'حماية',nameEn:'Safeguard'},
        { caseTypeId: 2, nameAr:'دعم مالي',nameEn:'Subsidy'},
        { caseTypeId: 2, nameAr:'مكافحة الإغراق',nameEn:'Anti-Dumping'},
      ];
      // this.CaseSer.getCaseTypes()
      // .subscribe((response) => {
      //   this.CaseTypes = response.data;
      //   }, (err) => {
      //     this.global.notificationMessage(4);
      //   })
    }
    resetSearch() {
      this.searchModel = new FilterCaseList();
      this.data = undefined;
    }

}
