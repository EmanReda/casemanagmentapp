export class FilterCaseList {
    constructor(
        public CaseId: number = 1, //
        public page: number = 1, //
        public recordPerPage: number = 20, //
        public NameAr: string = null, //
        public NameEn: string = null, //
        public Date: any = null, //
        public CaseStatus: boolean = null, //
        public CaseStageId: number = null, //
        public CaseTypeId: number = null, //
        public DomesticIndustryId: number = null,
        public ActionTakenId: number = null, //
        public GovernmentId: number = null, //
        
        ) { }
}