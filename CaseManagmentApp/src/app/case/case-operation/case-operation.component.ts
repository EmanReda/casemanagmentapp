import { Component, OnInit, ViewChild } from '@angular/core';
import { CaseOperationModel } from './case-operation.model';
import { SwalComponent } from '../../../../node_modules/@sweetalert2/ngx-sweetalert2';
import { FixedService } from '../../shared/services/fixed.service';
import { GlobalService } from '../../shared/services/global.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { CaseOperationService } from './case-operation.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-case-operation',
  templateUrl: './case-operation.component.html',
  styleUrls: ['./case-operation.component.css']
})
export class CaseOperationComponent  {
  model: CaseOperationModel;
  shadowModel: CaseOperationModel;
  checkcode = false;
  tempData: any;
  refreshTree: boolean;
  @ViewChild('confirmChanges') confirmChanges: SwalComponent;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private translate: TranslateService,
    private CaseOperationSer: CaseOperationService) {
    this.model = new CaseOperationModel();
    this.shadowModel = new CaseOperationModel();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Case', url: null },
        { name: 'Case.PageHeader', url: '/Case/Operation' }
      ],
      pageHeader: 'Case.PageHeader'
    };
  }
  generateModel(data: CaseOperationModel) {
    return new CaseOperationModel(data.caseId, data.nameAr, data.nameEn, data.date, data.caseStatus,
                                   data.caseStageId,data.caseTypeId,data.domesticIndustryId,data.actionTakenId,data.governmentId);
  }
  saveCategoryTree() {
    this.CaseOperationSer.operation(this.model)
      .subscribe(
        (response) => {
          if (response.type === ResponseEnum.Success) {
            this.refreshTree = !this.refreshTree;
            this.model = this.generateModel(response.data);
            this.shadowModel = this.generateModel(response.data);
          }
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }, (error) => {
          this.global.notificationMessage(4);
        });
  }
  deleteCategoryTree(id) {
    const chlidern = this.fixed.store.CategoryTree.filter(s => s.categoryParentId === this.model.caseId);
    if (chlidern.length === 0) {
      this.CaseOperationSer.delete(this.model.caseId)
        .subscribe(
          (response) => {
            if (response.type === ResponseEnum.Success) {
              this.refreshTree = !this.refreshTree;
              this.model = new CaseOperationModel();
              this.shadowModel = new CaseOperationModel();
            }
            if (response.name == null) {
              this.global.notificationMessage(response.type);
            } else {
              this.translate.get(response.name)
                .subscribe(
                  (val) => {
                    this.global.notificationMessage(response.type, null, val);
                  });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    } else {
      this.translate.get(`Category.Cann'tDeleteHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
    }
  }

  treeChanges()
  {}

}
