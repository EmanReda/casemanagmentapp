export class CaseOperationModel{
    constructor(
        public caseId: number = 1, //
        public nameAr: string = null, //
        public nameEn: string = null, //
        public date: any = null, //
        public caseStatus: boolean = null, //
        public caseStageId: number = null, //
        public caseTypeId: number = null, //
        public domesticIndustryId: number = null,
        public actionTakenId: number = null, //
        public governmentId: number = null, //
    ){}
}