import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CaseOperationService {
  constructor(private http: HttpClient) { }
  getAll(): Observable<any> {
      return this.http.get('Category/GetAll');
  }
  operation(model): Observable<any> {
      return this.http.post('Category/Operation', model);
  }
  delete(id: number): Observable<any> {
      return this.http.delete('Category/Delete/' + id);
  }
}
