import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaseListComponent } from './case-list/case-list.component';
import { CaseOperationComponent } from './case-operation/case-operation.component';

const routes: Routes = [
  {path:'list',component:CaseListComponent},
  {path:'operation',component:CaseOperationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseRoutingModule { }
