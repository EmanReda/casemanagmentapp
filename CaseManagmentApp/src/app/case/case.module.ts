import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseRoutingModule } from './case-routing.module';
import { LookupsModule } from '../lookups/lookups.module';
import { SharedModule } from '../shared/shared.module';
import { CaseListComponent } from './case-list/case-list.component';
import { CaseOperationComponent } from './case-operation/case-operation.component';


@NgModule({
  declarations: [
    CaseListComponent,
    CaseOperationComponent
  ],
  imports: [
    CommonModule,
    CaseRoutingModule,
    LookupsModule,
    SharedModule.forRoot(),
  ]
})
export class CaseModule { }
