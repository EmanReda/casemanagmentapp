import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CaseService {
  constructor(private http: HttpClient) { }

  getAll(model): Observable<any> {
    return this.http.post("Case/GetAll", model);
  }
  getCaseTypes(): Observable<any> {
    return this.http.get("Case/GetCaseTypes");
  }

  // getOrderById(id: number): Observable<any> {
  //   return this.http.get(`MaintenanceOrder/getOrderById/${id}`);
  // }
  // getTransactionNo(date): Observable<any> {
  //   return this.http.get('MaintenanceOrder/getTransactionNo?date=' + date);
  // }
  // save(model):Observable<any>{
  //   return this.http.post("MaintenanceOrder/Operation",model);
  // }
  // void(id): Observable<any> {
  //   return this.http.delete(`MaintenanceOrder/Voided/${id}`);
  // }
  // PrintReceipt(id): any {
  //   return this.http.get('MaintenanceOrder/DocumentReport/' + id);
  // }
}
