import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FilterClient , ClientModel } from '../client/client.model';
import { ClientService } from '../client/client.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
@Component({
  selector: 'app-client-shared',
  templateUrl: './client-shared.component.html'
})
export class ClientSharedComponent {
  list: any = [];
  modalRef: BsModalRef;
  search: any;
  model: FilterClient;
  clientOperation: boolean;
  checkCode = false;
  myModel : ClientModel;
  @Input() selectedValue: any;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    public clientSer: ClientService,
    private modalService: BsModalService) {
    this.model = new FilterClient();
    this.getAll();
  }

  getAll() {
    this.clientSer.getAll(this.model)
      .subscribe((response) => {
        if (response.data.length > 0) {
          this.list=[];
          this.list = response.data;
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  checkClientCode(code) {
    if (!(code == null || code == '')) {
      this.clientSer.CheckClientCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }
  openModal(template: any) {
    this.clientOperation = false;
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  searchFunction(term) {
    this.model.name = term;
    this.model.page = 1;
    this.list =[];
    this.getAll();
  }

  customSearchFn(term: string, item: any) {
    return item;
  }

  outputFunction(item) {
    this.selectedValueChanges.emit(item);
  }

  operation(data?) {
    this.myModel = (data != null) ?
        new ClientModel(data.clientId, data.moduleTreeId, data.clientTypeId, data.code,
                        data.name , data.priceListId , data.notes):
        new ClientModel();
    this.clientOperation = true;
}

save() {
  this.clientSer.Save(this.myModel)
      .subscribe(
          (response) => {
              if (response.type === ResponseEnum.Success) {
                  this.clientOperation = false;
                  this.myModel = new ClientModel();
                  this.getAll();
              }
              if (response.name == null) {
                  this.global.notificationMessage(response.type);
              } else {
                  this.translate.get(response.name)
                      .subscribe(
                          (val) => {
                              this.global.notificationMessage(response.type, null, val);
                          });
              }
          }, (error) => {
              this.global.notificationMessage(4);
          });
}

delete(id) {
  this.clientSer.Delete(id)
      .subscribe(
          (response) => {
              if (response.type === ResponseEnum.Success) {
                  this.clientOperation = false;
                  this.myModel = new ClientModel();
                  this.getAll();
              }
              if (response.name == null) {
                  this.global.notificationMessage(response.type);
              } else {
                  this.translate.get(response.name)
                      .subscribe(
                          (val) => {
                              this.global.notificationMessage(response.type, null, val);
                          });
              }
          }, (error) => {
              this.global.notificationMessage(4);
          });
}

}
