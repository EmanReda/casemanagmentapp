import { Component,Input,Output,EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ClientModel } from './client-type.model';
import { ClientTypeService } from './client-type.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-client-type',
  templateUrl: './client-type.component.html'
})

export class ClientTypeComponent  {
  clientOperation: boolean;
  clientTypes: any;
  modalRef: BsModalRef;
  search: any;
  model: ClientModel;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private translate: TranslateService,
      private clientTypeSer: ClientTypeService,
      private modalService: BsModalService) {
      this.getAll();
  }

  getAll() {
      this.clientTypeSer.GetAll()
          .subscribe((response) => {
              this.clientTypes = response;
          }, () => {
              this.global.notificationMessage(4);
          });
  }

  openModal(template: any) {
      this.clientOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  saveClientType() {
      this.clientTypeSer.Operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.clientOperation = false;
                      this.model = new ClientModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  delete(id) {
      this.clientTypeSer.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.clientOperation = false;
                      this.model = new ClientModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  operation(data?) {
      this.model = (data != null) ?
          new ClientModel (data.clientTypeId, data.name, data.notes, data.isDeleted) :
          new ClientModel();
      this.clientOperation = true;
  }

}
