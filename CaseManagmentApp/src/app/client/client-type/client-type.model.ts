export class ClientModel {
    constructor(
      public clientTypeId: number = null,
      public name: string = null,
      public notes: string = null,
      public isDeleted: boolean = null
    ) { }
  }
  