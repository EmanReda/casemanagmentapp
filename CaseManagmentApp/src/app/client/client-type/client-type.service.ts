import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class ClientTypeService {

  constructor(private http: HttpClient) {}

  GetAll(): Observable<any> {
    return this.http.get('ClientType/GetAll/');
  }

  Operation(data): Observable<any> {
    return this.http.post('ClientType/Operation' , data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete('ClientType/delete/' + id);
  }

}
