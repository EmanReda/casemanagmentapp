import { NgModule ,ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ClientRoutingModule } from './client-routing.module';
import { ClientTypeComponent } from './client-type/client-type.component';
import { ClientComponent } from './client/client.component';
import { ClientSharedComponent} from './client-shared/client-shared.component';
import { StoreModule } from '../store/store.module';
import { StoreSharedModule } from '../store/store-shared/store-shared.module';

@NgModule({
  declarations: [
    ClientTypeComponent,
    ClientComponent,
    ClientSharedComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule.forRoot(),
    StoreModule,
    StoreSharedModule
  ],
  exports: [
   ClientSharedComponent
  ]
})
export class ClientModule {
 
 }
