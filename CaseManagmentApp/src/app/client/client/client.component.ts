import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ClientService } from './client.service';
import { ClientModel, FilterClient } from './client.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html'
})
export class ClientComponent {
  searchModel: FilterClient;
  treeType: any;
  model: ClientModel;
  shadowModel: ClientModel;
  checkCode = false;
  clients: any;
  modalRef: BsModalRef;
  total: number;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public clientSer: ClientService,
    private translate: TranslateService,
    private modalService: BsModalService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Clients', url: null },
        { name: 'Client.PageHeader', url: '/client/list' }
      ],
      pageHeader: 'Client.PageHeader'
    };
    this.searchModel = new FilterClient();
    this.getAll();
    this.treeType = TreeType;
  }
  openModal(template: TemplateRef<any>, item: ClientModel) {
    this.model = new ClientModel();
    if (item != null) {
      this.model = new ClientModel(item.clientId, item.moduleTreeId, item.clientTypeId, item.code, item.name,item.priceListId, item.notes);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAll() {
    this.clientSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.clients = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkClientCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.clientSer.CheckClientCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveClient(): void {
    this.clientSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteClient(id) {
    this.clientSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
