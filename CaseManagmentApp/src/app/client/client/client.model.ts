export class ClientModel {
  constructor(
    public clientId: number = null,
    public moduleTreeId: number = null,
    public clientTypeId: number = null,
    public code: string = null,
    public name: string = null,
    public priceListId: number = null,
    public notes: string = null
  ) { }
}

export class FilterClient {
  constructor(
    public page: number = 1,
    public recordPerPage: number = 20,
    public name: any = null,
    public clientTypeId: number = 0,
    public moduleTreeId: number = 0,
    public priceListId: number = null) {
  }
}
