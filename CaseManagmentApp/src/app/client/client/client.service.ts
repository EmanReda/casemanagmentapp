import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ClientService {

  constructor(private http: HttpClient) { }

  CheckClientCode(Code): any {
    return this.http.get('Client/CheckClientCode?Code=' + Code);
  }

  getAll(model): Observable<any> {
    return this.http.post(`Client/GetAll`, model);
  }
  Save(model): any {
    return this.http.post('Client/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('Client/Delete/' + id);
  }
}
