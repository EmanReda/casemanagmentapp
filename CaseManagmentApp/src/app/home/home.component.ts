import { Component } from '@angular/core';
import { FixedService } from '../shared/services/fixed.service';
import { SubHeader } from '../shared/model/sub-header.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  model: any;
  constructor(public fixed: FixedService) {
    this.fixed.subHeader = new SubHeader(false);
  }
}
