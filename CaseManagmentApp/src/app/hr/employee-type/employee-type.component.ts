import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { EmployeeModel } from './employee-type.model';
import { EmployeeTypeService } from './employee-type.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-employee-type',
  templateUrl: './employee-type.component.html',
})
export class EmployeeTypeComponent {

  employeeOperation: boolean;
  employeeTypes: any;
  modalRef: BsModalRef;
  search: any;
  model: EmployeeModel;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private translate: TranslateService,
      private employeeTypeser: EmployeeTypeService,
      private modalService: BsModalService) {
      this.getAll();
  }

  getAll() {
      this.employeeTypeser.GetAll()
          .subscribe((response) => {
              this.employeeTypes = response;
          }, () => {
              this.global.notificationMessage(4);
          });
  }

  openModal(template: any) {
      this.employeeOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  saveEmployeeType() {
      this.employeeTypeser.Operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.employeeOperation = false;
                      this.model = new EmployeeModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  delete(id) {
      this.employeeTypeser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.employeeOperation = false;
                      this.model = new EmployeeModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  operation(data?) {
      this.model = (data != null) ?
          new EmployeeModel (data.employeeTypeId, data.name, data.notes, data.isDeleted) :
          new EmployeeModel();
      this.employeeOperation = true;
  }

}
