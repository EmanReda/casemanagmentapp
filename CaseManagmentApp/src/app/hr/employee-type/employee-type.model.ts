export class EmployeeModel {
  constructor(
    public employeeTypeId: number = null,
    public name: string = null,
    public notes: string = null,
    public isDeleted: boolean = null
  ) { }
}
