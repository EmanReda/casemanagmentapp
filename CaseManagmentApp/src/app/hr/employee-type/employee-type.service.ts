import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class EmployeeTypeService {

  constructor(private http: HttpClient) {}

  GetAll(): Observable<any> {
    return this.http.get('EmployeType/GetAll/');
  }

  Operation(data): Observable<any> {
    return this.http.post('EmployeType/Operation' , data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete('EmployeType/delete/' + id);
  }

}
