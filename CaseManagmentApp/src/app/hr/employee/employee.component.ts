import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { EmployeeService } from './employee.service';
import { EmployeeModel, FilterEmployee } from './employee.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html'
})

export class EmployeeComponent  {
  searchModel: FilterEmployee;
  treeType: any;
  model: EmployeeModel;
  shadowModel: EmployeeModel;
  checkCode = false;
  employees: any;
  modalRef: BsModalRef;
  total: number;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public employeeSer: EmployeeService,
    private translate: TranslateService,
    private modalService: BsModalService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.HR', url: null },
        { name: 'Employee.PageHeader', url: '/employee/employee/list' }
      ],
      pageHeader: 'Employee.PageHeader'
    };
    this.searchModel = new FilterEmployee();
    this.treeType = TreeType;
    this.getAll();
  }
  openModal(template: TemplateRef<any>, item: EmployeeModel) {
    this.model = new EmployeeModel();
    if (item != null) {
      this.model = new EmployeeModel(item.employeeId, item.moduleTreeId, item.employeeTypeId, 
        item.code, item.name, item.notes,item.address, item.isSalesRep);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAll() {
    this.employeeSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.employees = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkEmployeeCode(code) {
    if (!(code == null || code == '' || code == this.shadowModel.code)) {
      this.employeeSer.CheckEmployeeCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveEmployee(): void {
    this.employeeSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteEmployee(id) {
    this.employeeSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
  resetSearch() {
    this.searchModel = new FilterEmployee();
    this.total = undefined;
    this.employees = undefined;
  }
}
