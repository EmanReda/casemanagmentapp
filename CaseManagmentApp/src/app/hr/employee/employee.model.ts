export class EmployeeModel {
  constructor(
    public employeeId: number = null,
    public moduleTreeId: number = null,
    public employeeTypeId: number = null,
    public code: string = null,
    public name: string = null,
    public notes: string = null,
    public address: string = null,
    public isSalesRep: boolean = null) { }
}

export class FilterEmployee {
  constructor(
    public page: number = 1,
    public recordPerPage: number = 20,
    public name: any = null,
    public employeeTypeId: number = 0,
    public moduleTreeId: number = 0,
    public isSalesRep: boolean = null) {
  }
}
