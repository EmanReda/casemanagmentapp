import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  CheckEmployeeCode(Code): any {
    return this.http.get('Employee/CheckEmployeeCode?Code=' + Code);
  }

  getAll(model): Observable<any> {
    return this.http.post(`Employee/GetAll`, model);
  }
  Save(model): any {
    return this.http.post('Employee/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('Employee/Delete/' + id);
  }
}
