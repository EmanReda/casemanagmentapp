import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { HrRoutingModule } from './hr-routing.module';
import { EmployeeTypeComponent } from './employee-type/employee-type.component';
import { EmployeeComponent } from './employee/employee.component';

@NgModule({
  declarations: [
    EmployeeTypeComponent,
    EmployeeComponent
  ],
  imports: [
    CommonModule,
    HrRoutingModule,
    SharedModule.forRoot()
  ]
})
export class HrModule { }
