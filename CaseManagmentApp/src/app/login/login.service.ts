// import { CookieService } from 'ngx-cookie-service';
// import { Injectable } from '@angular/core';
// import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs';
// import { Router, ActivatedRoute } from '@angular/router';
// import { environment } from '../../environments/environment';
// import { LocalStorage } from '@ngx-pwa/local-storage';
// import { FixedService } from '../shared/services/fixed.service';

// @Injectable({ providedIn: 'root' })
// export class LoginService {
//   constructor(
//     private http: HttpClient,
//     private cookieSer: CookieService,
//     private localStorage: LocalStorage,
//     public fixed: FixedService,
//     private router: Router) {
//   }

//   obtainAccessToken(loginData): Observable<any> {
//     const params = new URLSearchParams();
//     params.append('grant_type', 'password');
//     params.append('username', loginData.email);
//     params.append('password', loginData.password);
//     const options = { headers: new HttpHeaders({ 'Content-type': 'application/x-www-form-urlencoded' }) };
//     return this.http.post<any>(environment.authEndpoint, params.toString(), options);
//   }

//   saveToken(token) {
//     const year = new Date(token['.expires']).getFullYear() + 1;
//     this.cookieSer.deleteAll();
//     this.cookieSer.set('access_token', token.access_token, new Date(token['.expires']), '/');
//     this.cookieSer.set('refresh_token', token.refresh_token, new Date(new Date(token['.expires']).setFullYear(year)), '/');
//     this.cookieSer.set('user_profile', token.user_profile, new Date(new Date(token['.expires']).setFullYear(year)), '/');
//   }

//   checkCredentials() {
//     return (!this.cookieSer.check('access_token') && !this.cookieSer.check('refresh_token')) ? false : true;
//   }

//   logout() {
//     if (window.location.pathname.indexOf('login') < 0) {
//       this.fixed = new FixedService();
//       this.cookieSer.deleteAll('/');
//       this.localStorage.clear().subscribe(() => { });
//       window.location.href = (window.location.pathname.indexOf('login') > 0 && window.location.pathname.indexOf('returnUrl') > 0) ?
//         this.router.url : '/login?returnUrl=' + this.router.url;
//     }
//   }

//   obtainRefreshToken() {
//     const params = new URLSearchParams();
//     params.append('grant_type', 'refresh_token');
//     params.append('refresh_token', this.cookieSer.get('refresh_token'));
//     const options = { headers: new HttpHeaders({ 'Content-type': 'application/x-www-form-urlencoded' }) };
//     return this.http.post(environment.authEndpoint, params.toString(), options);
//   }
// }
