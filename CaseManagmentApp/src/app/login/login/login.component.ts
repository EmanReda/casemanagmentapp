// import { Component, ViewEncapsulation } from '@angular/core';
// import { LoginService } from '../login.service';
// import { ActivatedRoute, Router } from '@angular/router';
// import { TranslateService } from '@ngx-translate/core';
// import { FixedService } from 'src/app/shared/services/fixed.service';
// import { GlobalService } from 'src/app/shared/services/global.service';

// @Component({
//   selector: 'app-login',
//   encapsulation: ViewEncapsulation.None,
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent {
//   model: any;

//   constructor(
//     public fixed: FixedService,
//     public global: GlobalService,
//     private router: Router,
//     private translate: TranslateService,
//     private activatedRoute: ActivatedRoute,
//     private loginSer: LoginService) {
//     this.loginBefore();
//     this.model = { email: '', password: '' };
//   }

//   login(model: any) {
//     this.loginSer.obtainAccessToken(model)
//       .subscribe((data: any) => {
//         this.loginSer.saveToken(data);
//         this.fixed.userProfile = JSON.parse(data.user_profile);
//         this.activatedRoute.queryParams
//           .subscribe(params => {
//             const returnUrl = params.returnUrl;
//             if (returnUrl != null) {
//               this.router.navigate([returnUrl]);
//             } else {
//               this.router.navigate(['/']);
//             }
//             this.translate.get('login.LoginSucess')
//               .subscribe((message) => {
//                 this.global.loading(false);
//                 this.global.notificationMessage(1, null, message);
//               });
//           });
//       }, () => {
//         this.translate.get('login.LoginError')
//           .subscribe((data) => {
//             const message = data;
//             this.global.loading(false);
//             this.global.notificationMessage(4, null, message);
//           });
//       }
//       );
//   }

//   loginBefore() {
//     if (this.loginSer.checkCredentials()) {
//       this.activatedRoute.queryParams
//         .subscribe(params => {
//           const returnUrl = params.returnUrl;
//           if (returnUrl != null) {
//             this.router.navigate([returnUrl]);
//           } else {
//             this.router.navigate(['/']);
//           }
//         });
//     }
//   }
// }
