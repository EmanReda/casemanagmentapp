import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from '../../../../node_modules/ngx-bootstrap';
import { actiontaken } from './actiontaken.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { ActiontakenService } from './actiontaken.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-actiontaken',
  templateUrl: './actiontaken.component.html',
  styleUrls: ['./actiontaken.component.css']
})
export class ActiontakenComponent {
    ActionOperation: boolean;
    Actions: any;
    modalRef: BsModalRef;
    search: any;
    model: actiontaken;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private translate: TranslateService,
      private actiontakenser: ActiontakenService,
      private modalService: BsModalService) {
      this.getAll();
  }
  getAll() {
    this.actiontakenser.getAll().subscribe((data) => {
        this.Actions = data;
    }, () => {
        this.global.notificationMessage(4);
    });
  }
  openModal(template: any) {
    this.ActionOperation = false;
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
  operation(data?) {
    this.model = (data != null)
        ? new actiontaken(data.productId, data.namear, data.nameen)
        : new actiontaken();
    this.ActionOperation = true;
  }
  saveUnit() {
    this.actiontakenser.operation(this.model)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.ActionOperation = false;
                    this.model = new actiontaken();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
  } 
  delete(id) {
    this.actiontakenser.delete(id)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.ActionOperation = false;
                    this.model = new actiontaken();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
    }

}
