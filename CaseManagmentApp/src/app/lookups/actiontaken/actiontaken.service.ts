import { Injectable } from '@angular/core';

import { Observable } from '../../../../node_modules/rxjs';
import { HttpClient } from '../../../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActiontakenService {

    constructor(private http: HttpClient) 
    { }
    getAll(): Observable<any> {
    return this.http.get('ActionTaken/GetAll');
    this.http
    }
    operation(model): Observable<any> {
    return this.http.post('MaintenanceProduct/Operation', model);
    }

    delete(id: number): Observable<any> {
    return this.http.delete('MaintenanceProduct/Delete/' + id);
    }
}
