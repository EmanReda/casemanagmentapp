import { Component, Input, Output, EventEmitter} from '@angular/core';
import { BsModalRef, BsModalService } from '../../../../node_modules/ngx-bootstrap';
import { CaseStage } from './case-stage.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { CaseStageService } from './case-stage.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-case-stage',
  templateUrl: './case-stage.component.html',
  styleUrls: ['./case-stage.component.css']
})
export class CaseStageComponent {
  CaseStageOperation: boolean;
  CaseStages: any;
  modalRef: BsModalRef;
  search: any;
  model: CaseStage;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor( public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private CaseStageser: CaseStageService,
    private modalService: BsModalService) { 
    this.getAll();
    }
    getAll() {
      this.CaseStageser.getAll().subscribe((data) => {
          this.CaseStages = data;
      }, () => {
          this.global.notificationMessage(4);
      });
    }
    openModal(template: any) {
      this.CaseStageOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    operation(data?) {
      this.model = (data != null)
          ? new CaseStage(data.productId, data.namear, data.nameen)
          : new CaseStage();
      this.CaseStageOperation = true;
    }
    saveUnit() {
      this.CaseStageser.operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.CaseStageOperation = false;
                      this.model = new CaseStage();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
    } 
    delete(id) {
      this.CaseStageser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.CaseStageOperation = false;
                      this.model = new CaseStage();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
      }

}
