import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from '../../../../node_modules/ngx-bootstrap';
import { DomesticIndustry } from './domesticindustry.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { DomesticindustryService } from './domesticindustry.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-domesticindustry',
  templateUrl: './domesticindustry.component.html',
  styleUrls: ['./domesticindustry.component.css']
})
export class DomesticindustryComponent{
  DomesticindustryOperation: boolean;
  domesticindustrys: any;
  modalRef: BsModalRef;
  search: any;
  model: DomesticIndustry;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private Domesticindustryser: DomesticindustryService,
    private modalService: BsModalService) {
    this.getAll();
  }
  getAll() {
    this.Domesticindustryser.getAll().subscribe((data) => {
        this.domesticindustrys = data;
    }, () => {
        this.global.notificationMessage(4);
    });
  }
  openModal(template: any) {
    this.DomesticindustryOperation = false;
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
  operation(data?) {
    this.model = (data != null)
        ? new DomesticIndustry(data.productId, data.namear, data.nameen)
        : new DomesticIndustry();
    this.DomesticindustryOperation = true;
  }
  saveUnit() {
    this.Domesticindustryser.operation(this.model)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.DomesticindustryOperation = false;
                    this.model = new DomesticIndustry();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
  } 
  delete(id) {
    this.Domesticindustryser.delete(id)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.DomesticindustryOperation = false;
                    this.model = new DomesticIndustry();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
 }
  
}
