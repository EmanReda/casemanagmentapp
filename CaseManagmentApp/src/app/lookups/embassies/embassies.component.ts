import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Embassies } from './embassies.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { EmbassiesService } from './embassies.service';
import { ResponseEnum } from '../../shared/enums/response.enum';


@Component({
  selector: 'app-embassies',
  templateUrl: './embassies.component.html',
  styleUrls: ['./embassies.component.css']
})
export class EmbassiesComponent  {
  EmbassiesOperation: boolean;
  Embassies: any;
  modalRef: BsModalRef;
  search: any;
  model: Embassies;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private Embassiesser: EmbassiesService,
    private modalService: BsModalService) {
    this.getAll();
    }
    getAll() {
      this.Embassiesser.getAll().subscribe((data) => {
          this.Embassies = data;
      }, () => {
          this.global.notificationMessage(4);
      });
    }
    openModal(template: any) {
      this.EmbassiesOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    operation(data?) {
      this.model = (data != null)
          ? new Embassies(data.productId, data.namear, data.nameen)
          : new Embassies();
      this.EmbassiesOperation = true;
    }
    saveUnit() {
      this.Embassiesser.operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.EmbassiesOperation = false;
                      this.model = new Embassies();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
    }
    delete(id) {
      this.Embassiesser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.EmbassiesOperation = false;
                      this.model = new Embassies();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
      }

}
