import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ForeignExporterProducers } from './foreignexporter-producers.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { ForeignexporterProducersService } from './foreignexporter-producers.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-foreignexporter-producers',
  templateUrl: './foreignexporter-producers.component.html',
  styleUrls: ['./foreignexporter-producers.component.css']
})
export class ForeignexporterProducersComponent{
  ForeignexporterOperation: boolean;
  Foreignexporters: any;
  modalRef: BsModalRef;
  search: any;
  model: ForeignExporterProducers;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private Foreignexporterser: ForeignexporterProducersService,
    private modalService: BsModalService) {
    this.getAll();
  }
  getAll() {
    this.Foreignexporterser.getAll().subscribe((data) => {
        this.Foreignexporters = data;
    }, () => {
        this.global.notificationMessage(4);
    });
  }
  openModal(template: any) {
    this.ForeignexporterOperation = false;
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
  operation(data?) {
    this.model = (data != null)
        ? new ForeignExporterProducers(data.productId, data.namear, data.nameen)
        : new ForeignExporterProducers();
    this.ForeignexporterOperation = true;
  }
  saveUnit() {
    this.Foreignexporterser.operation(this.model)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.ForeignexporterOperation = false;
                    this.model = new ForeignExporterProducers();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
  }
  delete(id) {
    this.Foreignexporterser.delete(id)
        .subscribe(
            (response) => {
                if (response.type === ResponseEnum.Success) {
                    this.ForeignexporterOperation = false;
                    this.model = new ForeignExporterProducers();
                    this.getAll();
                }
                if (response.name == null) {
                    this.global.notificationMessage(response.type);
                } else {
                    this.translate.get(response.name)
                        .subscribe(
                            (val) => {
                                this.global.notificationMessage(response.type, null, val);
                            });
                }
            }, (error) => {
                this.global.notificationMessage(4);
            });
    }
}
