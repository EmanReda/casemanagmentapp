import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Government } from './government.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { TranslateService } from '../../../../node_modules/@ngx-translate/core';
import { GovernmentService } from './government.service';
import { ResponseEnum } from '../../shared/enums/response.enum';

@Component({
  selector: 'app-government',
  templateUrl: './government.component.html',
  styleUrls: ['./government.component.css']
})
export class GovernmentComponent{
  GovernmentOperation: boolean;
  governments: any;
  modalRef: BsModalRef;
  search: any;
  model: Government;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private Governmentser: GovernmentService,
    private modalService: BsModalService) {
    this.getAll();
    }
    getAll() {
      this.Governmentser.getAll().subscribe((data) => {
          this.governments = data;
      }, () => {
          this.global.notificationMessage(4);
      });
    }
    openModal(template: any) {
      this.GovernmentOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    operation(data?) {
      this.model = (data != null)
          ? new Government(data.productId, data.namear, data.nameen)
          : new Government();
      this.GovernmentOperation = true;
    }
    saveUnit() {
      this.Governmentser.operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.GovernmentOperation = false;
                      this.model = new Government();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
    }
    delete(id) {
      this.Governmentser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.GovernmentOperation = false;
                      this.model = new Government();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
      }

}
