import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Importers } from './importers.model';
import { GlobalService } from '../../shared/services/global.service';
import { FixedService } from '../../shared/services/fixed.service';
import { ImportersService } from './importers.service';
import { ResponseEnum } from '../../shared/enums/response.enum';


@Component({
  selector: 'app-importers',
  templateUrl: './importers.component.html',
  styleUrls: ['./importers.component.css']
})
export class ImportersComponent  {
  ImportersOperation: boolean;
  Importers: any;
  modalRef: BsModalRef;
  search: any;
  model: Importers;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(public global: GlobalService,
    public fixed: FixedService,
    private translate: TranslateService,
    private Importersser: ImportersService,
    private modalService: BsModalService) {
    this.getAll();
    }
    getAll() {
      this.Importersser.getAll().subscribe((data) => {
          this.Importers = data;
      }, () => {
          this.global.notificationMessage(4);
      });
    }
    openModal(template: any) {
      this.ImportersOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    operation(data?) {
      this.model = (data != null)
          ? new Importers(data.productId, data.namear, data.nameen)
          : new Importers();
      this.ImportersOperation = true;
    }
    saveUnit() {
      this.Importersser.operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.ImportersOperation = false;
                      this.model = new Importers();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
    }
    delete(id) {
      this.Importersser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.ImportersOperation = false;
                      this.model = new Importers();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
      }
}
