import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImportersService {
  constructor(private http: HttpClient) 
  { }
  getAll(): Observable<any> {
  return this.http.get('ActionTaken/GetAll');
  this.http
  }
  operation(model): Observable<any> {
  return this.http.post('MaintenanceProduct/Operation', model);
  }

  delete(id: number): Observable<any> {
  return this.http.delete('MaintenanceProduct/Delete/' + id);
  }
}
