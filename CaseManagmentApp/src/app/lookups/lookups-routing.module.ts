import { ActiontakenComponent } from './actiontaken/actiontaken.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaseStageComponent } from './case-stage/case-stage.component';
import { DomesticindustryComponent } from './domesticindustry/domesticindustry.component';
import { EmbassiesComponent } from './embassies/embassies.component';
import { ForeignexporterProducersComponent } from './foreignexporter-producers/foreignexporter-producers.component';
import { GovernmentComponent } from './government/government.component';
import { ImportersComponent } from './importers/importers.component';

const routes: Routes = [
  { path: 'actiontaken', component:ActiontakenComponent  },
  { path: 'casestage', component:CaseStageComponent  },
  { path: 'domesticindustry', component:DomesticindustryComponent  },
  { path: 'embassies', component:EmbassiesComponent  },
  { path: 'foreignexporter', component:ForeignexporterProducersComponent  },
  { path: 'government', component:GovernmentComponent  },
  { path: 'importers', component:ImportersComponent  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LookupsRoutingModule { }
