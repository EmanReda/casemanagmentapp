import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LookupsRoutingModule } from './lookups-routing.module';
import { ActiontakenComponent } from './actiontaken/actiontaken.component';
import { CaseStageComponent } from './case-stage/case-stage.component';
import { DomesticindustryComponent } from './domesticindustry/domesticindustry.component';
import { ImportersComponent } from './importers/importers.component';
import { ForeignexporterProducersComponent } from './foreignexporter-producers/foreignexporter-producers.component';
import { EmbassiesComponent } from './embassies/embassies.component';
import { GovernmentComponent } from './government/government.component';
import { ActiontakenService } from './actiontaken/actiontaken.service';
import { SharedModule } from '../shared/shared.module';
import { CaseStageService } from './case-stage/case-stage.service';
import { DomesticindustryService } from './domesticindustry/domesticindustry.service';
import { EmbassiesService } from './embassies/embassies.service';
import { ForeignexporterProducersService } from './foreignexporter-producers/foreignexporter-producers.service';
import { GovernmentService } from './government/government.service';
import { ImportersService } from './importers/importers.service';

@NgModule({
  declarations: [
    ActiontakenComponent,
    CaseStageComponent,
    DomesticindustryComponent,
    ImportersComponent,
    ForeignexporterProducersComponent,
    GovernmentComponent,
    EmbassiesComponent
  ],
  imports: [
    CommonModule,
    LookupsRoutingModule,
    SharedModule.forRoot(),
  ],
  providers: [
    ActiontakenService,
    CaseStageService,
    DomesticindustryService,
    EmbassiesService,
    ForeignexporterProducersService,
    GovernmentService,
    ImportersService

  ],
  exports: [
    ActiontakenComponent,
    CaseStageComponent,
    DomesticindustryComponent,
    ImportersComponent,
    ForeignexporterProducersComponent,
    GovernmentComponent,
    EmbassiesComponent
  ]
})
export class LookupsModule { }
