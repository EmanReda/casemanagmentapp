import { Component } from '@angular/core';
import { FilterCenter } from './center-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { CenterService } from 'src/app/maintenance/center/center.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-center-list',
  templateUrl: './center-list.component.html'
})


export class CenterListComponent {
  searchModel: FilterCenter;
  total: number;
  centers: any;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public centerSer: CenterService,
    private translate: TranslateService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Maintenance', url: null },
        { name: 'Center.List', url: '/maintenance/center/list' }
      ],
      pageHeader: 'Center.List'
    };
    this.searchModel = new FilterCenter();
    this.getAll();
  }

  getAll() {
    this.centerSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.centers = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }


  delete(id) {
    this.centerSer.deleteCenter(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
