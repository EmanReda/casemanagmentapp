export class FilterCenter {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public code: string = null,
        public name: string = null) {
    }
}