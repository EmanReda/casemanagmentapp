import { Component, OnInit } from '@angular/core';
import { CenterModel } from './center-operation.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { CenterService } from '../center.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-center-operation',
  templateUrl: './center-operation.component.html'
})
export class CenterOperationComponent implements OnInit {
  model: CenterModel;
  sub: any;
  oldCode: any;
  checkCode = false;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private centerSer: CenterService,
    private location: Location,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Maintenance', url: null },
        { name: 'Center.List', url: '/maintenance/center/list' }
      ],
      pageHeader: ''
    };
    this.model = new CenterModel();
  }

  ngOnInit() {
    this.sub = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Center.Add', url: '/maintenance/center/operation' });
          this.fixed.subHeader.pageHeader = 'Center.Add';
          this.model.centerId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Center.Edit', url: '/maintenance/center/operation/' + id });
          this.fixed.subHeader.pageHeader = 'Center.Edit';
          this.model.centerId = Number(id);
          this.getStock(Number(id));
        }
      });
  }


  getStock(id: number) {
    this.centerSer.GetCenterById(id)
      .subscribe((Response) => {
        this.model = Response;
        this.oldCode = this.model.code;
        this.moduleTreeChange({
          moduleTreeId: Response.billModuleId,
          acC_ModuleAccount: Response.billTreeList
        }, 'stock');
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkStockCode(code) {
    if (!(code == null || code == '' || code == this.oldCode)) {
      this.centerSer.CheckCenterCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  moduleTreeChange(event, target: string, from?) {
    const account = target + 'AccountId';
    const module = target + 'ModuleId';
    const tree = target + 'TreeList';
    this.model[module] = (event == null) ? null : event.moduleTreeId;
    const list = [];
    if (event != null && event.acC_ModuleAccount != null) {
      event.acC_ModuleAccount.forEach(element => {
        list.push(element.accountTreeId);
      });
    }
    this.model[tree] = (event == null) ? undefined : list;
    if (from === 'html') {
      this.model[account] = null;
    }
  }
  saveStock() {
    this.centerSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteStock(id) {
    this.centerSer.deleteCenter(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
