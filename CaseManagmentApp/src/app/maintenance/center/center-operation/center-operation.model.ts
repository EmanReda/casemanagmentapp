export class CenterModel {
    constructor(
        public centerId: number = null,
        public code: string = null,
        public name: string = null,
        public companyId: number = null,
        public billAccountId: number = null,
        public billModuleId: number = null,
        public billTreeList: any = null ) {
    }
}
