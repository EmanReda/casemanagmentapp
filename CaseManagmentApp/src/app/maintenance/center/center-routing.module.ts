import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CenterListComponent } from './center-list/center-list.component';
import { CenterOperationComponent } from './center-operation/center-operation.component';

const routes: Routes = [
  { path: 'list', component: CenterListComponent },
  { path: 'operation', component: CenterOperationComponent },
  { path: 'operation/:id', component: CenterOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CenterRoutingModule { }
