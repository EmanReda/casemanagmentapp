import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { CenterService } from '../center.service';

@Component({
  selector: 'app-center-shared',
  templateUrl: './center-shared.component.html'
})
export class CenterSharedComponent {
  centers: any;
  modalRef: BsModalRef;
  search: any;
  @Input() disable: boolean;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private centerSer: CenterService,
    private modalService: BsModalService) {
    this.getAll();
  }
  getAll() {
    this.centerSer.GetSharedData()
      .subscribe((response) => {
        this.centers = response;
      },
        (err) => {
          this.global.notificationMessage(4);
        });
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

}
