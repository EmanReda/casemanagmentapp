import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CenterRoutingModule } from './center-routing.module';
import { CenterListComponent } from './center-list/center-list.component';
import { CenterOperationComponent } from './center-operation/center-operation.component';
import { CenterSharedComponent } from './center-shared/center-shared.component';
import { MaintenanceSharedModule } from '../maintenance-shared/maintenance-shared.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    CenterListComponent,
    CenterOperationComponent,
    CenterSharedComponent,
  ],
  imports: [
    CommonModule,
    CenterRoutingModule,
    MaintenanceSharedModule,
    SharedModule.forRoot(),
  ],
  exports: [
    CenterSharedComponent
  ]
})
export class CenterModule { }
