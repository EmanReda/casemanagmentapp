import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class CenterService {

  constructor(private http: HttpClient) { }
  GetAll(model): Observable<any> {
    return this.http.post('Center/GetAll', model);
  }

  GetSharedData(): Observable<any> {
    return this.http.get('Center/GetSharedData');
  }

  GetCenterById(id: number): Observable<any> {
    return this.http.get(`Center/GetCenterById/${id}`);
  }

  CheckCenterCode(Code): Observable<any> {
    return this.http.get('Center/CheckCenterCode?Code=' + Code);
  }

  Save(model): Observable<any> {
    return this.http.post('Center/Operation', model);
  }

  deleteCenter(id): Observable<any> {
    return this.http.delete(`Center/Delete/${id}`);
  }
}
