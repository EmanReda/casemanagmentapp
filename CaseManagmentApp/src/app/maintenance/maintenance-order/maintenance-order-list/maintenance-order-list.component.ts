import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { FilterMaintenanceOrder } from './maintenance-order-list.model';
import { MaintenanceOrderService } from '../maintenance-order.service'
@Component({
  selector: 'app-maintenance-order-list',
  templateUrl: './maintenance-order-list.component.html'
})
export class MaintenanceOrderListComponent {
  searchModel: FilterMaintenanceOrder;
  total: any;
  data: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public maintenanceOrderSer: MaintenanceOrderService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Maintenance', url: null },
        { name: 'MaintenanceOrder.List', url: '/maintenance/order/list' }
      ],
      pageHeader: 'MaintenanceOrder.List'
    };
    this.searchModel = new FilterMaintenanceOrder();
    this.getAll();
  }
  
  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.maintenanceOrderSer.getAll(this.searchModel)
    .subscribe((response) => {
      this.data = response.data;
        this.total = response.total;
      }, (err) => {
        this.global.notificationMessage(4);
      })
  }
  logger(eventtt)
  {
    console.log(eventtt);
  }
  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterMaintenanceOrder();
    this.total = undefined;
    this.data = undefined;
  }
}
