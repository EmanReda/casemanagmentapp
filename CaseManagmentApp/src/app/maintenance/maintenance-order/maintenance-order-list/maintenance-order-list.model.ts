export class FilterMaintenanceOrder {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public transactionNo: string = null,
        public fromDate: any = null,
        public toDate: any = null,
        public clientId: number = null,
        public centerId: number = null,
        public maiProductId: number = null,
        public isClosed: boolean = null,
        public isVoided: boolean = false) { }
}