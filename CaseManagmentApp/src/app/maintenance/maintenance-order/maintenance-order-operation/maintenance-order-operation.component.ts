import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { MaintenanceOrderService } from '../maintenance-order.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { MaintenanceOrderModel, MaintenanceOrderItemsModel } from './maintenance-order-operation.model';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-maintenance-order-operation',
  templateUrl: './maintenance-order-operation.component.html',
  styles: []
})
export class MaintenanceOrderOperationComponent implements OnInit {
  model: MaintenanceOrderModel;
  sub: any;
  orderItem = new MaintenanceOrderItemsModel();
  emptyList = false;
  @ViewChild('operationError') operationError: SwalComponent;
  lockGridEdit = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public maintenanceOrderSer: MaintenanceOrderService,
    private localeService: BsLocaleService,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Maintenance', url: null },
        { name: 'MaintenanceOrderOperation.List', url: '/maintenance/order/list' }
      ],
      pageHeader: ''
    };
    this.model = new MaintenanceOrderModel();
  }
  ngOnInit() {

    this.sub = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'MaintenanceOrderOperation.Add', path: '/maintenance/order/operation' })
          this.fixed.subHeader.pageHeader = 'MaintenanceOrderOperation.Add';
          this.model.maintenanceOrderId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'MaintenanceOrderOperation.Edit', path: '/maintenance/order/operation/' + id });
          this.fixed.subHeader.pageHeader = 'MaintenanceOrderOperation.Edit';
          this.model.maintenanceOrderId = Number(id);
          this.getOrder(Number(id));
        }
      });
  }

  getOrder(id: number) {
    this.maintenanceOrderSer.getOrderById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.model.maI_MaintenanceOrderItem.forEach(item => {
            item.operation = 's';
            item.nameAr = item.maI_Utilitiy.name;
            item.nameEn = item.maI_Utilitiy.name;
            item.price = item.maI_Utilitiy.price;
            item.vat = item.maI_Utilitiy.vat;
          });
          this.calTotal();
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate(['/maintenance/order/list']);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  getTransactionNo(date) {
    if (date != null && this.model.maintenanceOrderId == null) {
      this.maintenanceOrderSer.getTransactionNo(this.global.formatDate(date))
        .subscribe((response) => {
          this.model.transactionNo = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }
  saveOrder() {
    this.model.date = this.global.formatDate(this.model.date);
    this.calTotal();
    this.maintenanceOrderSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/maintenance/order/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  calTotal() {
    this.model.total = 0;
    const filter = this.model.maI_MaintenanceOrderItem.filter(s => s.isDeleted == false || s.isDeleted == null);
    if (filter.length !== 0) {
      filter.forEach(item => {
        this.model.total += this.calUtilitiyPrice(item);
      });
    }
  }
  selectItem(selected, item: MaintenanceOrderItemsModel) {
    if (selected != null) {
      item.price = selected.price;
      item.vat = selected.vat;
      item.utilitiyId = selected.utilitiyId;
      item.nameAr = selected.name;
      item.nameEn = selected.name;
    } else {
      item.price = null;
      item.vat = null;
      item.utilitiyId = null;
      item.nameAr = null;
      item.nameEn = null;
    }
    this.calTotal();
  }

  calUtilitiyPrice(item: MaintenanceOrderItemsModel) {
    if (item == null) { return null; }
    return item.price + item.vat;
  }

  itemOperation(data?: MaintenanceOrderItemsModel) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.maI_MaintenanceOrderItem == null) {
        this.model.maI_MaintenanceOrderItem = [];
      }
      if (data == null) {
        const temp = new MaintenanceOrderItemsModel(null,
          this.model.maintenanceOrderId, this.model.companyId);
        this.model.maI_MaintenanceOrderItem.push(temp);
      } else {
        this.orderItem = new MaintenanceOrderItemsModel(data.maintenanceOrderItemId, data.maintenanceOrderId, data.companyId,
          data.maI_Utilitiy, data.price, data.vat, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('MaintenanceOrderOperation.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data: MaintenanceOrderItemsModel, index?) {
    if (type === 'close') {
      data = this.orderItem;
      this.lockGridEdit = false;
      (data.maintenanceOrderItemId == null) ? this.model.maI_MaintenanceOrderItem.splice(index, 1) :
        this.model.maI_MaintenanceOrderItem[index] = this.orderItem;
      const len = this.model.maI_MaintenanceOrderItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
      this.emptyList = (len === 0) ? true : false;
    } else if (type === 'save') {
      if (data.utilitiyId == null) {
        this.translate.get('MaintenanceOrderOperation.ChooseUtilitiy')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }

      this.lockGridEdit = false;
      data.operation = 's';
    }
  }

  itemDelete(item: MaintenanceOrderItemsModel, index) {
    if (item.maintenanceOrderItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.maI_MaintenanceOrderItem.splice(index, 1);
    }
    this.calTotal();
    const len = this.model.maI_MaintenanceOrderItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }
  validateGrid(): boolean {
    if (this.emptyList || this.lockGridEdit || this.model.centerId == null || this.model.clientId == null
      || this.model.maiProductId == null || this.model.transactionNo == null) { return true; }
    return false;
  }

  voidedTransaction(id) {
    this.maintenanceOrderSer.void(id)
      .subscribe();
  }
  PrintReport() {
    this.maintenanceOrderSer.PrintReceipt(this.model.maintenanceOrderId)
      .subscribe((response) => {
        console.log(response);
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
