export class MaintenanceOrderModel {
    constructor(
        public transactionNo: string = null,
        public companyId: number = null,
        public maintenanceOrderId: number = null,
        public total: number = null,
        public clientId: number = null,
        public centerId: number = null,
        public maiProductId: number = null,
        public description: string = null,
        public date: any = null,
        public isClosed: boolean = false,
        public isVoided: boolean = null,
        public cL_Client: any = null,
        public maI_MaintenanceOrderItem: MaintenanceOrderItemsModel[] = []
    ) { }
}
export class MaintenanceOrderItemsModel {
    constructor(
        public maintenanceOrderItemId: number = null,
        public maintenanceOrderId: number = null,
        public companyId: number = null,
        public maI_Utilitiy: any = null,
        public price: number = null,
        public vat: number = null,
        public isEdited: boolean = false,
        public isDeleted: boolean = false,
        public operation: string = 'e',
        public utilitiyId: number = null,
        public nameAr: string = null,
        public nameEn: string = null) {
    }
}
