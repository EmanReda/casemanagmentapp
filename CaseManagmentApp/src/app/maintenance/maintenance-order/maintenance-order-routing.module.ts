import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceOrderListComponent } from './maintenance-order-list/maintenance-order-list.component';
import { MaintenanceOrderOperationComponent } from './maintenance-order-operation/maintenance-order-operation.component';

const routes: Routes = [
  { path: 'list', component: MaintenanceOrderListComponent },
  { path: 'operation', component: MaintenanceOrderOperationComponent },
  { path: 'operation/:id', component: MaintenanceOrderOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceOrderRoutingModule { }
