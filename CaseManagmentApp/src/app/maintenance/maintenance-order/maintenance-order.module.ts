import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceOrderRoutingModule } from './maintenance-order-routing.module';
import { MaintenanceOrderListComponent } from './maintenance-order-list/maintenance-order-list.component';
import { MaintenanceOrderOperationComponent } from './maintenance-order-operation/maintenance-order-operation.component';
import { MaintenanceSharedModule } from '../maintenance-shared/maintenance-shared.module';
import { SharedModule } from '../../shared/shared.module';
import { CenterModule } from '../center/center.module';
import { ClientModule } from '../../client/client.module';

@NgModule({
  declarations: [
    MaintenanceOrderListComponent,
    MaintenanceOrderOperationComponent
  ],
  imports: [
    CommonModule,
    MaintenanceOrderRoutingModule,
    MaintenanceSharedModule,
    SharedModule.forRoot(),
    CenterModule,
    ClientModule
  ]
})
export class MaintenanceOrderModule { }
