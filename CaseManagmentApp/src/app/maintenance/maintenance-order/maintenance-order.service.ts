import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceOrderService {

  constructor(private http: HttpClient) { }

  getAll(model): Observable<any> {
    return this.http.post("MaintenanceOrder/GetAll", model);
  }

  getOrderById(id: number): Observable<any> {
    return this.http.get(`MaintenanceOrder/getOrderById/${id}`);
  }
  getTransactionNo(date): Observable<any> {
    return this.http.get('MaintenanceOrder/getTransactionNo?date=' + date);
  }
  save(model):Observable<any>{
    return this.http.post("MaintenanceOrder/Operation",model);
  }
  void(id): Observable<any> {
    return this.http.delete(`MaintenanceOrder/Voided/${id}`);
  }
  PrintReceipt(id): any {
    return this.http.get('MaintenanceOrder/DocumentReport/' + id);
  }
}
