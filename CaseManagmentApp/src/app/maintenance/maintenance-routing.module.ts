import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceComponent } from './maintenance.component';

const routes: Routes = [
  { path: '',
   component: MaintenanceComponent,
   children : [
      {
        path: 'center',
        loadChildren: './center/center.module#CenterModule'
      },
      {
        path: 'order',
        loadChildren: './maintenance-order/maintenance-order.module#MaintenanceOrderModule'
      },
      ]
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
