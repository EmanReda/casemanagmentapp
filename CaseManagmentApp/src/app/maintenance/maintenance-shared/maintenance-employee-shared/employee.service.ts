import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }


  GetSharedData(): Observable<any> {
    return this.http.get('MaintenanceEmployee/GetSharedData');
  }

}
