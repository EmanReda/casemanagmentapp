import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalService } from '../../../shared/services/global.service';
import { FixedService } from '../../../shared/services/fixed.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { EmployeeService } from 'src/app/maintenance/maintenance-shared/maintenance-employee-shared/employee.service';

@Component({
  selector: 'app-maintenance-employee-shared',
  templateUrl: './maintenance-employee-shared.component.html',
  styles: []
})
export class MaintenanceEmployeeSharedComponent implements OnInit {
  Employees: any;
  modalRef: BsModalRef;
  search: any;
  @Input() disable: boolean;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private EmplyeesServ: EmployeeService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.EmplyeesServ.GetSharedData()
      .subscribe((response) => {
        console.log("employees");
        console.log(response);
        this.Employees = response;
      },
        (err) => {
          console.log("errors");
          console.log(err);
          this.global.notificationMessage(4);
        });
  }
  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
}
