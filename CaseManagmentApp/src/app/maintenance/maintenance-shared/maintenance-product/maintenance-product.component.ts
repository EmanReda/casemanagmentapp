import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MaintenanceProduct } from './maintenance-product.model';
import { MaintenanceProductService } from './maintenance-product.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
    selector: 'app-maintenance-product',
    templateUrl: './maintenance-product.component.html'
})

export class MaintenanceProductComponent {
    ProductOperation: boolean;
    Products: any;
    modalRef: BsModalRef;
    search: any;
    model: MaintenanceProduct;
    checkcode: any;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private MaintenanceProductSer: MaintenanceProductService,
        private modalService: BsModalService) {
        this.getAll();
    }

    getAll() {
        this.MaintenanceProductSer.getAll().subscribe((data) => {
            this.Products = data;
        }, () => {
            this.global.notificationMessage(4);
        });
    }

    openModal(template: any) {
        this.ProductOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }

    operation(data?) {
        this.model = (data != null)
            ? new MaintenanceProduct(data.productId, data.code, data.name, data.isDeleted)
            : new MaintenanceProduct();
        this.ProductOperation = true;
    }

    checkCode() {
        if (!(this.model.code == null || this.model.code == '')) {
            this.MaintenanceProductSer.checkCode(this.model)
                .subscribe((data) => {
                    this.checkcode = data;
                },
                    () => {
                        this.global.notificationMessage(4);
                    });
        }
    }

    saveUnit() {
        this.MaintenanceProductSer.operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.ProductOperation = false;
                        this.model = new MaintenanceProduct();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.MaintenanceProductSer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.ProductOperation = false;
                        this.model = new MaintenanceProduct();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }
}
