export class MaintenanceProduct{
    constructor(
        public productId:number=null,
        public code:string =null,
        public name:string =null,
        public isDeleted:boolean=null
    ){}
}