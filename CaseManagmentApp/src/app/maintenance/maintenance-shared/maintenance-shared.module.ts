import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaintenanceEmployeeSharedComponent } from './maintenance-employee-shared/maintenance-employee-shared.component';
import { MaintenanceProductComponent } from './maintenance-product/maintenance-product.component';
import { SharedModule } from '../../shared/shared.module';
import { UtilitiyComponent } from './utilitiy/utilitiy.component';

@NgModule({
  declarations: [
    MaintenanceEmployeeSharedComponent,
    MaintenanceProductComponent,
    UtilitiyComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    MaintenanceEmployeeSharedComponent,
    MaintenanceProductComponent,
    UtilitiyComponent
  ]
})
export class MaintenanceSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MaintenanceSharedModule
    };
  }
 }
