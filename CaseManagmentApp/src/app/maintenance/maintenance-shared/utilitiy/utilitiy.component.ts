import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Utilitiy } from './utilitiy.model';
import { UtilitiyService } from './utilitiy.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';



@Component({
    selector: 'app-utilitiy',
    templateUrl: './utilitiy.component.html',
    styles: []
})
export class UtilitiyComponent {
    UtilitiyOperation: boolean;
    Utilities: any;
    modalRef: BsModalRef;
    search: any;
    model: Utilitiy;
    checkcode: any;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private UtilitiySer: UtilitiyService,
        private modalService: BsModalService) {
        
        this.getAll();
    }
    emitObject(utilitiyId?) {
        if (utilitiyId == null)
            this.selectedValueChanges.emit(null);
        
        this.selectedValueChanges.emit(
            this.Utilities.find(
                (Utilitiy) => {
                    return Utilitiy.utilitiyId == utilitiyId
                }
            )
        )
    }
    getAll() {
        this.UtilitiySer.getAll().subscribe((data) => {
            this.Utilities = data;
        }, () => {
            this.global.notificationMessage(4);
        });
    }

    openModal(template: any) {
        this.UtilitiyOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    operation(data?) {
        this.model = (data != null)
            ? new Utilitiy(data.utilitiyId, data.code, data.name, data.price, data.vat, data.isDeleted)
            : new Utilitiy();
        this.UtilitiyOperation = true;
    }
    checkCode() {
        if (!(this.model.code == null || this.model.code == '')) {
            this.UtilitiySer.checkCode(this.model)
                .subscribe((data) => {
                    this.checkcode = data;
                },
                    () => {
                        this.global.notificationMessage(4);
                    });
        }
    }
    saveUnit() {
        this.UtilitiySer.operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.UtilitiyOperation = false;
                        this.model = new Utilitiy();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }
    delete(id) {
        this.UtilitiySer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.UtilitiyOperation = false;
                        this.model = new Utilitiy();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

}
