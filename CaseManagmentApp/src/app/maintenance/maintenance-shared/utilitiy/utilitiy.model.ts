export class Utilitiy{
    constructor(
        public utilitiyId:number=null,
        public code:string =null,
        public name:string =null,
        public vat:string =null,
        public price:string =null,
        public isDeleted:boolean=null
    ){}
}