import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiyService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
      return this.http.get('Utilitiy/GetAll');
  }

  operation(model): Observable<any> {
      return this.http.post('Utilitiy/Operation', model);
  }

  delete(id: number): Observable<any> {
      return this.http.delete('Utilitiy/Delete/' + id);
  }

  checkCode(model): any {
      return this.http.post('Utilitiy/CheckCode', model);
  }
}

