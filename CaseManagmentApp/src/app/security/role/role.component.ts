import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { RoleModel } from './role.model';
import { RoleService } from './role.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html'})
export class RoleComponent {
  roleOperation: boolean;
  roles: any;
  modalRef: BsModalRef;
  search: any;
  model: RoleModel;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private translate: TranslateService,
      private roleser: RoleService,
      private modalService: BsModalService) {
      this.getAll();
  }

  getAll() {
      this.roleser.GetAll()
          .subscribe((response) => {
              this.roles = response;
          }, () => {
              this.global.notificationMessage(4);
          });
  }

  openModal(template: any) {
      this.roleOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  saveRole() {
      this.roleser.Operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.roleOperation = false;
                      this.model = new RoleModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  delete(id) {
      this.roleser.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.roleOperation = false;
                      this.model = new RoleModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  operation(data?) {
      this.model = (data != null) ?
          new RoleModel(data.id, data.name , data.isDeleted) :
          new RoleModel();
      this.roleOperation = true;
  }

 

}
