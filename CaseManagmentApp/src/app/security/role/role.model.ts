export class RoleModel {
    constructor(
      public id: number = null,
      public name: string = null,
      public isDeleted: boolean = null
    ) { }
  }
