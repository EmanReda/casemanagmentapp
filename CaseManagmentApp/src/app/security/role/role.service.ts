import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class RoleService {

  constructor(private http: HttpClient) {}

  GetAll(): Observable<any> {
    return this.http.get('Roles/GetAll/');
  }

  Operation(data): Observable<any> {
    return this.http.post('Roles/Operation' , data);
  }

  delete(id: string): Observable<any> {
    return this.http.delete('Roles/delete/' + id);
  }

}
