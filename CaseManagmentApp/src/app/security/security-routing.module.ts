import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{UsersComponent} from './users/users.component';
import { RoleComponent } from './role/role.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'role', component: RoleComponent },
  { path: 'userProfile', component: UserProfileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule { }
