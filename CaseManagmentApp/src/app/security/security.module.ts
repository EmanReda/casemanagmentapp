import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SecurityRoutingModule } from './security-routing.module';
import { UsersComponent } from './users/users.component';
import {UserOperationComponent} from './user-operation/user-operation.component';
import { RoleComponent } from './role/role.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
@NgModule({
  declarations: [UsersComponent,
    UserOperationComponent,
    RoleComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    SecurityRoutingModule,
    SharedModule.forRoot(),
  ]
})
export class SecurityModule { }
