import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { UserProfileModel } from '../users/users.model';
import { UsersService } from '../users/users.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-user-operation',
  templateUrl: './user-operation.component.html'
})
export class UserOperationComponent implements OnInit, OnChanges {
  @Input() model: UserProfileModel;
  @Output() closeModal = new EventEmitter<any>();
  checkMail: boolean;
  checkName: boolean;
  hidden: boolean;
  oldUserName: string;
  oldEmail: string;

  constructor(private userSer: UsersService,
              public global: GlobalService,
              private translate: TranslateService) {
    this.checkMail = false;
    this.checkName = false;
  }

  ngOnInit() {
    
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    this.oldUserName = this.model.userName;
    this.oldEmail = this.model.email;
  }

  checkEmail(email) {
    if (!(email == null || email == '' || email == this.oldEmail)) {
      this.userSer.checkEmail(email)
        .subscribe(
          (data) => {
            this.checkMail = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  checkUserName(name) {
    if (!(name == null || name == '' || name == this.oldUserName)) {
      this.userSer.checkUserName(name)
        .subscribe(
          (data) => {
            this.checkName = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }


  saveUser() {
    this.userSer.saveProfile(this.model)
      .subscribe((data) => {
        if (data >= 1) {
          this.global.notificationMessage(1);
          this.closeModal.emit('save');
        } else if (data === 0) {
          this.translate.get('login.LoginError')
            .subscribe((response) => {
              const message = response;
              this.global.notificationMessage(4, null, message);
            });
        } else if (data === -1) {
          this.checkMail = false;
          this.translate.get('Users.EmailError')
            .subscribe((value) => {
              const message = value;
              this.global.notificationMessage(4, null, message);
            });
        } else if (data === -2) {
          this.translate.get('Users.UserNameError')
            .subscribe((value) => {
              const message = value;
              this.global.notificationMessage(4, null, message);
            });
        } else if (data === -3) {
          this.translate.get('Users.CheckRoles')
            .subscribe((value) => {
              const message = value;
              this.global.notificationMessage(4, null, message);
            });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
