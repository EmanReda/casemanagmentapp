import { Component } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FilterUser, UserProfileModel } from './users.model';
import { UsersService } from './users.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { UserOperationComponent } from '../user-operation/user-operation.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent  {
  modalRef: BsModalRef;
  model: FilterUser;
  total: number;
  list: any = [];
  userOperation: UserProfileModel;
  userOperationCo: UserOperationComponent;
  reTypePassword: string;
  CheckRetypedPassword: boolean;

  constructor(
    private modalService: BsModalService,
    private translate: TranslateService,
    public global: GlobalService,
    public fixed: FixedService,
    private userSer: UsersService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Users', url: null },
        { name: 'Users.PageHeader', url: '/users/list' }
      ],
      pageHeader: 'Users.PageHeader'
    };
    this.model = new FilterUser();
    this.filterUser();
  }

  openOperation(template: any, model) {
    if (model == null) {
      this.userOperation = new UserProfileModel();
    } else {
      this.userOperation = model;
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  filterUser() {
    this.userSer.getAll(this.model)
      .subscribe((response) => {
        this.total = response.count;
        this.list = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.model.recordPerPage = data.recordPerPage;
    this.model.page = data.currentPage;
    this.filterUser();
  }

  closeUserOperation(type) {
    if (this.modalRef != null) { this.modalRef.hide(); }
    this.filterUser();
  }

  deleteUser(data) {
    this.userSer.deleteUser(data.userId)
      .subscribe((response) => {
        this.filterUser();
        this.global.notificationMessage(1);
      }, (error) => {
        this.global.notificationMessage(4);
      });
  }
}
