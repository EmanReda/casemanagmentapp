export class UserProfileModel {
    constructor(public userId: string = null,
                public userName: string = null,
                public name: string = null,
                public email: string = null,
                public note: string = null,
                public password: string = null,
                public photo: string = null,
                public phoneNumber: string = null
                ) {
    }
}
export class FilterUser {
    constructor(public page: number = 1,
                public recordPerPage: number = 20,
                public userName: string = null,
                public name: string = null,
                public email: string = null,
                public phoneNumber: string = null,
                public sort: number = 1) {
    }
}