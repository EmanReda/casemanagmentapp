import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UsersService {

    constructor(private http: HttpClient) {
    }

    getAll(model): Observable<any> {
        return this.http.post('Users/FliterUser', model);
    }

    saveProfile(model): Observable<any> {
        return this.http.post('Users/SaveProfile', model);
    }

    checkEmail(email): any {
        return this.http.get('Users/CheckEmail?Email=' + email);
    }

    checkUserName(userName): any {
        return this.http.get('Users/CheckUserName?Name=' + userName);
    }

    deleteUser(id): Observable<any> {
        return this.http.get('Users/Delete/' + id);
    }
}
