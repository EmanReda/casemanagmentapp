import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'accountTreeFilter'
})
export class AccountTreeFilter implements PipeTransform {
    transform(items: Array<any>, filterType, fliterValue: Array<any>) {
        if (items == null || filterType == null) {
            return items;
        } else if (filterType === 'parent') {
            return items.filter(item => item.finish == false && item.cash == false
                && item.hasAssistant == false && item.hasCostCenter == false);
        } else if (filterType === 'child') {
            return items.filter(item => item.finish == true || item.cash == true
                && item.hasAssistant == true && item.hasCostCenter == true);
        } else if (filterType === 'master') {
            return items.filter(item => item.finish == true && item.hasAssistant == false);
        } else if (filterType === 'hasAssistant') {
            return items.filter(item => item.finish == true && item.hasAssistant == true);
        }  else if (filterType === 'list') {
            const returnItems = [];
            items.forEach(element => {
                const check = fliterValue.filter(s => s == element.accountTreeId);
                if (check.length !== 0) {
                    returnItems.push(element);
                }
            });
            return returnItems;
        }
    }
}
