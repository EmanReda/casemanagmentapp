import { Component, Input, Output, EventEmitter, SimpleChange, OnChanges, ViewEncapsulation } from '@angular/core';
import { FixedService } from '../../services/fixed.service';
import { GlobalService } from '../../services/global.service';
import { AccountTreeService } from 'src/app/accounting/account-tree/account-tree.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-account-arbor',
  templateUrl: './account-arbor.component.html',
  styleUrls: ['./account-arbor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AccountArborComponent implements OnChanges {
  treeList: any;
  list: any;
  selectedNode: any;
  requestSent = false;
  @Input() refresh: boolean;
  @Input() treeView: boolean;
  @Input() selectedValue: any;
  @Input() disable: boolean;
  @Input() multiple: boolean;
  @Input() filterType: string;
  @Input() fliterValue: Array<any>;
  @Output() selectedValueChanges = new EventEmitter<any>();
  modalRef: BsModalRef;
  @Input() bindLabel: string;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private accountTreeSer: AccountTreeService,
    private modalService: BsModalService) {
    this.getTreeData();
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.refresh != undefined && changes.refresh.currentValue !== changes.refresh.previousValue) {
      this.Reload();
    } else {
      this.getTreeData();
    }
  }

  onNodeSelect(data, type, operation?) {
    if (operation === 'delete') {
      this.selectedValue = null;
      this.selectedNode = null;
      this.selectedValueChanges.emit(null);
    } else {
      if (type === 'tree') {
        this.selectedValueChanges.emit(data.node.value);
      } else if (this.multiple) {
        this.selectedValueChanges.emit(data);
      } else {
        this.selectedValueChanges.emit(this.list.filter(s => s.accountTreeId == data)[0]);
      }
    }
  }

  getTreeData() {
    if (this.fixed.accounting.AccountTree.length === 0 && !this.requestSent) {
      this.requestSent = true;
      this.accountTreeSer.getAll()
        .subscribe((data) => {
          this.list = data;
          this.generateDropdownArray();
          this.fixed.accounting.AccountTree = data;
          if (this.treeView) { this.generateArray(data); }
        }, (data) => {
          this.global.notificationMessage(4);
        });
    } else {
      this.list = this.fixed.accounting.AccountTree;
      this.generateDropdownArray();
      if (this.treeView) { this.generateArray(this.fixed.accounting.AccountTree); }
    }
  }

  generateDropdownArray() {
    if (!(this.list == null || this.list.length === 0)) {
      this.list.forEach(element => {
        element.fullName = element.name;
        element = this.loadFullPath(this.list, element, element.accountTreeParentId);
      });
    }
  }

  loadFullPath(data, element: any, ParentId) {
    if (ParentId != null) {
      const parent = data.filter(s => s.accountTreeId === ParentId)[0];
      if (parent != null) {
        element.fullName = parent.name + ' . ' + element.fullName;
        this.loadFullPath(data, element, parent.accountTreeParentId);
      }
    }
    return element;
  }

  generateArray(data) {
    if (!(data == null || data.length === 0)) {
      this.treeList = [];
      data.forEach(element => {
        const item = {
          label: element.code + ' -- ' + element.name,
          data: element.accountTreeId,
          expandedIcon: 'fa fa-folder-open',
          expanded: (element.accountTreeParentId == null),
          collapsedIcon: 'fa fa-folder',
          parent: element.accountTreeParentId,
          type: element.accountTreeTypeId,
          value: element
        };
        this.treeList.push(item);
      });
      this.treeList = this.loadChildren(this.treeList);
      this.checkNode(this.treeList, Number(this.selectedValue));
      this.listOpened(this.selectedValue);
    }
  }

  loadChildren(arr: any, parent?: any) {
    const children = [];
    arr.forEach(element => {
      if (element.parent == parent) {
        const grandChildren = this.loadChildren(arr, element.data);
        if (grandChildren.length) {
          element.children = grandChildren;
        }
        children.push(element);
      }
    });
    return children;
  }

  listOpened(id) {
    this.list.forEach(element => {
      if (element.accountTreeId === id) {
        this.openNode(this.treeList, element.accountTreeId);
        this.listOpened(element.accountTreeParentId);
      }
    });
  }

  openNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        element.expanded = true;
      }
      if (element.children != null) {
        this.openNode(element.children, value);
      }
    });
  }

  checkNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        this.selectedNode = element;
      }
      if (element.children != null) {
        this.checkNode(element.children, value);
      }
    });
  }

  Reload() {
    this.requestSent = false;
    this.list = [];
    this.fixed.accounting.AccountTree.length = 0;
    this.getTreeData();
  }

  openModal(template: any) {
    if (this.multiple) {
      this.list.forEach(element => {
        if ((this.selectedValue.indexOf(element.accountTreeId)) > -1) {
          element.check = true;
        }
      });
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  checkChanged(event, item) {
    const mm = [];
    this.selectedValue.forEach(element => {
      mm.push(element);
    });
    if (event.currentTarget.checked) {
      mm.push(item.accountTreeId);
    } else {
      for (let i = 0; i < mm.length; i++) {
        if (mm[i] === item.accountTreeId) {
          mm.splice(i, 1);
          break;
        }
      }
    }
    this.selectedValue = mm;
  }
}
