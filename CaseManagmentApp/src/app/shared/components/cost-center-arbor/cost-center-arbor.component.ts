import { Component, ViewEncapsulation, OnChanges, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FixedService } from '../../services/fixed.service';
import { GlobalService } from '../../services/global.service';
import { CostCenterService } from 'src/app/accounting/cost-center/cost-center.service';

@Component({
  selector: 'app-cost-center-arbor',
  templateUrl: './cost-center-arbor.component.html',
  styleUrls: ['./cost-center-arbor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CostCenterArborComponent implements OnChanges {
  treeList: any;
  list: any;
  selectedNode: any;
  requestSent = false;
  @Input() refresh: boolean;
  @Input() treeView: boolean;
  @Input() selectedValue: number;
  @Input() disable: boolean;
  @Output() selectedValueChanges = new EventEmitter<any>();
  modalRef: BsModalRef;
  @Input() bindLabel: string;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private costCenterSer: CostCenterService,
    private modalService: BsModalService) {
    this.getTreeData();
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.refresh != undefined && changes.refresh.currentValue !== changes.refresh.previousValue) {
      this.Reload();
    } else {
      this.getTreeData();
    }
  }

  onNodeSelect(data, type, operation?) {
    if (operation === 'delete') {
      this.selectedValue = null;
      this.selectedNode = null;
      this.selectedValueChanges.emit(null);
    } else {
      if (type === 'tree') {
        this.selectedValueChanges.emit(data.node.value);
      } else {
        this.selectedValueChanges.emit(this.list.filter(s => s.costCenterId == data)[0]);
      }
    }
  }

  getTreeData() {
    if (this.fixed.accounting.CostCenter.length === 0 && !this.requestSent) {
      this.requestSent = true;
      this.costCenterSer.getAll()
        .subscribe((data) => {
          this.list = data;
          this.generateDropdownArray();
          this.fixed.accounting.CostCenter = data;
          if (this.treeView) { this.generateArray(data); }
        }, (data) => {
          this.global.notificationMessage(4);
        });
    } else {
      this.list = this.fixed.accounting.CostCenter;
      this.generateDropdownArray();
      if (this.treeView) { this.generateArray(this.fixed.accounting.CostCenter); }
    }
  }

  generateDropdownArray() {
    if (!(this.list == null || this.list.length === 0)) {
      this.list.forEach(element => {
        element.fullName = element.name;
        element = this.loadFullPath(this.list, element, element.costCenterParentId);
      });
    }
  }

  loadFullPath(data, element: any, ParentId) {
    if (ParentId != null) {
      const parent = data.filter(s => s.costCenterId === ParentId)[0];
      if (parent != null) {
        element.fullName = parent.name + ' . ' + element.fullName;
        this.loadFullPath(data, element, parent.costCenterParentId);
      }
    }
    return element;
  }

  generateArray(data) {
    if (!(data == null || data.length === 0)) {
      this.treeList = [];
      data.forEach(element => {
        const item = {
          label: element.code + ' -- ' + element.name,
          data: element.costCenterId,
          expandedIcon: 'fa fa-folder-open',
          expanded: (element.costCenterParentId == null),
          collapsedIcon: 'fa fa-folder',
          parent: element.costCenterParentId,
          value: element
        };
        this.treeList.push(item);
      });
      this.treeList = this.loadChildren(this.treeList);
      this.checkNode(this.treeList, Number(this.selectedValue));
      this.listOpened(this.selectedValue);
    }
  }

  loadChildren(arr: any, parent?: any) {
    const children = [];
    arr.forEach(element => {
      if (element.parent == parent) {
        const grandChildren = this.loadChildren(arr, element.data);
        if (grandChildren.length) {
          element.children = grandChildren;
        }
        children.push(element);
      }
    });
    return children;
  }

  listOpened(id) {
    this.list.forEach(element => {
      if (element.costCenterId === id) {
        this.openNode(this.treeList, element.costCenterId);
        this.listOpened(element.costCenterParentId);
      }
    });
  }

  openNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        element.expanded = true;
      }
      if (element.children != null) {
        this.openNode(element.children, value);
      }
    });
  }

  checkNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        this.selectedNode = element;
      }
      if (element.children != null) {
        this.checkNode(element.children, value);
      }
    });
  }

  Reload() {
    this.requestSent = false;
    this.list = [];
    this.fixed.accounting.CostCenter.length = 0;
    this.getTreeData();
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
}
