import { Component, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FilterEmployee } from '../../../hr/employee/employee.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { EmployeeService } from '../../../hr/employee/employee.service';

@Component({
  selector: 'app-employee-shared',
  templateUrl: './employee-shared.component.html'
})
export class EmployeeSharedComponent implements AfterViewInit {
  empList: any = [];
  modalRef: BsModalRef;
  search: any;
  model: FilterEmployee;
  @Input() selectedValue: any;
  @Input() isSalesRep: any;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    public employeeSer: EmployeeService,
    private modalService: BsModalService) {
    this.model = new FilterEmployee();
  }

  ngAfterViewInit() {
    this.getAll();
  }

  getAll() {
    this.model.isSalesRep = this.isSalesRep;
    this.employeeSer.getAll(this.model)
      .subscribe((response) => {
        if (response.data.length > 0) {
          this.empList = this.empList.concat(response.data);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  searchFunction(term) {
    this.model.name = term;
    this.model.page = 1;
    this.empList.length = 0;
    this.getAll();
  }

  customSearchFn(term: string, item: any) {
    return item;
  }

  outputFunction(item) {
    this.selectedValueChanges.emit(item);
  }

}
