import { Pipe, PipeTransform } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Pipe({
    name: 'moduleTreeFilter'
})
export class ModuleTreeFilter implements PipeTransform {
    constructor(public global: GlobalService) {
    }
    transform(items: Array<any>, filterType, fliterValue: Array<any>) {
        if (items == null || filterType == null) {
            return items;
        } else if (filterType === 'parent') {
            return items.filter(item => item.final == false);
        } else if (filterType === 'child') {
            return items.filter(item => item.final == true);
        } else if (filterType === 'node') {
            const list = this.global.treeChild(items, 'moduleTreeId', 'moduleParentId', fliterValue);
            const returnItems = [];
            items.forEach(element => {
                if (list.indexOf(element.moduleTreeId) > -1) {
                    returnItems.push(element);
                }
            });
            return returnItems;
        } else if (filterType === 'childNode') {
            const list = this.global.treeChild(items, 'moduleTreeId', 'moduleParentId', fliterValue);
            const returnItems = [];
            items.forEach(element => {
                if (list.indexOf(element.moduleTreeId) > -1) {
                    returnItems.push(element);
                }
            });
            return returnItems.filter(item => item.final == true);
        }
    }
}
