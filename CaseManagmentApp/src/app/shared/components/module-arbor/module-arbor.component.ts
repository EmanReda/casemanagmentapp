import { Component, ViewEncapsulation, OnChanges, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { FixedService } from '../../services/fixed.service';
import { GlobalService } from '../../services/global.service';
import { ModuleTreeService } from 'src/app/accounting/module-tree/module-tree.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-module-arbor',
  templateUrl: './module-arbor.component.html',
  styleUrls: ['./module-arbor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModuleArborComponent implements OnChanges {
  treeList: any;
  list: any;
  selectedNode: any;
  requestSent = false;
  @Input() refresh: boolean;
  @Input() treeView: boolean;
  @Input() selectedValue: number;
  @Input() disable: boolean;
  @Input() filterType: string;
  @Input() fliterValue: Array<any>;
  @Output() selectedValueChanges = new EventEmitter<any>();
  modalRef: BsModalRef;
  @Input() bindLabel: string;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private moduleTreeSer: ModuleTreeService,
    private modalService: BsModalService) {
    this.getTreeData();
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.refresh != undefined && changes.refresh.currentValue !== changes.refresh.previousValue) {
      this.Reload();
    } else {
      this.getTreeData();
    }
  }

  onNodeSelect(data, type, operation?) {
    if (operation === 'delete') {
      this.selectedValue = null;
      this.selectedNode = null;
      this.selectedValueChanges.emit(null);
    } else {
      if (type === 'tree') {
        this.selectedValueChanges.emit(data.node.value);
      } else {
        this.selectedValueChanges.emit(this.list.filter(s => s.moduleTreeId == data)[0]);
      }
    }
  }

  getTreeData() {
    if (this.fixed.accounting.ModuleTree.length === 0 && !this.requestSent) {
      this.requestSent = true;
      this.moduleTreeSer.getAll()
        .subscribe((data) => {
          this.list = data;
          this.generateDropdownArray();
          this.fixed.accounting.ModuleTree = data;
          if (this.treeView) { this.generateArray(data); }
        }, (data) => {
          this.global.notificationMessage(4);
        });
    } else {
      this.list = this.fixed.accounting.ModuleTree;
      this.generateDropdownArray();
      if (this.treeView) { this.generateArray(this.fixed.accounting.ModuleTree); }
    }
  }

  generateDropdownArray() {
    if (!(this.list == null || this.list.length === 0)) {
      this.list.forEach(element => {
        element.fullName = element.name;
        element = this.loadFullPath(this.list, element, element.moduleParentId);
      });
    }
  }

  loadFullPath(data, element: any, ParentId) {
    if (ParentId != null) {
      const parent = data.filter(s => s.moduleTreeId === ParentId)[0];
      if (parent != null) {
        element.fullName = parent.name + ' . ' + element.fullName;
        this.loadFullPath(data, element, parent.moduleParentId);
      }
    }
    return element;
  }

  generateArray(data) {
    if (!(data == null || data.length === 0)) {
      this.treeList = [];
      data.forEach(element => {
        const item = {
          label: element.code + ' -- ' + element.name,
          data: element.moduleTreeId,
          expandedIcon: 'fa fa-folder-open',
          expanded: (element.moduleParentId == null),
          collapsedIcon: 'fa fa-folder',
          parent: element.moduleParentId,
          value: element
        };
        this.treeList.push(item);
      });
      this.treeList = this.loadChildren(this.treeList);
      this.checkNode(this.treeList, Number(this.selectedValue));
      this.listOpened(this.selectedValue);
    }
  }

  loadChildren(arr: any, parent?: any) {
    const children = [];
    arr.forEach(element => {
      if (element.parent == parent) {
        const grandChildren = this.loadChildren(arr, element.data);
        if (grandChildren.length) {
          element.children = grandChildren;
        }
        children.push(element);
      }
    });
    return children;
  }

  listOpened(id) {
    this.list.forEach(element => {
      if (element.moduleTreeId === id) {
        this.openNode(this.treeList, element.moduleTreeId);
        this.listOpened(element.moduleParentId);
      }
    });
  }

  openNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        element.expanded = true;
      }
      if (element.children != null) {
        this.openNode(element.children, value);
      }
    });
  }

  checkNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        this.selectedNode = element;
      }
      if (element.children != null) {
        this.checkNode(element.children, value);
      }
    });
  }

  Reload() {
    this.requestSent = false;
    this.list = [];
    this.fixed.accounting.ModuleTree.length = 0;
    this.getTreeData();
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
}
