import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FilterVendor } from '../../../vendor/vendor/vendor.model';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { VendorService } from '../../../vendor/vendor.service';

@Component({
  selector: 'app-vendor-shared',
  templateUrl: './vendor-shared.component.html'
})
export class VendorSharedComponent {
  vendors: any = [];
  modalRef: BsModalRef;
  search: any;
  model: FilterVendor;
  @Input() selectedValue: any;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    public vendorSer: VendorService,
    private modalService: BsModalService) {
    this.model = new FilterVendor();
    this.getAll();
  }

  getAll() {
    this.vendorSer.getAll(this.model)
      .subscribe((response) => {
        if (response.data.length > 0) {
          this.vendors = this.vendors.concat(response.data);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  searchFunction(term) {
    this.model.name = term;
    this.model.page = 1;
    this.vendors.length = 0;
    this.getAll();
  }

  customSearchFn(term: string, item: any) {
    return item;
  }

  outputFunction(item) {
    this.selectedValueChanges.emit(item);
  }
}
