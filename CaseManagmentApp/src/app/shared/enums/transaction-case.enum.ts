export enum TransactionCase {
    Temporary = 1,
    Unposted = 2,
    Posted = 3
}
