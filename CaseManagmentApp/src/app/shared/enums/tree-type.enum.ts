export enum TreeType {
    AccountTree = 1,
    CostCenter = 2,
    ModuleTree = 3
}
