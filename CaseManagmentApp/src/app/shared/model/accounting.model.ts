import { AccountingSettingModel } from 'src/app/accounting/accounting-setting/account-setting.model';

export class Accounting {
    constructor(
        public AccountTreeType: any = [],
        public ModuleTree: any = [],
        public AccountTree: any = [],
        public CostCenter: any = [],
        public TransactionCase: any = [],
        public AccountingSetting: AccountingSettingModel = new AccountingSettingModel()) { }
}
