export class Message {
    constructor(
        public type: number = null,
        public title: string = null,
        public message: string = null,
        public position: any = null) { }
}
