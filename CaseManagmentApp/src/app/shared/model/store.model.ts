export class Store {
    constructor(
        public CategoryTree: any = [],
        public PriceList: any = [],
        public PaymentMethod :any =[],
        public Stock:any=[] ,
        public UOM : any=[]) { }
}
