export class SubHeader {
  constructor(
    public display: boolean = true,
    public data: any = [],
    public pageHeader: string = '') {
  }
}
