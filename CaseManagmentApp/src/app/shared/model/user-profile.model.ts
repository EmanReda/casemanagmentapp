export class UserProfile {
    constructor(public userId: string = '',
                public email: string = '',
                public emailConfirmed: boolean = false,
                public phoneNumber: string = '',
                public userName: string = '',
                public name: string = '',
                public photo: any = null,
                public note: string = '',
                public companyName:string='',
                public createdBy: string = null,
                public createdDate: any = null,
                public modifiedBy: string = null,
                public modifiedDate: any = null,
                public PhoneNumberConfirmed: boolean = false,
                public roleList: any = [],
                public realData: boolean = false) {
    }
}
