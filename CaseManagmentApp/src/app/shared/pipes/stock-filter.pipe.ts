import { Pipe, PipeTransform } from '@angular/core';
import { StockModel } from 'src/app/store/stock/stock-operation/stock-operation.model';

@Pipe({
    name: 'stockFilter'
})

export class StockFilterPipe implements PipeTransform {
    transform(list: StockModel[], stockId: number): StockModel[] {
        if (!list || !stockId) {
            return list;
        } else {
            return list.filter(s => s.stockId !== stockId);
        }
    }
}
