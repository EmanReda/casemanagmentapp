import { Injectable } from '@angular/core';
import { UserProfile } from '../model/user-profile.model';
import { SubHeader } from '../model/sub-header.model';
import { Accounting } from '../model/accounting.model';
import { Store } from '../model/store.model';
import { Client } from '../model/client.model';

@Injectable({ providedIn: 'root' })
export class FixedService {
  public themeLoaded: boolean;
  public activeLang: any = {};
  public appLanguages: any = [];
  public userProfile: UserProfile;
  public subHeader: SubHeader;
  public menuList: any;
  public recordPerPage: any = [];
  public KTAppOptions: any;
  public datePickerConfig: any = {};
  public dateTimePickerConfig: any = {};
  public accounting: Accounting;
  public store: Store;
  public currentURL: string;
  public client: Client;
  public today: any;

  constructor() {
    this.accounting = new Accounting();
    this.store = new Store();
    this.userProfile = new UserProfile();
    this.subHeader = new SubHeader();
    this.initialAppLang();
    this.themeLoaded = false;
    this.today = new Date();
    this.recordPerPage = [
      { label: '10', value: 10 },
      { label: '20', value: 20 },
      { label: '30', value: 30 },
      { label: '40', value: 40 },
      { label: '50', value: 50 },
      { label: '100', value: 100 },
      { label: '150', value: 150 },
      { label: 'Common.All', value: 0 }
    ];
    this.KTAppOptions = {
      colors: {
        state: {
          brand: '#5d78ff',
          dark: '#282a3c',
          light: '#ffffff',
          primary: '#5867dd',
          success: '#34bfa3',
          info: '#36a3f7',
          warning: '#ffb822',
          danger: '#fd3995'
        },
        base: {
          label: ['#c5cbe3', '#a1a8c3', '#3d4465', '#3e4466'],
          shape: ['#f0f3ff', '#d9dffa', '#afb4d4', '#646c9a']
        }
      }
    };
    this.datePickerConfig = {
      dateInputFormat: 'YYYY/MM/DD',
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false
    };
    this.dateTimePickerConfig = {
      dateInputFormat: 'YYYY/MM/DD , hh:mm a',
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false
    };
  }

  initialAppLang() {
    this.appLanguages = [
      { languageId: 1, isRTL: true, code: 'ar', name: 'عربي', dir: 'rtl', icon: 'flag-icon-sa' },
      { languageId: 2, isRTL: false, code: 'en', name: 'English', dir: 'ltr', icon: 'flag-icon-uk' }
    ];
  }
}
