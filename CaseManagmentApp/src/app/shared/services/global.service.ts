import { Injectable } from '@angular/core';
import { FixedService } from './fixed.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { forkJoin, BehaviorSubject } from 'rxjs';
import { BsLocaleService, document } from 'ngx-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';
import { ResponseEnum } from '../enums/response.enum';
import { Message } from '../model/message.model';

@Injectable({ providedIn: 'root' })
export class GlobalService {
  toastrOptions: any;
  appLangChanged: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  filesLoaded: number;
  oldMessage: Message;

  constructor(
    private translate: TranslateService,
    private toastrSer: ToastrService,
    private localStorage: LocalStorage,
    private cookieService: CookieService,
    public localeService: BsLocaleService,
    public fixed: FixedService) {
    this.oldMessage = new Message();
    this.localStorage.getItem<any>('appLang')
      .subscribe((data) => {
        const lang = (document.getElementsByTagName('html')[0] as HTMLHtmlElement).getAttribute('lang');
        this.fixed.activeLang = (data === null) ? this.fixed.appLanguages[1] : data;
        if (lang != this.fixed.activeLang.code) {
          this.themeSettings(this.fixed.activeLang);
        }
        this.setLang(this.fixed.activeLang.code);
      });
  }

  setLang(lang) {
    this.translate.addLangs(['en', 'ar']);
    this.translate.setDefaultLang('en');
    this.translate.use(lang);
  }

  notificationMessage(type?: number, title?: string, message?: string, position?) {
    this.toastrOptions = {
      positionClass: (position == null) ?
        (this.fixed.activeLang.isRTL) ? 'toast-top-left' : 'toast-top-right' : position,
      progressBar: true,
      progressAnimation: 'increasing',
      closeButton: true
    };
    let severity;
    if (type === 1) {
      forkJoin([this.translate.get('Message.Successful'), this.translate.get('Message.SuccessfulText')])
        .subscribe(results => {
          title = title ? title : results[0];
          message = message ? message : results[1];
          severity = 'toast-success';
          if (this.oldMessage.type == type && this.oldMessage.title == title
            && this.oldMessage.message == message && this.oldMessage.position == position) {
            return;
          } else {
            this.oldMessage = new Message(type, title, message, position);
            this.toastrSer.show(message, title, this.toastrOptions, severity);
          }
        });
    } else if (type === 2) {
      forkJoin([this.translate.get('Message.InProgress'), this.translate.get('Message.InProgressText')])
        .subscribe(results => {
          title = title ? title : results[0];
          message = message ? message : results[1];
          severity = 'toast-info';
          if (this.oldMessage.type == type && this.oldMessage.title == title
            && this.oldMessage.message == message && this.oldMessage.position == position) {
            return;
          } else {
            this.oldMessage = new Message(type, title, message, position);
            this.toastrSer.show(message, title, this.toastrOptions, severity);
          }
        });
    } else if (type === 3) {
      forkJoin([this.translate.get('Message.Warning'), this.translate.get('Message.WarningText')])
        .subscribe(results => {
          title = title ? title : results[0];
          message = message ? message : results[1];
          severity = 'toast-warning';
          if (this.oldMessage.type == type && this.oldMessage.title == title
            && this.oldMessage.message == message && this.oldMessage.position == position) {
            return;
          } else {
            this.oldMessage = new Message(type, title, message, position);
            this.toastrSer.show(message, title, this.toastrOptions, severity);
          }
        });
    } else if (type === 4) {
      forkJoin([this.translate.get('Message.Unsuccessful'), this.translate.get('Message.UnsuccessfulText')])
        .subscribe(results => {
          title = title ? title : results[0];
          message = message ? message : results[1];
          severity = 'toast-error';
          if (this.oldMessage.type == type && this.oldMessage.title == title
            && this.oldMessage.message == message && this.oldMessage.position == position) {
            return;
          } else {
            this.oldMessage = new Message(type, title, message, position);
            this.toastrSer.show(message, title, this.toastrOptions, severity);
          }
        });
    }
  }

  themeSettings(appLang) {
    this.fixed.activeLang = appLang;
    const html = document.getElementsByTagName('html')[0] as HTMLHtmlElement;
    html.setAttribute('lang', this.fixed.activeLang.code);
    const body = document.getElementsByTagName('body')[0] as HTMLBodyElement;
    body.setAttribute('dir', appLang.dir);

    (document.getElementById('style') as HTMLAnchorElement).href =
      'assets/css/style.bundle.' + appLang.dir + '.min.css';
    (document.getElementById('header') as HTMLAnchorElement).href =
      'assets/css/header.light.' + appLang.dir + '.min.css';
    (document.getElementById('menu') as HTMLAnchorElement).href =
      'assets/css/menu.light.' + appLang.dir + '.min.css';
    (document.getElementById('brand') as HTMLAnchorElement).href =
      'assets/css/brand.dark.' + appLang.dir + '.min.css';
    (document.getElementById('aside') as HTMLAnchorElement).href =
      'assets/css/aside.dark.' + appLang.dir + '.min.css';
    (document.getElementById('custome') as HTMLAnchorElement).href =
      'assets/css/custome.css';

    this.localStorage.setItem('appLang', this.fixed.activeLang)
      .subscribe(() => { });
    this.setLang(this.fixed.activeLang.code);
    this.localeService.use(this.fixed.activeLang.code);
    this.appLangChanged.next(this.fixed.activeLang);
  }

  getUserId() {
    if (this.cookieService.get('user_data')) {
      return JSON.parse(this.cookieService.get('user_data')).Id;
    } else {
      return null;
    }
  }

  loading(type?: boolean) {
    const body = document.getElementById('loading') as HTMLDivElement;
    (type === true) ? body.classList.add('kt-page--loading') : body.classList.remove('kt-page--loading');
  }

  // image(src, type) {
  //   return (src == null) ? 'assets/img/' + type + '.jpg' :
  //     (src.indexOf('/' + type) > -1 || src.indexOf('data:image/png;base64') > -1) ? src :
  //       environment.serverImg + '/' + type + '/' + src;
  // }

  OnPressFnction(event, type) {
    // type 0 as Mobiles, 1 as number, 2 as Email, 3 as English
    if (type === 0) {
      return event.charCode >= 48 && event.charCode <= 57 || event.charCode === 43;
    } else if (type === 1) {
      return event.charCode >= 48 && event.charCode <= 57;
    } else if (type === 2) {
      return event.charCode >= 65 && event.charCode <= 90 ||
        event.charCode >= 97 && event.charCode <= 122 ||
        event.charCode >= 48 && event.charCode <= 57 || event.charCode === 64 || event.charCode === 46;
    } else if (type === 3) {
      return event.charCode >= 65 && event.charCode <= 90 ||
        event.charCode >= 97 && event.charCode <= 122 ||
        event.charCode >= 48 && event.charCode <= 57 || event.charCode === 32;
    }
  }

  formatDateTime(date: Date) {
    if (date == null || typeof (date) === 'string') { return date; }
    const year = (String(date.getFullYear()).length === 1) ? '0' + date.getFullYear() : date.getFullYear();
    const month = (String(date.getMonth() + 1).length === 1) ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    const day = (String(date.getDate()).length === 1) ? '0' + date.getDate() : date.getDate();
    const hour = (String(date.getHours()).length === 1) ? '0' + date.getHours() : date.getHours();
    const min = (String(date.getMinutes()).length === 1) ? '0' + date.getMinutes() : date.getMinutes();
    return year + '-' + month + '-' + day + 'T' + hour + ':' + min + ':00';
  }

  formatDate(date: Date) {
    if (date == null || typeof (date) === 'string') { return date; }
    const year = (String(date.getFullYear()).length === 1) ? '0' + date.getFullYear() : date.getFullYear();
    const month = (String(date.getMonth() + 1).length === 1) ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    const day = (String(date.getDate()).length === 1) ? '0' + date.getDate() : date.getDate();
    return year + '-' + month + '-' + day;
  }

  formatTime(date: Date) {
    if (date == null || typeof (date) === 'string') { return date; }
    const hour = (String(date.getHours()).length === 1) ? '0' + date.getHours() : date.getHours();
    const min = (String(date.getMinutes()).length === 1) ? '0' + date.getMinutes() : date.getMinutes();
    return hour + ':' + min + ':00';
  }

  generateMenu(activeUrl?: string, parentTag?) {
    this.fixed.menuList = [
      {
        label: 'Common.Home',
        icon: 'la la-home',
        routerLink: '/home',
        visible: true,
        active: (activeUrl === '/home' || activeUrl === '/'),
        children: []
      },
      {
        label: 'Menu.Accounting',
        icon: 'la la-money',
        routerLink: 'accounting',
        visible: true,
        active: (parentTag[1] === 'accounting'),
        children: [
          {
            label: 'Menu.GeneralLedger',
            icon: 'flaticon-doc',
            routerLink: 'general-ledger',
            visible: true,
            active: (parentTag[2] === 'general-ledger'),
            children: [
              {
                label: 'AccountTree.PageHeader',
                icon: 'la la-tree',
                routerLink: '/accounting/general-ledger/account-tree',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/account-tree'),
                children: []
              },
              {
                label: 'ModuleTree.PageHeader',
                icon: 'la la-anchor',
                routerLink: '/accounting/general-ledger/module-tree',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/module-tree'),
                children: []
              },
              {
                label: 'Transaction.TransactionList',
                icon: 'la la-exchange',
                routerLink: '/accounting/general-ledger/transaction-list',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/transaction-list'),
                children: []
              },
              {
                label: 'Accounting-setting.PageHeader',
                icon: 'la la-cogs',
                routerLink: '/accounting/general-ledger/setting',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/setting'),
                children: []
              },
              {
                label: 'FinantialYear.PageHeader',
                icon: 'flaticon-bag',
                routerLink: '/accounting/general-ledger/finantial',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/finantial'),
                children: []
              },
              {
                label: 'CostCenterTree.PageHeader',
                icon: 'flaticon2-bar-chart',
                routerLink: '/accounting/general-ledger/cost-center',
                visible: true,
                active: (activeUrl === '/accounting/general-ledger/cost-center'),
                children: []
              },
            ]
          },
          {
            label: 'Banks.PageHeader',
            icon: 'la la-bank',
            routerLink: '/accounting/banks',
            visible: true,
            active: (activeUrl === '/accounting/banks'),
            children: []
          },
          {
            label: 'Asset.PageHeader',
            icon: 'la la-building',
            routerLink: '/accounting/asset',
            visible: true,
            active: (activeUrl === '/accounting/asset'),
            children: []
          }
        ]
      },
      {
        label: 'Menu.Clients',
        icon: 'flaticon-users-1',
        routerLink: 'client',
        visible: true,
        active: (parentTag[1] === 'client'),
        children: [
          {
            label: 'Client.PageHeader',
            icon: 'flaticon2-list',
            routerLink: '/client/list',
            visible: true,
            active: (activeUrl === '/client/list'),
            children: []
          }
        ]
      },
      {
        label: 'Menu.Vendor',
        icon: 'la la-users',
        routerLink: 'vendor',
        visible: true,
        active: (parentTag[1] === 'vendor'),
        children: [
          {
            label: 'Vendor.PageHeader',
            icon: 'flaticon2-list',
            routerLink: '/vendor/list',
            visible: true,
            active: (activeUrl === '/vendor/list'),
            children: []
          }
        ]
      },
      {
        label: 'Menu.HR',
        icon: 'flaticon-user',
        routerLink: 'hr',
        visible: true,
        active: (parentTag[1] === 'hr'),
        children: [
          {
            label: 'Employee.PageHeader',
            icon: 'la la-list',
            routerLink: '/hr/employee-list',
            visible: true,
            active: (activeUrl === '/hr/employee-list'),
            children: []
          }
        ]
      },
      {
        label: 'Menu.Store',
        icon: 'flaticon-layer',
        routerLink: 'store',
        visible: true,
        active: (parentTag[1] === 'store'),
        children: [
          {
            label: 'Category.PageHeader',
            icon: 'la la-inbox',
            routerLink: '/store/category',
            visible: true,
            active: (activeUrl === '/store/category'),
            children: []
          },
          {
            label: 'PaymentMethod.PageHeader',
            icon: 'la la-money',
            routerLink: '/store/payment-method',
            visible: true,
            active: (activeUrl === '/store/payment-method'),
            children: []
          },
          {
            label: 'Stock.List',
            icon: 'la la-database',
            routerLink: '/store/stock/list',
            visible: true,
            active: (activeUrl === '/store/stock/list'),
            children: []
          },
          {
            label: 'Product.ProductList',
            icon: 'flaticon-folder-1',
            routerLink: '/store/product/list',
            visible: true,
            active: (activeUrl === '/store/product/list'),
            children: []
          },
          {
            label: 'StockProduct.List',
            icon: 'flaticon-list',
            routerLink: '/store/stock/product-list',
            visible: true,
            active: (activeUrl === '/store/stock/product-list'),
            children: []
          },
          {
            label: 'StockOpening.StockOpening',
            icon: 'flaticon-open-box',
            routerLink: '/store/stock-opening/list',
            visible: true,
            active: (activeUrl === '/store/stock-opening/list'),
            children: []
          },
          {
            label: 'StockInDocument.StockInDocument',
            icon: 'flaticon-interface-10',
            routerLink: '/store/stock-in-document/list',
            visible: true,
            active: (activeUrl === '/store/stock-in-document/list'),
            children: []
          },
          {
            label: 'StockOutDocument.StockOutDocument',
            icon: 'flaticon-logout',
            routerLink: '/store/stock-out-document/list',
            visible: true,
            active: (activeUrl === '/store/stock-out-document/list'),
            children: []
          },
          {
            label: 'StockTransferDocument.StockTransferDocument',
            icon: 'la la-refresh',
            routerLink: '/store/stock-transfer-document/list',
            visible: true,
            active: (activeUrl === '/store/stock-transfer-document/list'),
            children: []
          },
          {
            label: 'StockReceiveVoucher.List',
            icon: 'flaticon2-shopping-cart-1',
            routerLink: '/store/stock-receive-voucher/list',
            visible: true,
            active: (activeUrl === '/store/stock-receive-voucher/list'),
            children: []
          },
          {
            label: 'StockReturnVoucher.List',
            icon: 'flaticon-reply',
            routerLink: '/store/stock-return-voucher/list',
            visible: true,
            active: (activeUrl === '/store/stock-return-voucher/list'),
            children: []
          },
          {
            label: 'Payable.List',
            icon: 'la-money',
            routerLink: '/store/payable/list',
            visible: true,
            active: (activeUrl === '/store/payable/list'),
            children: []
          },
          {
            label: 'Incentive.Incentive',
            icon: 'flaticon2-gift',
            routerLink: '/store/incentive/list',
            visible: true,
            active: (activeUrl === '/store/incentive/list'),
            children: []
          },
          {
            label: 'Invoice.List',
            icon: 'la la-level-up',
            routerLink: '/store/i/invoice/list',
            visible: true,
            active: (activeUrl === '/store/i/invoice/list'),
            children: []
          },
          {
            label: 'Order.List',
            icon: 'flaticon2-box-1',
            routerLink: '/store/i/order/list',
            visible: true,
            active: (activeUrl === '/store/i/order/list'),
            children: []
          },
          {
            label: 'Return.List',
            icon: 'flaticon-download',
            routerLink: '/store/return/list',
            visible: true,
            active: (activeUrl === '/store/return/list'),
            children: []
          },
          {
            label: 'Receivable.List',
            icon: 'la-money',
            routerLink: '/store/receivable/list',
            visible: true,
            active: (activeUrl === '/store/receivable/list'),
            children: []
          }
        ]
      },
      {
        label: 'Menu.Maintenance',
        icon: 'flaticon2-gear',
        routerLink: 'maintenance',
        visible: true,
        active: (parentTag[1] === 'maintenance'),
        children: [
          {
            label: 'Center.PageHeader',
            icon: 'flaticon-buildings',
            routerLink: '/maintenance/center/list',
            visible: true,
            active: (activeUrl === '/maintenance/center/list'),
            children: []
          },
          {
            label: 'MaintenanceOrder.PageHeader',
            icon: 'flaticon-buildings',
            routerLink: '/maintenance/order/list',
            visible: true,
            active: (activeUrl === '/maintenance/order/list'),
            children: []
          }
        ]
      }
    ];
  }

  toTop() {
    const top = (document.getElementById('kt_scrolltop') as HTMLSpanElement);
    if (top != null) {
      top.click();
    }
  }

  treeChild(list, accessId, accessParent, children) {
    for (let i = 0; i < children.length; i++) {
      children = children
        .concat(list.filter(s => s[accessParent] == children[i])
          .map((s) => {
            return s[accessId];
          }));
    }
    return children;
  }

  openReport(response: any) {
    // if (response.type === ResponseEnum.Success) {
    //   this.notificationMessage(1);
    //   window.open(environment.reportUrl + '/' + response.data, '_blank');
    // }
    if (response.name == null) {
      this.notificationMessage(response.type);
    } else {
      this.translate.get(response.name)
        .subscribe(
          (val) => {
            this.notificationMessage(response.type, null, val);
          });
    }
  }
}
