import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { tap } from 'rxjs/operators';
import { catchError } from 'rxjs/internal/operators';
// import { LoginService } from '../../login/login.service';
import { GlobalService } from './global.service';
import { FixedService } from './fixed.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable({ providedIn: 'root' })
export class Interceptor implements HttpInterceptor {
  private newToken = '';

  constructor(
              // private loginSer: LoginService,
              private cookieService: CookieService,
              private translate: TranslateService,
              public fixed: FixedService,
              public global: GlobalService) {
  }

  private applyCredentials = (req) => {
    return req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + this.newToken)
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.global.loading(true);
    if (req.url.indexOf('i18n') > -1) {
      return next.handle(this.applyCredentials(req))
        .pipe(
          catchError((error) => {
            this.global.loading(false);
            return throwError(error);
          }),
          tap(event => {
            this.global.loading(false);
          }, err => {
            this.global.loading(false);
          })
        );
    } else if (req.url.indexOf('token') > -1) {
      return next.handle(req)
        .pipe(
          catchError((error) => {
            return throwError(error);
          }),
          tap(event => {
          }, err => {
          })
        );
    } else {
      // req = req.clone({ url: environment.serverUrl + req.url });
      if (this.cookieService.check('access_token')) {
        this.newToken = this.cookieService.get('access_token');
      } else {
        // if (this.cookieService.check('refresh_token')) {
        //   this.loginSer.obtainRefreshToken()
        //     .subscribe((data: any) => {
        //       this.loginSer.saveToken(data);
        //       this.newToken = data.access_token;
        //     });
        // } else {
        //   this.newToken = '';
        //   this.loginSer.logout();
        // }
      }
      return next.handle(this.applyCredentials(req))
        .pipe(
          catchError((error) => {
            this.global.loading(false);
            return throwError(error);
          }),
          tap(event => {
            if (event instanceof HttpResponse) {
              this.global.loading(false);
            }
          }, err => {
            if (err instanceof HttpErrorResponse) {
              this.global.loading(false);
              if (err.status === 401) {
                this.translate.get('Account.Logged')
                  .subscribe((data) => {
                    const message = data;
                    this.global.notificationMessage(4, null, message);
                  });
                // this.loginSer.logout();
              }
            }
          })
        );
    }
  }
}
