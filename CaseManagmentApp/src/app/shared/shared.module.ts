import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FixedService } from './services/fixed.service';
import { CookieService } from 'ngx-cookie-service';
import { defineLocale, arLocale, BsLocaleService, BsDatepickerModule, TooltipModule, BsModalService, ModalModule } from 'ngx-bootstrap';
import { Interceptor } from './services/interceptor.service';
import arSaLocale from '@angular/common/locales/ar-SA';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { PaginationComponent } from './components/pagination/pagination.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { TreeModule } from 'primeng/tree';
import { GridFilterPipe } from './pipes/grid-Filter.pipe';
import { StockFilterPipe} from './pipes/stock-filter.pipe';
import { AccountArborComponent } from './components/account-arbor/account-arbor.component';
import { ModuleArborComponent } from './components/module-arbor/module-arbor.component';
import { CostCenterArborComponent } from './components/cost-center-arbor/cost-center-arbor.component';
import { AccountTreeFilter } from './components/account-arbor/account-arbor-filter.pipe';
import { ModuleTreeFilter } from './components/module-arbor/module-arbor-filter.pipe';
import { OrderModule } from 'ngx-order-pipe';
import { EmployeeSharedComponent } from './components/employee-shared/employee-shared.component';
import { VendorSharedComponent } from './components/vendor-shared/vendor-shared.component';
import { FilterByIdPipe } from './pipes/filter-by-id.pipe';

const fixed = new FixedService();

@NgModule({
  declarations: [
    PaginationComponent,
    GridFilterPipe,
    StockFilterPipe,
    AccountArborComponent,
    ModuleArborComponent,
    CostCenterArborComponent,
    AccountTreeFilter,
    ModuleTreeFilter,
    EmployeeSharedComponent,
    VendorSharedComponent,
    FilterByIdPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    TreeModule,
    ToastrModule.forRoot(),
    SweetAlert2Module.forRoot(),
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    ScrollToModule.forRoot(),
    TranslateModule,
    HttpClientModule,
    NgSelectModule,
    OrderModule
  ],
  providers: [
    { provide: FixedService, useValue: fixed },
    CookieService,
    BsLocaleService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    BsModalService
  ],
  exports: [
    CommonModule,
    ToastrModule,
    FormsModule,
    TranslateModule,
    HttpClientModule,
    SweetAlert2Module,
    TooltipModule,
    ModalModule,
    ScrollToModule,
    PaginationComponent,
    NgSelectModule,
    BsDatepickerModule,
    GridFilterPipe,
    StockFilterPipe,
    AccountArborComponent,
    ModuleArborComponent,
    CostCenterArborComponent,
    OrderModule,
    EmployeeSharedComponent,
    VendorSharedComponent,
    FilterByIdPipe
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
registerLocaleData(arSaLocale);
defineLocale('ar', arLocale);
