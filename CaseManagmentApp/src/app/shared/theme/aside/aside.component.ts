import { Component } from '@angular/core';
import { FixedService } from '../../services/fixed.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html'
})
export class AsideComponent {

  constructor(public fixed: FixedService) { }
}
