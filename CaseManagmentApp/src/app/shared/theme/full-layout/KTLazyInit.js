import KTUtil from './KTUtil';

const KTLazyInit = function () {
    document.addEventListener("click", function (e) {
        var body = KTUtil().get('body');
        var query;
        if (query = body.querySelectorAll('.kt-menu__nav .kt-menu__item.kt-menu__item--submenu.kt-menu__item--hover:not(.kt-menu__item--tabs)[data-ktmenu-submenu-toggle="click"]')) {
            for (var i = 0, len = query.length; i < len; i++) {
                var element = query[i].closest('.kt-menu__nav').parentNode;

                if (element) {
                    var the = KTUtil().data(element).get('menu');

                    if (!the) {
                        break;
                    }

                    if (!the || the.getSubmenuMode() !== 'dropdown') {
                        break;
                    }

                    if (e.target !== element && element.contains(e.target) === false) {
                        var items;
                        if (items = element.querySelectorAll('.kt-menu__item--submenu.kt-menu__item--hover:not(.kt-menu__item--tabs)[data-ktmenu-submenu-toggle="click"]')) {
                            for (var j = 0, cnt = items.length; j < cnt; j++) {
                                the.hideDropdown(items[j]);
                            }
                        }
                    }
                }
            }
        }
    });
};
export default KTLazyInit;