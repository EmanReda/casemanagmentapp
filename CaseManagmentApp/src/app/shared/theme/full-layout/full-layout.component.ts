import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FixedService } from '../../services/fixed.service';
import KTUtil from './KTUtil';
import KTApp from './KTApp';
import KTLayout from './KTLayout';
import KTLazyInit from './KTLazyInit';
import { CookieService } from 'ngx-cookie-service';
// import { LoginService } from '../../../login/login.service';
import { GlobalService } from '../../services/global.service';
import { Router, NavigationEnd } from '@angular/router';
import { AccountingSettingService } from '../../../accounting/accounting-setting/accounting-setting.service';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FullLayoutComponent implements AfterViewInit {

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    public accountingSettingSer: AccountingSettingService,
    private router: Router,
    // private loginSer: LoginService,
    private cookieSer: CookieService) {
    this.accountingSettingSer.getAccountingSetting();
    this.getUser();
    this.router.events
      .subscribe((val: NavigationEnd) => {
        if (val.url != null && val.url !== this.fixed.currentURL) {
          this.fixed.currentURL = val.url;
          this.global.toTop();
          this.global.generateMenu(val.url, val.url.split('/'));
        }
      });
  }

  ngAfterViewInit() {
    if (this.fixed.themeLoaded === false) {
      KTUtil().ready(() => {
        KTUtil().init();
      });
      KTLazyInit();
      KTApp().init(this.fixed.KTAppOptions);
      KTLayout().init();
      this.fixed.themeLoaded = true;
    } else {
      KTApp().init(this.fixed.KTAppOptions);
      KTLayout().init();
    }
  }

  getUser() {
    if (this.cookieSer.check('user_profile')) {
      this.fixed.userProfile = JSON.parse(this.cookieSer.get('user_profile'));
    } else {
      // this.loginSer.logout();
    }
  }
}
