import { Component } from '@angular/core';
import { FixedService } from '../../services/fixed.service';
import { GlobalService } from '../../services/global.service';
// import { LoginService } from 'src/app/login/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent {

  constructor(public fixed: FixedService,
              public global: GlobalService,
              // public loginSer: LoginService
            )
               { }

}
