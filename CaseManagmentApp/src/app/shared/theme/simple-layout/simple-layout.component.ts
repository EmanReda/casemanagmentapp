import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-simple-layout',
  template: `<router-outlet></router-outlet>`,
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class SimpleLayoutComponent {
}
