import { Component } from '@angular/core';
import { FixedService } from '../../services/fixed.service';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html'
})
export class SubHeaderComponent {

  constructor(public fixed: FixedService) { }
}
