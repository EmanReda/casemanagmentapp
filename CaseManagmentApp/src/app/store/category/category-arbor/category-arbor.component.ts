
import { Component, ViewEncapsulation, OnChanges, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CategoryService } from '../../../store/category/category/category.service';
import { FixedService } from '../../../shared/services/fixed.service';
import { GlobalService } from '../../../shared/services/global.service';


@Component({
  selector: 'app-category-arbor',
  templateUrl: './category-arbor.component.html',
  styleUrls: ['./category-arbor.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryArborComponent implements OnChanges {
  treeList: any;
  list: any;
  selectedNode: any;
  requestSent = false;
  @Input() refresh: boolean;
  @Input() treeView: boolean;
  @Input() selectedValue: number;
  @Input() disable: boolean;
  @Output() selectedValueChanges = new EventEmitter<any>();
  modalRef: BsModalRef;
  @Input() bindLabel: string;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private categorySer: CategoryService,
    private modalService: BsModalService) {
    this.getTreeData();
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes.refresh != undefined && changes.refresh.currentValue !== changes.refresh.previousValue) {
      this.Reload();
    } else {
      this.getTreeData();
    }
  }

  onNodeSelect(data, type, operation?) {
    if (operation === 'delete') {
      this.selectedValue = null;
      this.selectedNode = null;
      this.selectedValueChanges.emit(null);
    } else {
      if (type === 'tree') {
        this.selectedValueChanges.emit(data.node.value);
      } else {
        this.selectedValueChanges.emit(this.list.filter(s => s.categoryId == data)[0]);
      }
    }
  }

  getTreeData() {
    if (this.fixed.store.CategoryTree.length === 0 && !this.requestSent) {
      this.requestSent = true;
      this.categorySer.getAll()
        .subscribe((data) => {
          this.list = data;
          this.generateDropdownArray();
          this.fixed.store.CategoryTree = data;
          if (this.treeView) { this.generateArray(data); }
        }, (data) => {
          this.global.notificationMessage(4);
        });
    } else {
      this.list = this.fixed.store.CategoryTree;
      this.generateDropdownArray();
      if (this.treeView) { this.generateArray(this.fixed.store.CategoryTree); }
    }
  }

  generateDropdownArray() {
    if (!(this.list == null || this.list.length === 0)) {
      this.list.forEach(element => {
        element.fullName = element.name;
        element = this.loadFullPath(this.list, element, element.categoryParentId);
      });
    }
  }

  loadFullPath(data, element: any, ParentId) {
    if (ParentId != null) {
      const parent = data.filter(s => s.categoryId === ParentId)[0];
      if (parent != null) {
        element.fullName = parent.name + ' . ' + element.fullName;
        this.loadFullPath(data, element, parent.categoryParentId);
      }
    }
    return element;
  }

  generateArray(data) {
    if (!(data == null || data.length === 0)) {
      this.treeList = [];
      data.forEach(element => {
        const item = {
          label: element.code + ' -- ' + element.name,
          data: element.categoryId,
          expandedIcon: 'fa fa-folder-open',
          expanded: (element.categoryParentId == null),
          collapsedIcon: 'fa fa-folder',
          parent: element.categoryParentId,
          value: element
        };
        this.treeList.push(item);
      });
      this.treeList = this.loadChildren(this.treeList);
      this.checkNode(this.treeList, Number(this.selectedValue));
      this.listOpened(this.selectedValue);
    }
  }

  loadChildren(arr: any, parent?: any) {
    debugger ;
    const children = [];
    arr.forEach(element => {
      if (element.parent == parent) {
        const grandChildren = this.loadChildren(arr, element.data);
        if (grandChildren.length) {
          element.children = grandChildren;
        }
        children.push(element);
      }
    });
    return children;
  }

  listOpened(id) {
    this.list.forEach(element => {
      if (element.categoryId === id) {
        this.openNode(this.treeList, element.categoryId);
        this.listOpened(element.categoryParentId);
      }
    });
  }

  openNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        element.expanded = true;
      }
      if (element.children != null) {
        this.openNode(element.children, value);
      }
    });
  }

  checkNode(tree: any, value: any) {
    tree.forEach(element => {
      if (element.data === value) {
        this.selectedNode = element;
      }
      if (element.children != null) {
        this.checkNode(element.children, value);
      }
    });
  }

  Reload() {
    this.requestSent = false;
    this.list = [];
    this.fixed.store.CategoryTree.length = 0;
    this.getTreeData();
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }
}
