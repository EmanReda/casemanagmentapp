import { CategoryComponent } from './category/category.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryRoutingModule } from './category-routing.module';
import { CategoryArborComponent } from './category-arbor/category-arbor.component';
import { SharedModule } from '../../shared/shared.module';
import { TreeModule } from '../../../../node_modules/primeng/tree';

@NgModule({
  declarations: [
    CategoryComponent,
    CategoryArborComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    SharedModule.forRoot(),
    TreeModule
  ],
  exports: [
    CategoryArborComponent,
  ]
})
export class CategoryModule { }
