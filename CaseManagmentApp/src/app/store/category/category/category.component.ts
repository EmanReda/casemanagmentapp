import { Component, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { Category } from './category.model';
import { CategoryService } from './category.service';
import deepEqual from 'deep-equal';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html'
})
export class CategoryComponent {
  model: Category;
  shadowModel: Category;
  checkcode = false;
  tempData: any;
  refreshTree: boolean;
  @ViewChild('confirmChanges') confirmChanges: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private translate: TranslateService,
    private categorySer: CategoryService) {
    this.model = new Category();
    this.shadowModel = new Category();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Category.PageHeader', url: '/store/category' }
      ],
      pageHeader: 'Category.PageHeader'
    };
  }

  
  treeChanges(data?, skip?: boolean) {
    if (deepEqual(this.model, this.shadowModel) || skip) {
      this.model = (data != null) ? this.generateModel(data) : new Category();
      this.shadowModel = (data != null) ? this.generateModel(data) : new Category();
    } else if (data.categoryId === this.model.categoryId) {
      this.model.categoryParentId = null;
      this.translate.get(`Category.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.categoryParentId = this.shadowModel.categoryParentId;
      }, 100);
    } else {
      this.confirmChanges.show();
      this.tempData = data;
    }
  }

  generateModel(data: Category) {
    return new Category(data.categoryId, data.name, data.code, data.categoryParentId, data.vat, data.requestLimit);
  }

  categoryParentChange(data) {
    let preventedParent = [];
    if (this.model.categoryId != null) {
      preventedParent = this.global.treeChild(this.fixed.store.CategoryTree, 'categoryId',
        'categoryParentId', [this.model.categoryId]);
    }
    if (data == null) {
      this.model.categoryParentId = null;
      this.model.vat = null;
      this.model.requestLimit = null;
    } else if (this.model.categoryId != null && preventedParent.indexOf(data.categoryId) > -1) {
      this.model.categoryParentId = undefined;
      this.translate.get(`Category.Cann'tselected`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
      setTimeout(() => {
        this.model.categoryParentId = this.shadowModel.categoryParentId;
      }, 50);
    } else {
      this.model.categoryParentId = data.categoryId;
      this.model.vat = data.vat;
      this.model.requestLimit = data.requestLimit;
    }
  }
  checkCode() {
    if (!(this.model.code == null || this.model.code == '' || this.model.code == this.shadowModel.code)) {
      this.categorySer.checkCode(this.model)
        .subscribe((data) => {
          this.checkcode = data;
        },
          () => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveCategoryTree() {
    this.categorySer.operation(this.model)
      .subscribe(
        (response) => {
          if (response.type === ResponseEnum.Success) {
            this.refreshTree = !this.refreshTree;
            this.model = this.generateModel(response.data);
            this.shadowModel = this.generateModel(response.data);
          }
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }, (error) => {
          this.global.notificationMessage(4);
        });
  }

  deleteCategoryTree(id) {
    const chlidern = this.fixed.store.CategoryTree.filter(s => s.categoryParentId === this.model.categoryId);
    if (chlidern.length === 0) {
      this.categorySer.delete(this.model.categoryId)
        .subscribe(
          (response) => {
            if (response.type === ResponseEnum.Success) {
              this.refreshTree = !this.refreshTree;
              this.model = new Category();
              this.shadowModel = new Category();
            }
            if (response.name == null) {
              this.global.notificationMessage(response.type);
            } else {
              this.translate.get(response.name)
                .subscribe(
                  (val) => {
                    this.global.notificationMessage(response.type, null, val);
                  });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    } else {
      this.translate.get(`Category.Cann'tDeleteHasChilderns`)
        .subscribe(
          (val) => {
            this.global.notificationMessage(3, null, val);
          });
    }
  }


}