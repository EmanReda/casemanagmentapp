export class Category{
    constructor(
        public categoryId:number=null,
        public name:string =null,
        public code:string=null,
        public categoryParentId:number=null,
        public vat:number = null,
        public requestLimit :number = null
    ){}
}