import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CategoryService {
    constructor(private http: HttpClient) { }
    getAll(): Observable<any> {
        return this.http.get('Category/GetAll');
    }
    operation(model): Observable<any> {
        return this.http.post('Category/Operation', model);
    }
    delete(id: number): Observable<any> {
        return this.http.delete('Category/Delete/' + id);
    }
    checkCode(model): any {
        return this.http.post('Category/CheckCode', model);
    }
}
