import { Component } from '@angular/core';
import { FilterIncentive } from './incentive-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { IncentiveService } from '../incentive.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-incentive-list',
  templateUrl: './incentive-list.component.html'
})

export class IncentiveListComponent {
  searchModel: FilterIncentive;
  total: number;
  list: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public incentiveSer: IncentiveService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Incentive.List', url: '/store/incentive/list' }
      ],
      pageHeader: 'Incentive.List'
    };
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterIncentive();
    this.getAll();
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.incentiveSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.list = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  resetSearch() {
    this.searchModel = new FilterIncentive();
    this.total = undefined;
    this.list = undefined;
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
