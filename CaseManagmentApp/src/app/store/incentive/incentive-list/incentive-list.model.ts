export class FilterIncentive{
constructor(
    public page: number = 1,
    public recordPerPage: number = 20,
    public fromDate:any=null,
    public toDate:any=null,
    public incentiveName:string=null,
    public categoryId:number=null,
    public productId:number=null,
    public onQty:boolean=null,
    public isPercentage:boolean=null
){}
}