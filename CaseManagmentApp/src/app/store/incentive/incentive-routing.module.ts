import { IncentiveComponent } from './incentive/incentive.component';
import { IncentiveListComponent } from './incentive-list/incentive-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: IncentiveListComponent },
  { path: '', component: IncentiveComponent },
  { path: ':id', component: IncentiveComponent }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncentiveRoutingModule { }
