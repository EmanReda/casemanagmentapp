import { SharedModule } from './../../shared/shared.module';
import { IncentiveListComponent } from './incentive-list/incentive-list.component';
import { IncentiveComponent } from './incentive/incentive.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncentiveRoutingModule } from './incentive-routing.module';
import { CategoryModule } from '../category/category.module';
import { ProductModule } from '../product/product.module';
import { StoreSharedModule } from '../store-shared/store-shared.module';

@NgModule({
  declarations: [
    IncentiveComponent,
    IncentiveListComponent,
  ],
  imports: [
    CommonModule,
    IncentiveRoutingModule,
    SharedModule.forRoot(),
    CategoryModule,
    ProductModule,
    StoreSharedModule
    
  ]
})
export class IncentiveModule { }
