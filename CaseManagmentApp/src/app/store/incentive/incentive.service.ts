import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class IncentiveService {
    constructor(private http:HttpClient){};
      GetAll(model):Observable<any>{
        return this.http.post('Incentive/GetAll',model);
      }
     
      getById(id: number): any {
        return this.http.get(`Incentive/GetById/${id}`);
      }
    
      save(model: any): Observable<any> {
        return this.http.post(`Incentive/Operation`, model);
      }
      delete(id): Observable<any> {
        return this.http.delete(`Incentive/Voided/${id}`);
      }
}