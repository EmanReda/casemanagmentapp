import { Component, OnInit } from '@angular/core';
import { IncentiveItem, IncentiveModel } from './incentive.model';
import { IncentiveService } from '../incentive.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap';
@Component({
  selector: 'app-incentive',
  templateUrl: './incentive.component.html'
})
export class IncentiveComponent implements OnInit {
  model: IncentiveModel;
  parms: any;
  incentiveItemList = new IncentiveItem();
  lockGridEdit = false;
  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private localeService: BsLocaleService,
    private translate: TranslateService,
    private incentiveSer: IncentiveService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Incentive.List', url: '/store/incentive/list' }
      ],
      pageHeader: 'Incentive.Incentive'
    };
    this.model = new IncentiveModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Incentive.Add', url: '/store/incentive' });
          this.fixed.subHeader.pageHeader = 'Incentive.Add';
          this.model.incentiveId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Incentive.Edit', url: '/store/incentive/' + id });
          this.fixed.subHeader.pageHeader = 'Incentive.Edit';
          this.model.incentiveId = Number(id);
          this.getIncentive(Number(id));
        }
      });
  }
  itemOperation(data?: IncentiveItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_IncentiveItem == null) {
        this.model.stO_IncentiveItem = [];
      }
      if (data == null) {
        this.model.stO_IncentiveItem.push(new IncentiveItem());
      } else {
        this.incentiveItemList = new IncentiveItem(data.incentiveItemId,
          data.incentiveId, data.qty, data.value, data.uomId, data.isDeleted, 's');
        data.operation = 'e';
      }
    } else {
      this.translate.get('Incentive.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.incentiveItemList;
      this.lockGridEdit = false;
      (data.IncentiveItem == null) ? this.model.stO_IncentiveItem.splice(index, 1) :
        this.model.stO_IncentiveItem[index] = this.incentiveItemList;
    } else {
      this.lockGridEdit = false;
      data.operation = 's';
    }
  }

  itemDelete(item, index) {
    if (item.incentiveId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_IncentiveItem.splice(index, 1);
    }
  }

  getIncentive(id: number) {
    this.incentiveSer.getById(id)
      .subscribe((Response) => {
        this.model = Response;
        if (Response.categoryId == 0 || Response.categoryId == null) {
          this.model.type = 'product';
        } else {
          this.model.type = 'category';
        }
        this.model.stO_IncentiveItem.forEach(element => {
          element.operation = 's';
        });
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  saveIncentive() {
    this.model.startDate = this.global.formatDate(this.model.startDate);
    this.model.endDate = this.global.formatDate(this.model.endDate);
    this.incentiveSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/incentive/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  delete(id) {
    this.incentiveSer.delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/incentive/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  selectProduct(event) {
    if (event != null) {
      this.model.product = event.element;
      this.model.productId = event.element.productId;
    } else {
      this.model.product = null;
      this.model.productId = null;
    }
  }
}
