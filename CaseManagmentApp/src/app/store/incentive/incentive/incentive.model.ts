export class IncentiveModel {
    constructor(
        public incentiveId: number = null,
        public incentiveName: string = null,
        public categoryId: number = null,
        public productId: number = null,
        public product: any = null,
        public startDate: any = null,
        public endDate: any = null,
        public onQty: any = 1,
        public isPercentage: boolean = false,
        public stO_IncentiveItem: IncentiveItem[] = [],
        public type: string = 'category') { }
}

export class IncentiveItem {
    constructor(
        public incentiveItemId: number = null,
        public incentiveId: number = null,
        public qty: number = null,
        public value: number = null,
        public uomId: number = null,
        public isDeleted: boolean = false,
        public operation: string = 'e') { }
}
