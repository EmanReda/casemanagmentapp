import { Component, OnInit, ViewChild } from '@angular/core';
import { InvoiceModel, InvoiceItem } from './invoice-amount.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { InvoiceService } from '../invoice.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-invoice-amount',
  templateUrl: './invoice-amount.component.html'
})

export class InvoiceAmountComponent implements OnInit {
  model: InvoiceModel;
  parms: any;
  lockGridEdit = false;
  invoiceItem = new InvoiceItem();
  @ViewChild('operationError') operationError: SwalComponent;
  listPage: string;
  emptyList = true;
  pageType: string;
  checkDate = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private invoiceSer: InvoiceService,
    private localStorage: LocalStorage,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null }
      ],
      pageHeader: ''
    };
    this.model = new InvoiceModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        this.pageType = params.type;
        if (this.pageType === 'invoice') {
          this.listPage = '/store/i/invoice/list/';
          this.fixed.subHeader.data.push({ name: 'Invoice.List', url: '/store/i/invoice/list/' });
          this.fixed.subHeader.pageHeader = 'Invoice.Sales';
          if (id == null || id === 'null') {
            this.fixed.subHeader.data.push({ name: 'Invoice.Add', url: '/store/i/invoice/amount' });
            this.fixed.subHeader.pageHeader = 'Invoice.Add';
          } else {
            this.fixed.subHeader.data.push({ name: 'Invoice.Edit', url: '/store/i/invoice/amount/' + id });
            this.fixed.subHeader.pageHeader = 'Invoice.Edit';
          }
        } else if (this.pageType === 'order') {
          this.listPage = '/store/i/order/list/';
          this.fixed.subHeader.data.push({ name: 'Order.List', url: '/store/i/order/list/' });
          this.fixed.subHeader.pageHeader = 'Order.List';
          if (id == null || id === 'null') {
            this.fixed.subHeader.data.push({ name: 'Order.Add', url: '/store/i/order/amount' });
            this.fixed.subHeader.pageHeader = 'Order.Add';
          } else {
            this.fixed.subHeader.data.push({ name: 'Order.Edit', url: '/store/i/order/amount/' + id });
            this.fixed.subHeader.pageHeader = 'Order.Edit';
          }
        } else {
          this.router.navigate(['/']);
        }
        if (id === 'null') {
          this.localStorage.getItem<any>(this.pageType)
            .subscribe((data: any) => {
              this.model = data;
              if (this.pageType === 'order') {
                this.model.invoiceId = data.orderId;
              }
              this.model.stO_InvoiceItem.forEach(item => {
                item.operation = 's';
                item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
              });
            }, () => {
              this.global.notificationMessage(4);
              this.router.navigate(['/store/i/' + this.pageType + '/amount']);
            });
        } else if (id == null) {
          this.model.invoiceId = null;
        } else {
          this.model.invoiceId = Number(id);
          this.getInvoice(Number(id));
        }
      });
  }

  getInvoice(id: number) {
    this.invoiceSer.getById(this.pageType, id)
      .subscribe((response: any) => {
        if (response.type === ResponseEnum.Success) {
          this.checkDate = true;
          this.emptyList = false;
          this.model = response.data;
          if (this.pageType === 'order') {
            this.model.invoiceId = response.data.orderId;
            this.model.stO_InvoiceItem = response.data.stO_OrderItem;
          }
          this.model.stO_InvoiceItem.forEach((item: any) => {
            if (this.pageType === 'order') {
              item.invoiceId = item.orderId;
              item.invoiceItemId = item.orderItemId;
            }
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.generateUnitList(item);
            item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
            item.maxQty = item.batchExpire.qtyInUom;
          });
        } else {
          this.closePage();
        }
      }, () => {
        this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate([this.listPage]);
    }, 5000);
  }

  getTransactionNo(data) {
    if (data != null && this.model.invoiceId == null) {
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
          if (this.checkDate === false) {
            this.model.transactionNo = null;
          } else {
            this.invoiceSer.getTransactionNo(this.pageType, this.global.formatDate(data))
              .subscribe((respo) => {
                this.model.transactionNo = respo;
                this.checkDate = true;
              }, () => {
                this.global.notificationMessage(4);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: InvoiceItem) {
    if (!this.lockGridEdit && this.model.client != null && this.model.stockId != null) {
      this.lockGridEdit = true;
      if (this.model.stO_InvoiceItem == null) {
        this.model.stO_InvoiceItem = [];
      }
      if (data == null) {
        this.model.stO_InvoiceItem
          .push(new InvoiceItem(null, null, this.model.stO_InvoiceItem.length + 1));
      } else {
        this.invoiceItem = new InvoiceItem(data.invoiceItemId, data.invoiceId,
          data.itemId, data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.uomDiscountPercentage, data.uomDiscountValue,
          data.totalAfterDiscount, data.vatPercentage, data.totalVatValue, data.totalDiscount, data.product,
          data.unitId, data.units, data.batchExpireList, data.batchExpire, data.maxQty, data.qtyInStock,
          data.restQtyInInvoice, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else if (this.lockGridEdit) {
      this.translate.get('Invoice.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    } else if (this.model.client == null) {
      this.translate.get('Invoice.SelectClient')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    } else if (this.model.stockId == null) {
      this.translate.get('Invoice.SelectStock')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.invoiceItem;
      this.lockGridEdit = false;
      (data.stO_InvoiceItemId == null) ? this.model.stO_InvoiceItem.splice(index, 1) :
        this.model.stO_InvoiceItem[index] = this.invoiceItem;
    } else {
      if (data.product == null) {
        this.translate.get('Invoice.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('Invoice.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('Invoice.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom < 1) {
        this.translate.get('Invoice.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomPrice == 0) {
        this.translate.get('Invoice.uomPriceRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom > data.maxQty && this.pageType === 'invoice') {
        data.qtyInUom = data.maxQty;
        this.translate.get('Invoice.NotFoundQnty')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('Invoice.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_InvoiceItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber && s.uomPrice == data.uomPrice);
      if (prev.length === 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        if (prev[0].isDeleted === true) {
          prev[0].qtyInUom = data.qtyInUom;
          prev[0].uomPrice = data.uomPrice;
          prev[0].isDeleted = null;
          prev[0].isEdited = true;
        } else {
          prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
          if (prev[0].qtyInUom > data.maxQty && this.pageType === 'invoice') {
            prev[0].qtyInUom = data.maxQty;
            this.translate.get('Invoice.NotFoundQnty')
              .subscribe((val) => {
                this.global.notificationMessage(3, null, val);
              });
          }
          prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    const len = this.model.stO_InvoiceItem.filter(s => s.isDeleted === false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  itemDelete(item, index) {
    if (item.invoiceItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_InvoiceItem.splice(index, 1);
    }
    const len = this.model.stO_InvoiceItem.filter(s => s.isDeleted === false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  selectProduct(event, item: InvoiceItem) {
    if (event != null) {
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      const priceList = event.element.productPrice
        .filter(s => s.priceListId === this.model.client.priceListId)[0];
      item.unitId = null;
      item.batchExpireList = [];
      item.uomBarCode = null;
      item.batchExpire = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.uomPrice = 0.0;
      item.totalPrice = 0.0;
      if (event.patch != null) {
        item.batchExpire = event.patch;
        item.expiryDate = event.patch.expiryDate;
        item.batchNumber = event.patch.batchNumber;
        item.maxQty = event.patch.qtyInUom;
      }
      this.generateUnitList(item);
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          item.uomPrice = priceList.smallUomPrice;
          this.loadBatchExpireList(item);
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          item.uomPrice = priceList.mediumUomPrice;
          this.loadBatchExpireList(item);
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          item.uomPrice = priceList.largeUomPrice;
          this.loadBatchExpireList(item);
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
        this.unitChange(item);
      }
    } else {
      item.product = null;
      item.barcode = null;
      item.productId = null;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      item.units = null;
      item.batchExpireList = null;
      item.batchExpire = null;
    }
  }

  generateUnitList(item) {
    item.units = [];
    if (item.product.smallUomNameId != null) {
      const unit = {
        unitId: item.product.smallUomNameId,
        name: item.product.smallUomName,
        barcode: item.product.smallUomBarCode,
        size: 's'
      };
      item.units.push(unit);
    }
    if (item.product.mediumUomNameId != null) {
      const unit = {
        unitId: item.product.mediumUomNameId,
        name: item.product.mediumUomName,
        barcode: item.product.mediumUomBarCode,
        size: 'm'
      };
      item.units.push(unit);
    }
    if (item.product.largeUomNameId != null) {
      const unit = {
        unitId: item.product.largeUomNameId,
        name: item.product.largeUomName,
        barcode: item.product.largeUomBarCode,
        size: 'l'
      };
      item.units.push(unit);
    }
  }

  loadBatchExpireList(item) {
    if (item.productId != null && this.model.stockId != null && item.uomBarCode != null) {
      this.invoiceSer.loadBatchExpireList(item.productId, this.model.stockId, item.uomBarCode)
        .subscribe((response) => {
          if (response != null && response.length > 0) {
            item.batchExpireList = response;
            if (response.length === 1) {
              item.batchExpire = response[0];
              this.batchExpireChange(item);
            }
          } else {
            this.translate.get('Invoice.NotFoundQnty')
              .subscribe((val) => {
                this.global.notificationMessage(3, null, val);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  batchExpireChange(item) {
    item.expiryDate = item.batchExpire.expiryDate;
    item.batchNumber = item.batchExpire.batchNumber;
    item.maxQty = item.batchExpire.qtyInUom;
  }

  unitChange(item) {
    item.uomPrice = 0;
    item.batchExpire = null;
    item.expiryDate = null;
    item.batchNumber = null;
    item.batchExpireList = [];
    item.totalPrice = 0;
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
    const priceList = item.product.productPrice
      .filter(s => s.priceListId === this.model.client.priceListId)[0];
    if (priceList == null || priceList == undefined) {
      this.translate.get('Invoice.ProductPriceLess')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    } else {
      switch (item.uomBarCode) {
        case (item.product.smallUomBarCode):
          item.uomPrice = priceList.smallUomPrice;
          break;
        case (item.product.mediumUomBarCode):
          item.uomPrice = priceList.mediumUomPrice;
          break;
        case (item.product.largeUomBarCode):
          item.uomPrice = priceList.largeUomPrice;
          break;
      }
      item.totalPrice = item.uomPrice * item.qtyInUom;
      item.expiryDate = null;
      item.batchNumber = null;
      this.loadBatchExpireList(item);
    }
  }

  nextPage() {
    this.translate.get('Invoice.WaitIncentive')
      .subscribe(
        (val) => {
          this.global.notificationMessage(2, null, val);
        });
    this.model.date = this.global.formatDate(this.model.date);
    this.invoiceSer.calDiscount(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.InProgress) {
          this.localStorage.setItem(this.pageType, response.data)
            .subscribe(() => {
              this.router.navigate(['/store/i/' + this.pageType + '/discount']);
            });
        } else {
          if (response.name == null) {
            this.global.notificationMessage(response.type);
          } else {
            this.translate.get(response.name)
              .subscribe(
                (val) => {
                  this.global.notificationMessage(response.type, null, val);
                });
          }
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
