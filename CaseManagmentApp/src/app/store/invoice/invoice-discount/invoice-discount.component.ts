import { Component, OnInit, ViewChild } from '@angular/core';
import { InvoiceModel } from '../invoice-amount/invoice-amount.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { InvoiceService } from '../invoice.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-invoice-discount',
  templateUrl: './invoice-discount.component.html'
})
export class InvoiceDiscountComponent implements OnInit {
  model: InvoiceModel;
  parms: any;
  listPage: string;
  pageType: string;
  discountList = [];
  @ViewChild('operationError') operationError: SwalComponent;
  justInfo = true;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private invoiceSer: InvoiceService,
    private localStorage: LocalStorage) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null }
      ],
      pageHeader: ''
    };
    this.model = new InvoiceModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        this.pageType = params.type;
        if (this.pageType === 'invoice') {
          this.listPage = '/store/i/invoice/list/';
          this.fixed.subHeader.data.push({ name: 'Invoice.List', url: '/store/i/invoice/list/' });
          this.fixed.subHeader.pageHeader = 'Invoice.Sales';
          if (id == null || id === 'null') {
            this.fixed.subHeader.data.push({ name: 'Invoice.Add', url: '/store/i/invoice/amount' });
            this.fixed.subHeader.pageHeader = 'Invoice.Add';
          } else {
            this.fixed.subHeader.data.push({ name: 'Invoice.Edit', url: '/store/i/invoice/amount/' + id });
            this.fixed.subHeader.pageHeader = 'Invoice.Edit';
          }
        } else if (this.pageType === 'order') {
          this.listPage = '/store/i/order/list/';
          this.fixed.subHeader.data.push({ name: 'Order.List', url: '/store/i/order/list/' });
          this.fixed.subHeader.pageHeader = 'Order.List';
          if (id == null || id === 'null') {
            this.fixed.subHeader.data.push({ name: 'Order.Add', url: '/store/i/order/amount' });
            this.fixed.subHeader.pageHeader = 'Order.Add';
          } else {
            this.fixed.subHeader.data.push({ name: 'Order.Edit', url: '/store/i/order/amount/' + id });
            this.fixed.subHeader.pageHeader = 'Order.Edit';
          }
        } else {
          this.router.navigate(['/']);
        }
        if (id == null) {
          this.justInfo = false;
          this.localStorage.getItem<any>(this.pageType)
            .subscribe((data: any) => {
              if (data == null) {
                this.router.navigate(['/store/i/invoice/amount']);
              } else {
                this.model = data;
                this.diccountTotal();
              }
            }, () => {
              this.global.notificationMessage(4);
              this.router.navigate(['/store/i/invoice/amount']);
            });
        } else {
          this.getInvoice(Number(id));
        }
      });
  }

  diccountTotal(from?) {
    this.discountList = [];
    this.model.stO_InvoiceItem.forEach((element: any) => {
      element.stO_InvoiceItemIncentive.forEach(item => {
        const inc = this.discountList.filter(s => s.incentiveId === item.incentiveId)[0];
        if (inc == null) {
          this.discountList.push({
            incentiveId: item.incentiveId,
            incentiveName: (from === 'server') ? item.stO_Incentive.incentiveName : item.incentiveName,
            incentiveValue: item.incentiveValue,
            qtyUsedInDiscount: item.qtyUsedInDiscount
          });
        } else {
          inc.incentiveValue = inc.incentiveValue + item.incentiveValue;
          inc.qtyUsedInDiscount = inc.qtyUsedInDiscount + item.qtyUsedInDiscount;
        }
      });
    });
  }

  save() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_InvoiceItem.forEach((element: any) => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
      if (this.pageType === 'order') {
        element.stO_OrderItemIncentive = element.stO_InvoiceItemIncentive;
        element.orderId = element.invoiceId;
        element.orderItemId = element.invoiceItemId;
      }
    });
    if (this.pageType === 'order') {
      this.model.orderId = this.model.invoiceId;
      this.model.stO_OrderItem = this.model.stO_InvoiceItem;
    }
    this.invoiceSer.save(this.pageType, this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
          this.localStorage.removeItem(this.pageType);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  getInvoice(id: number) {
    this.invoiceSer.getById(this.pageType, id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.model = response.data;
          if (this.pageType === 'order') {
            this.model.invoiceId = response.data.orderId;
            this.model.stO_InvoiceItem = response.data.stO_OrderItem;
          }
          this.model.stO_InvoiceItem.forEach((item: any) => {
            if (this.pageType === 'order') {
              item.invoiceId = item.orderId;
              item.invoiceItemId = item.orderItemId;
              item.stO_InvoiceItemIncentive = item.stO_OrderItemIncentive;
            }
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.units = [];
            if (item.product.smallUomNameId != null) {
              const unit = {
                unitId: item.product.smallUomNameId,
                name: item.product.smallUomName,
                barcode: item.product.smallUomBarCode,
                size: 's'
              };
              item.units.push(unit);
            }
            if (item.product.mediumUomNameId != null) {
              const unit = {
                unitId: item.product.mediumUomNameId,
                name: item.product.mediumUomName,
                barcode: item.product.mediumUomBarCode,
                size: 'm'
              };
              item.units.push(unit);
            }
            if (item.product.largeUomNameId != null) {
              const unit = {
                unitId: item.product.largeUomNameId,
                name: item.product.largeUomName,
                barcode: item.product.largeUomBarCode,
                size: 'l'
              };
              item.units.push(unit);
            }
          });
          this.diccountTotal('server');
        } else {
          this.closePage();
        }
      }, () => {
        this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate([this.listPage]);
    }, 5000);
  }

  voidedTransaction() {
    this.invoiceSer.voided(this.pageType, this.model.invoiceId)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  Print() {
    this.invoiceSer.PrintById(this.pageType, this.model.invoiceId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
