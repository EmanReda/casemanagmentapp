import { Component, OnInit } from '@angular/core';
import { FilterInvoice } from './invoice-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { InvoiceService } from '../invoice.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html'
})
export class InvoiceListComponent implements OnInit {
  searchModel: FilterInvoice;
  total: any;
  dataList: any;
  parms: any;
  pageType: string;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private invoiceSer: InvoiceService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null }
      ],
      pageHeader: ''
    };
    this.searchModel = new FilterInvoice();
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        this.pageType = params.type;
        if (this.pageType === 'invoice') {
          this.fixed.subHeader.data.push({ name: 'Invoice.List', url: '/store/i/invoice/list/' });
          this.fixed.subHeader.pageHeader = 'Invoice.Sales';
        } else if (this.pageType === 'order') {
          this.fixed.subHeader.data.push({ name: 'Order.List', url: '/store/i/order/list/' });
          this.fixed.subHeader.pageHeader = 'Order.List';
        } else {
          this.router.navigate(['/']);
        }
        this.getAll();
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.invoiceSer.getAll(this.pageType, this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterInvoice();
    this.total = undefined;
    this.dataList = undefined;
  }

}
