export class FilterInvoice {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public transactionNo: string = null,
        public stockId: number = null,
        public fromDate: any = null,
        public toDate: any = null,
        public isVoided: boolean = null) { }
}
