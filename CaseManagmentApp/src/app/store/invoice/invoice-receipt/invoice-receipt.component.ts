import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { InvoiceService } from '../invoice.service';
import { InvoiceModel, InvoiceReceipt } from '../invoice-amount/invoice-amount.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-invoice-receipt',
  templateUrl: './invoice-receipt.component.html'
})

export class InvoiceReceiptComponent implements OnInit {
  model: InvoiceModel;
  parms: any;
  lockGridEdit = false;
  invoiceReceipt: InvoiceReceipt;
  InvoiceReceipt = new InvoiceReceipt();
  @ViewChild('operationError') operationError: SwalComponent;
  emptyList = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private invoiceSer: InvoiceService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Invoice.List', url: '/store/i/invoice/list' }
      ],
      pageHeader: 'InvoiceReceipt.InvoiceReceipt'
    };
    this.model = new InvoiceModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate(['/store/i/invoice/list']);
          }, 5000);
        } else {
          this.fixed.subHeader.data.push({ name: 'Invoice.Edit', url: '/store/i/invoice-receipt/' + id });
          this.fixed.subHeader.pageHeader = 'InvoiceReceipt.InvoiceReceipt';
          this.model.invoiceId = Number(id);
          this.getInvoice(Number(id));
        }
      });
  }

  getInvoice(id: number) {
    this.invoiceSer.getById('Invoice', id)
      .subscribe((Response) => {
        if (Response.type === ResponseEnum.Success) {
          this.model = Response.data;
          this.model.stO_InvoiceReceipt.forEach(item => {
            item.operation = 's';
          });
        } else {
         this.closePage();
        }
      }, () => {
          this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate(['/store/i/invoice/list']);
    }, 5000);
  }

  getReceiptNo(data, item) {
    if (data != null) {
      this.invoiceSer.getReceiptNo(this.global.formatDate(data))
        .subscribe((response) => {
          const result = this.model.stO_InvoiceReceipt.filter(s => s.receiptNo === response);
          if (result.length == 0) {
            item.receiptNo = response;
          } else {
            const x = response.toString().substring(0, 6);
            const y = response.toString().substring(6);
            const newNum = Number(y) + 1;
            item.receiptNo = x + newNum.toString();
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: InvoiceReceipt) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_InvoiceReceipt == null) {
        this.model.stO_InvoiceReceipt = [];
      }
      if (data == null) {
        this.model.stO_InvoiceReceipt.push(new InvoiceReceipt());
      } else {
        this.invoiceReceipt = new InvoiceReceipt(data.invoiceReceiptId, data.invoiceId, data.paymentMethodId,
          data.receiptNo, data.date, data.amount, data.isEdited, data.isDeleted, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('InvoiceReceipt.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemDelete(item, index) {
    if (item.invoiceReceiptId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_InvoiceReceipt.splice(index, 1);
    }
    this.CalcPaied();
    const len = this.model.stO_InvoiceReceipt.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.invoiceReceipt;
      this.lockGridEdit = false;
      (data.InvoiceReceipt == null) ? this.model.stO_InvoiceReceipt.splice(index, 1) :
        this.model.stO_InvoiceReceipt[index] = this.invoiceReceipt;
    } else {
      if (data.date == 'Invalid Date' || data.date == null) {
        this.translate.get('InvoiceReceipt.EnterValidDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
      }
      if (data.paymentMethodId == 0 || data.paymentMethodId == null) {
        this.translate.get('InvoiceReceipt.PaymentMethod')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.amount == null || data.amount <= 0.0) {
        this.translate.get('InvoiceReceipt.Amount')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      this.CalcPaied();
      this.lockGridEdit = false;
      data.operation = 's';
    }
  }

  CalcPaied() {
    this.model.paidAmount = 0;
    this.model.stO_InvoiceReceipt.forEach(item => {
      if (!item.isDeleted) {
        this.model.paidAmount = this.model.paidAmount + item.amount;
        if (this.model.paidAmount > this.model.total) {
          this.translate.get('InvoiceReceipt.TotalGreatetThanPaiedAmount')
            .subscribe((val) => {
              this.global.notificationMessage(3, null, val);
            });
          this.lockGridEdit = true;
        }
      }
    });
  }

  saveInvoiceReceipt() {
    this.invoiceSer.saveInvoiceReceipt(this.model)
      .subscribe((response: any) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/i/invoice/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
