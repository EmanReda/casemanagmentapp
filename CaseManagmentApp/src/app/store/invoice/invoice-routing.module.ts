import { InvoiceReceiptComponent } from './invoice-receipt/invoice-receipt.component';
import { InvoiceDiscountComponent } from './invoice-discount/invoice-discount.component';
import { InvoiceAmountComponent } from './invoice-amount/invoice-amount.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: ':type/list', component: InvoiceListComponent },
  { path: ':type/amount/:id', component: InvoiceAmountComponent },
  { path: ':type/amount', component: InvoiceAmountComponent },
  { path: ':type/discount', component: InvoiceDiscountComponent },
  { path: ':type/discount/:id', component: InvoiceDiscountComponent },
  { path: 'receipt', component: InvoiceReceiptComponent },
  { path: 'receipt/:id', component: InvoiceReceiptComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
