import { SharedModule } from './../../shared/shared.module';
import { InvoiceReceiptComponent } from './invoice-receipt/invoice-receipt.component';
import { InvoiceDiscountComponent } from './invoice-discount/invoice-discount.component';
import { InvoiceAmountComponent } from './invoice-amount/invoice-amount.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { ClientModule}from './../../client/client.module';
@NgModule({
  declarations: [
    InvoiceListComponent,
    InvoiceAmountComponent,
    InvoiceDiscountComponent,
    InvoiceReceiptComponent,
  ],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule,
    PaymentMethodModule,
    ClientModule
  ]
})
export class InvoiceModule { }
