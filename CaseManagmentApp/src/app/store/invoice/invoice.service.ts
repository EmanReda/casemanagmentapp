import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class InvoiceService {

  constructor(private http: HttpClient) { }

  getTransactionNo(type, date): Observable<any> {
    return this.http.get(type + '/getTransactionNo?date=' + date);
  }

  getById(type, id: number): any {
    return this.http.get(type + `/GetById/${id}`);
  }

  save(type, model: any): Observable<any> {
    return this.http.post(type + `/Operation`, model);
  }

  voided(type, id): Observable<any> {
    return this.http.delete(type + `/Voided/${id}`);
  }

  getAll(type, model): Observable<any> {
    return this.http.post(type + `/GetAll`, model);
  }

  calDiscount(model): Observable<any> {
    return this.http.post(`Invoice/CalDiscount`, model);
  }

  loadBatchExpireList(ProductId, StockId, BarCode): Observable<any> {
    return this.http.get(`Invoice/loadBatchExpireList?ProductId=` + ProductId + '&StockId=' + StockId + '&BarCode=' + BarCode);
  }

  getReceiptNo(date) {
    return this.http.get('Invoice/GetReceiptNo?date=' + date);
  }

  saveInvoiceReceipt(model): Observable<any> {
    return this.http.post(`Invoice/SaveInvoiceReceipt`, model);
  }

  PrintById(type, id): any {
    return this.http.get(type + '/OrderReport/' + id);
  }
}
