import { Component, OnInit } from '@angular/core';

import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PayableService } from '../payable.service';
import { BsLocaleService } from 'ngx-bootstrap';

import { FilterStockPayableVoucher } from './payable-list.model';

@Component({
  selector: 'app-payable-list',
  templateUrl: './payable-list.component.html',
  styles: []
})
export class PayableListComponent{
  searchModel:FilterStockPayableVoucher;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private PayableSer: PayableService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Payable.List', url: '/store/payable/list' }
      ],
      pageHeader: 'Payable.List'
    };
    this.searchModel = new FilterStockPayableVoucher();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.PayableSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
  resetSearch() {
    this.searchModel = new FilterStockPayableVoucher();
    this.total = undefined;
    this.dataList = undefined;
  }
 
}
