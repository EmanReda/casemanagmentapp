import { Component, OnInit, ViewChild ,TemplateRef} from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PayableService } from '../payable.service';
import { PayableModel, PayableItem } from './payable-operation.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-payable-operation',
  templateUrl: './payable-operation.component.html'
})
export class PayableOperationComponent implements OnInit {
  model: PayableModel;
  selectedItem: PayableItem;
  parms: any;
  lockGridEdit = false;
  invoiceList: any;
  returnList: any;
  payableItem: PayableItem;
  listPage = '/store/payable/list';
  modalRef: BsModalRef;
  @ViewChild('operationError') operationError: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private router: Router,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private payableSer: PayableService,
    private localeService: BsLocaleService,
    private modalService: BsModalService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Payable.List', url: '/store/payable/list' }
      ],
      pageHeader: 'Payable.PageHeader'
    };
    this.model = new PayableModel();
    this.selectedItem = new PayableItem();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Payable.Add', url: '/store/payable/operation' });
          this.fixed.subHeader.pageHeader = 'Payable.Add';
          this.model.payableId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Common.Show', url: '' });
          this.fixed.subHeader.pageHeader = 'Common.Show';
          this.model.payableId = Number(id);
          this.getById(Number(id));
        }
      });
  }
  getById(id: number) {
    this.payableSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate([this.listPage]);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  openModal(template: TemplateRef<any>) {
    if(this.model.payableId == null && this.model.vendorId != null && this.model.paymentMethodId != null){
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    }
    else if (this.model.vendorId == null) {
      this.translate.get('Payable.SelectVendor')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
    else if (this.model.paymentMethodId == null) {
      this.translate.get('Payable.SelectPaymentMethod')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  getTransactionNo(data) {
    if (data != null && this.model.payableId == null) {
      this.payableSer.getTransactionNo(this.global.formatDate(data))
        .subscribe((response) => {
          this.model.payableNo = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  getInvoicesAndReturns(model) {
    if (model.paymentMethodId != null && model.vendorId != null) {
      this.payableSer.getInvoices(model.vendorId)
        .subscribe((response) => {
          this.invoiceList = response;
        }, () => {
          this.global.notificationMessage(4);
        });

      this.payableSer.getReturns(model.vendorId, model.paymentMethodId)
        .subscribe((response) => {
          this.returnList = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }
  getInvoiceData(item, type) {
    item.total = 0.0;
    if (type == 'invoice') {
      item.stockReturnVoucherId = null;
      const invoice = this.invoiceList.filter(s => s.stockReceiveVoucherId == item.stockReceiveVoucherId);
      item.total = invoice[0].total;
      item.rest = invoice[0].total - invoice[0].paidAmount;
      item.transactionNo = invoice[0].transactionNo;
    } else if (type == 'return') {
      item.stockReceiveVoucherId = null;
      const returnInv = this.returnList.filter(s => s.stockReturnVoucherId == item.stockReturnVoucherId);
      item.total = returnInv[0].total;
      item.rest = returnInv[0].total - returnInv[0].paidAmount;
      item.transactionNo = returnInv[0].transactionNo;
    }
  }

  itemDelete(item, index) {
    if (item.payableItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_PayableItem.splice(index, 1);
    }
    item.amount = 0;
    this.calcTotal();
  }
  saveItem(item) {
    this.payableItem = new PayableItem(item.payableItemId, item.payableId, item.stockReceiveVoucherId, item.stockReturnVoucherId,
      item.amount, item.total, item.rest, item.transactionNo, item.type, item.isEdited, false);

    if (item.stockReceiveVoucherId == null && item.stockReturnVoucherId == null) {
      this.translate.get('Payable.ChooseInvoiceOrReturn')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    }
    if (item.amount == null || item.amount == 0) {
      this.translate.get('Payable.EnterAmount')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    }
    const prev = this.model.stO_PayableItem.filter(s => s.stockReceiveVoucherId == item.stockReceiveVoucherId);
    if (prev.length == 0) {
      if (item.amount > item.rest) {
        this.translate.get('Payable.AmountGreaterThanRest')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      this.model.stO_PayableItem.push(this.payableItem);
    } else {
      const x = prev[0].amount + item.amount;
      if (x > item.rest) {
        this.translate.get('Payable.AmountGreaterThanRest')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      prev[0].amount = prev[0].amount + item.amount;
    }

    this.calcTotal();
    this.New();
  }

  New() {
    this.selectedItem = new PayableItem();
  }
  calcTotal() {
    this.model.total = 0.0;
    let sumOfInvoices = 0.0;
    let sumOfReturns = 0.0;
    this.model.stO_PayableItem.forEach(element => {
      if (element.stockReceiveVoucherId != null) {
        sumOfInvoices += element.amount;
      } else if (element.stockReturnVoucherId != null) {
        sumOfReturns += element.amount;
      }
    });
    this.model.total = sumOfInvoices - sumOfReturns;
  }

  save() {
    this.model.date = this.global.formatDate(this.model.date);
    this.payableSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  voidedTransaction(id) {
    this.payableSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

}
