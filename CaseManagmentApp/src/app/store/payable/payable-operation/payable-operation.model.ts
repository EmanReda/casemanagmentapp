export class PayableModel {
    constructor(
        public payableId: number = null,
        public stockId: number = null,
        public vendorId: number = null,
        public vendor: any = null,
        public paymentMethodId: number = null,
        public payableNo: string = null,
        public date: any = null,
        public total: number = 0.0,
        public type: string = null,
        public payment: boolean = true,
        public isPrinted: boolean = false,
        public isVoided: boolean = false,
        public stO_PayableItem: PayableItem[] = []) { }
}

export class PayableItem {
    constructor(
        public payableItemId: number = null,
        public payableId: number = null,
        public stockReceiveVoucherId: number = null,
        public stockReturnVoucherId: number = null,
        public amount: number = null,
        public total: number = null,
        public rest: number = null,
        public transactionNo: string = null,
        public type: string = null,
        public isEdited: boolean = false,
        public isDeleted: boolean = false) { }
}
