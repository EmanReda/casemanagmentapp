import { PayableOperationComponent } from './payable-operation/payable-operation.component';
import { PayableListComponent } from './payable-list/payable-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: PayableListComponent },
  { path: 'operation', component: PayableOperationComponent },
  { path: 'operation/:id', component: PayableOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayableRoutingModule { }
