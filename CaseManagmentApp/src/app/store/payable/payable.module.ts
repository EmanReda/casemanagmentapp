import { SharedModule } from './../../shared/shared.module';
import { PayableOperationComponent } from './payable-operation/payable-operation.component';
import { PayableListComponent } from './payable-list/payable-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayableRoutingModule } from './payable-routing.module';
import { StockModule } from '../stock/stock.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';

@NgModule({
  declarations: [
    PayableListComponent,
    PayableOperationComponent,
  ],
  imports: [
    CommonModule,
    PayableRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    PaymentMethodModule
  ]
})
export class PayableModule { }
