import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PayableService {

  constructor(private http: HttpClient) { }

  getTransactionNo(date): Observable<any> {
    return this.http.get('payable/GetTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`payable/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`payable/Operation`, model);
  }

  getAll(model): Observable<any> {
    return this.http.post(`payable/GetAll`, model);
  }
  getInvoices(vendorId): any {
    return this.http.get('payable/GetInvoices?vendorId=' + vendorId);
  }

  getReturns(vendorId, payment): any {
    return this.http.get('payable/GetReturns?vendorId=' + vendorId + '&payment=' + payment);
  }

  voided(id): Observable<any> {
    return this.http.delete(`payable/Voided/${id}`);
  }
}
