
import { Component, Input, Output, EventEmitter ,OnChanges, SimpleChanges } from '@angular/core';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { PaymentMethodService } from '../payment-method/payment-method.service';
@Component({
  selector: 'app-payment-method-shared',
  templateUrl: './payment-method-shared.component.html'
})
export class PaymentMethodSharedComponent implements OnChanges{
  list: any;
  @Input() selectedValue: number;
  @Input() typeId: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    public paymentSer:PaymentMethodService) {
    this.getAll();
}
ngOnChanges(changes:SimpleChanges){
 this.getAll();
}
getAll() {
  if(this.typeId !=undefined){
   this.paymentSer.GetFilteredSharedData(this.typeId) 
   .subscribe((response) => {
    this.fixed.store.PaymentMethod = [];
    this.list = response;
    this.fixed.store.PaymentMethod =this.list;
  }, () => {
      this.global.notificationMessage(4);
  });}
}
}

