import { PaymentMethodSharedComponent } from './payment-method-shared/payment-method-shared.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentMethodRoutingModule } from './payment-method-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    PaymentMethodComponent,
    PaymentMethodSharedComponent
  ],
  imports: [
    CommonModule,
    PaymentMethodRoutingModule,
    SharedModule.forRoot(),
  ],
  exports:[
    PaymentMethodSharedComponent
  ]
})
export class PaymentMethodModule { }
