import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { PaymentMethodService } from './payment-method.service';
import { PaymentMethod, FilterPaymentMethod } from './payment-method.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html'
})
export class PaymentMethodComponent {
  searchModel: FilterPaymentMethod;
  model: PaymentMethod;
  checkCode = false;
  PaymentMethods: any;
  modalRef: BsModalRef;
  total: number;
  paymentMethodTypes: any[];
  accountTreeList: any[];

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public paymentMethodSer: PaymentMethodService,
    private translate: TranslateService,
    private modalService: BsModalService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'PaymentMethod.PageHeader', url: '/store/payment-method' }
      ],
      pageHeader: 'PaymentMethod.PageHeader'
    };
    this.searchModel = new FilterPaymentMethod();
    this.getPaymentMethodTypes();
    this.getAll();
  }

  openModal(template: TemplateRef<any>, item: PaymentMethod) {
    this.model = new PaymentMethod();
    if (item != null) {
      this.model = new PaymentMethod(item.paymentMethodId, item.paymentMethodTypeId, item.code,
        item.name, item.accountTreeId, item.moduleTreeId, item.acC_ModuleAccount);
      if (this.model.moduleTreeId != null) {
        const event = { moduleTreeId: this.model.moduleTreeId, acC_ModuleAccount: this.model.acC_ModuleAccount };
        this.moduleTreeChange(event, this.model);
      }
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  moduleTreeChange(event, from?) {
    this.model.moduleTreeId = (event == null) ? null : event.moduleTreeId;
    const list = [];
    if (event != null) {
      event.acC_ModuleAccount.forEach(element => {
        list.push(element.accountTreeId);
      });
    }
    this.accountTreeList = (event == null) ? undefined : list;
    if (from === 'html') {
      this.model.accountTreeId = null;
    }
  }

  getPaymentMethodTypes() {
    this.paymentMethodSer.getPaymentMethodTypes()
      .subscribe((data) => {
        this.paymentMethodTypes = data;
      },
        () => {
          this.global.notificationMessage(4);
        });
  }

  getAll() {
    this.paymentMethodSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.PaymentMethods = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkPaymentMethodCode(model) {
    if (!(model.code == '' || model.code == null)) {
      this.paymentMethodSer.CheckPaymentMethodCode(model)
        .subscribe((data) => {
          this.checkCode = data;
        },
          () => {
            this.global.notificationMessage(4);
          });
    }
  }

  savePaymentMethod(): void {
    this.paymentMethodSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deletePaymentMethod(id) {
    this.paymentMethodSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

}
