import { ModuleAccountModel } from 'src/app/accounting/module-tree/module-tree.model';

export class PaymentMethod {
    constructor(
        public paymentMethodId: number = null,
        public paymentMethodTypeId: number = 1,
        public code: string = null,
        public name: string = null,
        public accountTreeId: number = null,
        public moduleTreeId: number = null,
        public acC_ModuleAccount: ModuleAccountModel[] = [],
    ) { }
}

export class PaymentMethodType {
    constructor(
        public paymentMethodTypeId: number = null,
        public nameAr: string = null,
        public nameEn: string = null
    ) { }
}

export class FilterPaymentMethod {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public name: any = null,
        public code: any = null,
        public paymentMethodTypeId: number = 0) {
    }
}
