import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PaymentMethodService {

  constructor(private http: HttpClient) { }

  CheckPaymentMethodCode(model): any {
    return this.http.post('PaymentMethod/CheckPaymentMethodCode', model);
  }

  getPaymentMethodTypes(): Observable<any> {
    return this.http.get('PaymentMethod/GetPaymentMethodTypes');
  }

  GetAll(model): Observable<any> {
    return this.http.post(`PaymentMethod/GetAll`, model);
  }

  GetFilteredSharedData(paymentMethodTypeId): Observable<any> {
    return this.http.get('PaymentMethod/GetFilteredSharedData?paymentMethodTypeId='+paymentMethodTypeId);
  }
  Save(model): any {
    return this.http.post('PaymentMethod/Operation', model);
  }

  Delete(id): any {
    return this.http.delete('PaymentMethod/Delete/' + id);
  }

}
