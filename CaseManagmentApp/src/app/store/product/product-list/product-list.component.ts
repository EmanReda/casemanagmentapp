import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FilterProduct } from './product-list.model';
import { ProductService } from '../product.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html'
})

export class ProductListComponent {
  searchModel: FilterProduct;
  products: any;
  total: number;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public productSer: ProductService,
    private translate: TranslateService) {
    this.fixed.subHeader = {
     display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Product.ProductList', url: '/store/product/list' }
      ],
      pageHeader: 'Product.ProductList'
    };
    this.searchModel = new FilterProduct();
    this.getAll();
  }

  getAll() {
    this.productSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.products = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  delete(id) {
    this.productSer.delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
