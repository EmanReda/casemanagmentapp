export class FilterProduct {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public code: string = null,
        public nameAr: string = null,
        public nameEn: string = null,
        public categoryId: number = null,
        public countryId: number = null,
        public hasBatchNumber: boolean = false,
        public hasDateValidity: boolean = false,
        public stockId: number = null) {
    }
}
