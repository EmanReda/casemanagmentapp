import { Component, OnInit } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ProductModel, ProductPrice } from '../product.model';
import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { PriceListService } from '../../store-shared/components/price-list/price-list.service';


@Component({
  selector: 'app-product-operation',
  templateUrl: './product-operation.component.html'
})

export class ProductOperationComponent implements OnInit {
  model: ProductModel;
  lockGridEdit = false;
  productPriceList = new ProductPrice();
  parms: any;
  checkCode: any;
  checkBarCode = false;
  barCodes: any[];
  validBar = true;
  flag: any;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private translate: TranslateService,
    private productSer: ProductService,
    private priceListSer: PriceListService) {
    this.priceListSer.getAll();
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Product.ProductList', url: '/store/product/list' }
      ],
      pageHeader: 'Product.ProductOperation'
    };
    this.model = new ProductModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Product.Add', url: '/store/product/operation' });
          this.fixed.subHeader.pageHeader = 'Product.Add';
          this.model.productId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Product.Edit', url: '/store/product/operation/' + id });
          this.fixed.subHeader.pageHeader = 'Product.Edit';
          this.model.productId = Number(id);
          this.getProduct(Number(id));
        }
      });
  }

  checkUnitBarCode(value: string, title: string) {
    this.barCodes = [];
    this.validBar = true;
    this.checkBarCode = false;
    this.barCodes = [
      { title: 'small', value: this.model.smallUomBarCode },
      { title: 'medium', value: this.model.mediumUomBarCode },
      { title: 'large', value: this.model.largeUomBarCode }];
    if (!(value == null || value == '')) {
      for (const item of this.barCodes) {
        if (title !== item['title'] && value === item['value']) {
          this.translate.get('Product.UnvalidBarCode')
            .subscribe((val) => {
              this.global.notificationMessage(3, null, val);
              this.validBar = false;
              this.flag = title;
            });
          break;
        }
      }
    }
    if (!(value == null || value == '') && (this.validBar)) {
      this.productSer.checkBarCode(value)
        .subscribe(
          (data) => {
            this.checkBarCode = data;
            this.flag = title;
            if (title == 'small' && this.checkBarCode) {
              this.translate.get('Product.SmallUomBarCodeExistsBefore')
                .subscribe((val) => {
                  this.global.notificationMessage(3, null, val);
                });
            } else if (title == 'medium' && this.checkBarCode) {
              this.translate.get('Product.MediumUomBarCodeExistsBefore')
                .subscribe((val) => {
                  this.global.notificationMessage(3, null, val);
                });
            } else if (title == 'large' && this.checkBarCode) {
              this.translate.get('Product.LargeUomBarCodeExistsBefore')
                .subscribe((val) => {
                  this.global.notificationMessage(3, null, val);
                });
            }
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  getProduct(id: number) {
    this.productSer.getProductById(id)
      .subscribe((Response) => {
        this.model = Response;
        this.model.stO_ProductPrice.forEach(element => {
          element.operation = 's';
        });
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkCodeOperation() {
    if (!(this.model.code == null || this.model.code == '')) {
      this.productSer.checkCodeOperation(this.model)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  checkPrice(name: string) {
    if (name == 'small' && this.model.smallUomName == null) {
      this.model.smallUomBarCode = null;
      this.model.stO_ProductPrice.forEach(element => {
        element.smallUomPrice = null;
      });
    }
    if (name == 'medium' && this.model.mediumUomName == null) {
      this.model.mediumUomBarCode = null;
      this.model.mediumUomFactor = null;
      this.model.stO_ProductPrice.forEach(element => {
        element.mediumUomPrice = null;
      });
    }
    if (name == 'large' && this.model.largeUomName == null) {
      this.model.largeUomBarCode = null;
      this.model.largeUomFactor = null;
      this.model.stO_ProductPrice.forEach(element => {
        element.largeUomPrice = null;
      });
    }
  }
  priceListOperation(data?: ProductPrice) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_ProductPrice == null) {
        this.model.stO_ProductPrice = [];
      }
      if (data == null) {
        this.model.stO_ProductPrice.push(new ProductPrice());
      } else {
        this.productPriceList = new ProductPrice(data.productPriceId,
          data.priceListId, data.productId, data.smallUomPrice, data.mediumUomPrice, data.largeUomPrice, data.isDeleted, 's');
        data.operation = 'e';
      }
    } else {
      this.translate.get('Product.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  priceListFinish(type, data, index?) {
    if (type === 'close') {
      data = this.productPriceList;
      this.lockGridEdit = false;
      this.model.stO_ProductPrice[index] = this.productPriceList;
    } else {
      if (data.priceListId == null
        || (data.smallUomPrice == null && this.model.smallUomName != null)
        || (data.mediumUomPrice == null && this.model.mediumUomName != null)
        || (data.largeUomPrice == null && this.model.largeUomName != null)) {
        this.translate.get('Product.CompletePeriod')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
      } else {
        this.lockGridEdit = false;
        data.operation = 's';
      }
    }
  }

  deletePriceList(item, index) {
    if (item.productPriceId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_ProductPrice.splice(index, 1);
    }
  }

  saveProduct() {
    this.productSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteProduct(id) {
    this.productSer.delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

}
