import { ProductOperationComponent } from './product-operation/product-operation.component';
import { ProductListComponent } from './product-list/product-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: ProductListComponent },
  { path: 'operation', component: ProductOperationComponent },
  { path: 'operation/:id', component: ProductOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
