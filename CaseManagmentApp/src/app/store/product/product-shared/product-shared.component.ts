import { Component, Input, Output, EventEmitter, ViewChild, OnChanges, SimpleChange } from '@angular/core';
import { ProductService } from '../product.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FilterProduct } from '../product-list/product-list.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-product-shared',
  templateUrl: './product-shared.component.html'
})
export class ProductSharedComponent implements OnChanges {
  products: any = [];
  modalRef: BsModalRef;
  search: any;
  model: FilterProduct;
  @Input() stockId: number;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();
  @ViewChild('select') select: any;

  constructor(
    public global: GlobalService,
    public fixed: FixedService,
    private productSer: ProductService,
    private modalService: BsModalService) {
    this.model = new FilterProduct();
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    this.products=[];
    this.model.stockId = this.stockId;
    this.getAll();
  }

  getAll() {
    this.productSer.QuickFilter(this.model)
      .subscribe((response) => {
        if (response.patch != null) {
          this.products = response.data;
          if (this.modalRef != null) {
            this.modalRef.hide();
          }
          this.select.close();
          this.model.code = response.patch.barcode;
          this.selectedValue = response.data[0];
          this.outputFunction(response.data[0], response.patch);
        } else if (response.data.length > 0) {
          this.products = this.products.concat(response.data);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  openModal(template: any) {
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  searchFunction(term) {
    this.model.code = term;
    this.model.page = 1;
    this.products.length = 0;
    this.getAll();
  }

  customSearchFn(term: string, item: any) {
    return item;
  }

  outputFunction(item, patchData?) {
    const data = { code: this.model.code, element: item, patch: patchData };
    (item != null) ? this.selectedValueChanges.emit(data) : this.selectedValueChanges.emit(null);
  }
}
