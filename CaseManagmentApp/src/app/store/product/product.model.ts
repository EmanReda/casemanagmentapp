export class ProductModel {
  constructor(
    public productId: number = null,
    public code: string = null,
    public nameAr: string = null,
    public nameEn: string = null,
    public categoryId: number = null,
    public countryId: number = null,
    public smallUomName: number = null,
    public smallUomBarCode: string = null,
    public mediumUomName: number = null,
    public mediumUomBarCode: string = null,
    public mediumUomFactor: number = null,
    public largeUomName: number = null,
    public largeUomBarCode: string = null,
    public largeUomFactor: number = null,
    public maxDiscountValue: number = null,
    public hasBatchNumber: boolean = false,
    public hasDateValidity: boolean = false,
    public vat: number = null,
    public stO_ProductPrice: ProductPrice[] = []) { }
}

export class ProductPrice {
  constructor(
    public productPriceId: number = null,
    public priceListId: number = null,
    public productId: number = null,
    public smallUomPrice: number = null,
    public mediumUomPrice: number = null,
    public largeUomPrice: number = null,
    public isDeleted: boolean = false,
    public operation: string = 'e') { }
}
