import { SharedModule } from './../../shared/shared.module';
import { ProductSharedComponent } from './product-shared/product-shared.component';
import { ProductOperationComponent } from './product-operation/product-operation.component';
import { ProductListComponent } from './product-list/product-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product-routing.module';
import { StoreSharedModule } from '../store-shared/store-shared.module';
import { CategoryModule } from '../category/category.module';

@NgModule({
  declarations: [
    ProductListComponent,
    ProductOperationComponent,
    ProductSharedComponent,
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    SharedModule.forRoot(),
    StoreSharedModule,
    CategoryModule  
  ],
  exports: [
    ProductSharedComponent
  ]
})
export class ProductModule { }
