import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ProductService {

  constructor(private http: HttpClient) { }

  getAll(model): Observable<any> {
    return this.http.post(`Product/GetAll`, model);
  }

  QuickFilter(model): Observable<any> {
    return this.http.post(`Product/QuickFilter`, model);
  }

  getProductById(id: number): any {
    return this.http.get(`Product/GetProductById/${id}`);
  }

  checkCodeOperation(model): any {
    return this.http.post('Product/CheckCode', model);
  }

  checkBarCode(code): any {
    return this.http.get('Product/CheckBarCode?code=' + code);
  }

  save(model: any): Observable<any> {
    return this.http.post(`Product/Operation`, model);
  }

  delete(id): Observable<any> {
    return this.http.delete(`Product/Delete/${id}`);
  }
}
