import { Component, OnInit } from '@angular/core';
import { FilterStockReceivableClient } from './receivable-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ReceivableService } from '../receivable.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-receivable-list',
  templateUrl: './receivable-list.component.html'
})
export class ReceivableListComponent {
  searchModel: FilterStockReceivableClient;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private receivableListSer: ReceivableService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Receivable.List', url: '/store/receivable/list' }
      ],
      pageHeader: 'Receivable.List'
    };
    this.searchModel = new FilterStockReceivableClient();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.receivableListSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterStockReceivableClient();
    this.total = undefined;
    this.dataList = undefined;
  }
}
