export class FilterStockReceivableClient {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public transactionNo: string = null, //receivable no
        public fromDate: any = null,
        public toDate: any = null,
        public clientId: number = null,
        public stockId: number = null,
        public paymentMethodId : number = null ,
        public isVoided: boolean = null) { }
}
