import { Component, OnInit, ViewChild ,TemplateRef} from '@angular/core';
import { ReceivableModel,ReceivableItem } from './receivable-operation.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ReceivableService } from '../receivable.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-receivable-operation',
  templateUrl: './receivable-operation.component.html'
})
export class ReceivableOperationComponent implements OnInit {
  model: ReceivableModel;
  selectedItem: ReceivableItem;
  parms: any;
  lockGridEdit = false;
  invoiceList: any;
  returnList: any;
  receivableItem: ReceivableItem;
  listPage = '/store/receivable/list';
  modalRef: BsModalRef;
  @ViewChild('operationError') operationError: SwalComponent;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private receivableSer: ReceivableService,
    private localeService: BsLocaleService,
    private modalService: BsModalService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Receivable.List', url: this.listPage }
      ],
      pageHeader: 'Receivable.pageHeader'
    };
    this.model = new ReceivableModel();
    this.selectedItem = new ReceivableItem();
  }
  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Receivable.Add', url: '/store/receivable/operation' });
          this.fixed.subHeader.pageHeader = 'Receivable.Add';
          this.model.receivableId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Common.Show', url: '' });
          this.fixed.subHeader.pageHeader = 'Common.Show';
          this.model.receivableId = Number(id);
          this.getById(Number(id));
        }
      });
  }
  getById(id: number) {
    this.receivableSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate([this.listPage]);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  openModal(template: TemplateRef<any>) {
    if(this.model.receivableId == null && this.model.clientId != null && this.model.paymentMethodId != null){
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
    }
    else if (this.model.clientId == null) {
      this.translate.get('Receivable.SelectClient')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
    else if (this.model.paymentMethodId == null) {
      this.translate.get('Receivable.SelectPaymentMethod')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  getTransactionNo(data) {
    if (data != null && this.model.receivableId == null) {
      this.receivableSer.getTransactionNo(this.global.formatDate(data))
        .subscribe((response) => {
          this.model.receivableNo = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  getInvoicesAndReturns(model) {
    if (model.paymentMethodId != null && model.clientId != null) {
      this.receivableSer.getInvoices(model.clientId, model.paymentMethodId)
        .subscribe((response) => {
          this.invoiceList = response;
        }, () => {
          this.global.notificationMessage(4);
        });

      this.receivableSer.getReturns(model.clientId, model.paymentMethodId)
        .subscribe((response) => {
          this.returnList = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }
  getInvoiceData(item, type) {
    item.total = 0.0;
    if (type == 'invoice') {
      item.returnId = null;
      const invoice = this.invoiceList.filter(s => s.invoiceId == item.invoiceId);
      item.total = invoice[0].total;
      item.rest = invoice[0].total - invoice[0].paidAmount;
      item.transactionNo = invoice[0].transactionNo;
    } else if (type == 'return') {
      item.invoiceId = null;
      const returnInv = this.returnList.filter(s => s.returnId == item.returnId);
      item.total = returnInv[0].total;
      item.rest = returnInv[0].total - returnInv[0].paidAmount;
      item.transactionNo = returnInv[0].transactionNo;
    }
  }

  itemDelete(item, index) {
    if (item.receivableItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_ReceivableItem.splice(index, 1);
    }
    item.amount = 0;
    this.calcTotal();
  }
  saveItem(item) {
    this.receivableItem = new ReceivableItem(item.receivableItemId, item.receivableId, item.invoiceId, item.returnId,
      item.amount, item.total, item.rest, item.transactionNo, item.type, item.isEdited, false);

    if (item.invoiceId == null && item.returnId == null) {
      this.translate.get('Receivable.ChooseInvoiceOrReturn')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    }
    if (item.amount == null || item.amount == 0) {
      this.translate.get('Receivable.EnterAmount')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    }
    const prev = this.model.stO_ReceivableItem.filter(s => s.invoiceId == item.invoiceId);
    if (prev.length == 0) {
      if (item.amount > item.rest) {
        this.translate.get('Receivable.AmountGreaterThanRest')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      this.model.stO_ReceivableItem.push(this.receivableItem);
    } else {
      const x = prev[0].amount + item.amount;
      if (x > item.rest) {
        this.translate.get('Receivable.AmountGreaterThanRest')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      prev[0].amount = prev[0].amount + item.amount;
    }

    this.calcTotal();
    this.New();
  }

  New() {
    this.selectedItem = new ReceivableItem();
  }
  calcTotal() {
    this.model.total = 0.0;
    let sumOfInvoices = 0.0;
    let sumOfReturns = 0.0;
    this.model.stO_ReceivableItem.forEach(element => {
      if (element.invoiceId != null) {
        sumOfInvoices += element.amount;
      } else if (element.returnId != null) {
        sumOfReturns += element.amount;
      }
    });
    this.model.total = sumOfInvoices - sumOfReturns;
  }

  save() {
    this.model.date = this.global.formatDate(this.model.date);
    this.receivableSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  voidedTransaction(id) {
    this.receivableSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }


}
