export class ReceivableModel {
    constructor(
        public receivableId: number = null,
        public stockId: number = null,
        public clientId: number = null,
        public paymentMethodId: number = null,
        public receivableNo: string = null,
        public date: any = null,
        public total: number = 0.0,
        public type: string = null,
        public payment: boolean = true,
        public isPrinted: boolean = false,
        public isVoided: boolean = false,
        public stO_ReceivableItem: ReceivableItem[] = []) { }
}

export class ReceivableItem {
    constructor(
        public receivableItemId: number = null,
        public receivableId: number = null,
        public invoiceId: number = null,
        public returnId: number = null,
        public amount: number = null,
        public total: number = null,
        public rest: number = null,
        public transactionNo: string = null,
        public type: string = null,
        public isEdited: boolean = false,
        public isDeleted: boolean = false) { }
}

