import { ReceivableOperationComponent } from './receivable-operation/receivable-operation.component';
import { ReceivableListComponent } from './receivable-list/receivable-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: ReceivableListComponent },
  { path: 'operation', component: ReceivableOperationComponent },
  { path: 'operation/:id', component: ReceivableOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceivableRoutingModule { }
