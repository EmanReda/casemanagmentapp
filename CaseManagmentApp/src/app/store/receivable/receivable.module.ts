import { SharedModule } from './../../shared/shared.module';
import { ReceivableOperationComponent } from './receivable-operation/receivable-operation.component';
import { ReceivableListComponent } from './receivable-list/receivable-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceivableRoutingModule } from './receivable-routing.module';
import { StockModule } from '../stock/stock.module';
import { PaymentMethodModule } from '../payment-method/payment-method.module';
import { ClientModule} from './../../client/client.module';
@NgModule({
  declarations: [
    ReceivableListComponent,
    ReceivableOperationComponent,
  ],
  imports: [
    CommonModule,
    ReceivableRoutingModule,
     SharedModule.forRoot(),
     StockModule,
     PaymentMethodModule,
     ClientModule
  ]
})
export class ReceivableModule { }
