import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ReceivableService {

  constructor(private http: HttpClient) { }

  getAll(model): Observable<any> {
    return this.http.post(`Receivable/GetAll`, model);
  }

  getTransactionNo(date): Observable<any> {
    return this.http.get('Receivable/GetTransactionNo?date=' + date);
  }
  getInvoices(clientId,payment): any {
    return this.http.get('Receivable/GetInvoices?clientId=' + clientId + '&payment=' + payment);
  }

  getReturns(clientId, payment): any {
    return this.http.get('Receivable/GetReturns?clientId=' + clientId + '&payment=' + payment);
  }

  getById(id: number): any {
    return this.http.get(`Receivable/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`Receivable/Operation`, model);
  }
  
  voided(id): Observable<any> {
    return this.http.delete(`Receivable/Voided/${id}`);
  }
}
