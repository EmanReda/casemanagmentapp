import { Component } from '@angular/core';
import { FilterReturn } from './return-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ReturnService } from '../return.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-return-list',
  templateUrl: './return-list.component.html'
})
export class ReturnListComponent {
  searchModel: FilterReturn;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private returnSer: ReturnService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Return.List', url: '/store/return/list' }
      ],
      pageHeader: 'Return.List'
    };
    this.searchModel = new FilterReturn();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.returnSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterReturn();
    this.total = undefined;
    this.dataList = undefined;
  }
}
