import { Component, OnInit, ViewChild } from '@angular/core';
import { ReturnModel, ReturnItem } from './return-operation.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ReturnService } from '../return.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { InvoiceModel } from '../../invoice/invoice-amount/invoice-amount.model';

@Component({
  selector: 'app-return-operation',
  templateUrl: './return-operation.component.html'
})
export class ReturnOperationComponent implements OnInit {
  model: ReturnModel;
  invoicemodel: InvoiceModel;
  productList: any[];
  distinctProducts: any[];
  unitBarCodeList: any[];
  parms: any;
  lockGridEdit = false;
  returnItem = new ReturnItem();
  editedItemQty = null;
  @ViewChild('operationError') operationError: SwalComponent;
  listPage = '/store/return/list';

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private returnSer: ReturnService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Return.List', url: this.listPage }
      ],
      pageHeader: 'Return.Add'
    };
    this.model = new ReturnModel();
    this.invoicemodel = new InvoiceModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Return.Add', url: '/store/return/operation' });
          this.fixed.subHeader.pageHeader = 'Return.Add';
          this.model.returnId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'Return.Edit', url: '/store/return/operation/' + id });
          this.fixed.subHeader.pageHeader = 'Return.Edit';
          this.model.returnId = Number(id);
          this.getById(Number(id));
        }
      });
  }

  getTransactionNo(data) {
    if (data != null && this.model.returnId == null) {
      this.returnSer.getTransactionNo(this.global.formatDate(data))
        .subscribe((response) => {
          this.model.transactionNo = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  getById(id: number) {
    this.returnSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.invoicemodel = Response.invoiceTransaction;
          this.invoiceOperation(this.invoicemodel);
          this.model.stO_ReturnItem.forEach(item => {
            item.operation = 's';
            this.productChanges(item);
          });
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate([this.listPage]);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  getInvoiceData(transactionNumber: string) {
    this.returnSer.getInvoiceData(transactionNumber)
      .subscribe((Response) => {
        if (Response != null) {
          if (this.global.formatDate(Response.date) > this.global.formatDate(this.model.date)) {
            this.model.date = null;
          }
          this.invoicemodel = Response;
          this.invoicemodel.date = new Date(Response.date);
          this.invoiceOperation(this.invoicemodel);
        } else {
          this.translate.get('Return.UnfoundInvoice')
            .subscribe((val) => {
              this.global.notificationMessage(3, null, val);
            });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  invoiceOperation(Response: any) {
    this.model.invoiceTransactionNo = Response.transactionNo;
    this.model.stockId = Response.stockId;
    this.model.clientId = Response.clientId;
    this.model.invoiceId = Response.invoiceId;
    this.distinctProducts = [];
    Response.stO_InvoiceItem.forEach(element => {
      const exist = this.distinctProducts.filter(s => s.productId == element.productId);
      if (exist.length == 0) {
        this.distinctProducts.push(element.product);
      }
    });
    this.productList = Response.stO_InvoiceItem;
  }

  productChanges(item) {
    item.selectedProductData = [];
    item.units = [];
    item.batchExpireList = [];
    item.uomBarCode = null;
    item.batchExpire = null;
    item.batchNumber = null;
    item.expiryDate = null;
    if(this.model.returnId == null){
      item.qtyInUom = null;
      item.uomPrice = 0;
      item.uomDiscountValue = 0;
    }
    item.selectedProductData.push(this.productList.filter(s => s.productId === item.product.productId));
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.uomBarCode === ele.product.smallUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.smallUomBarCode,
            uomBarCodeName: ele.product.smallUomName
          });
        } else if (ele.uomBarCode === ele.product.mediumUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.mediumUomBarCode,
            uomBarCodeName: ele.product.mediumUomName
          });
        } else if (ele.uomBarCode === ele.product.largeUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.largeUomBarCode,
            uomBarCodeName: ele.product.largeUomName
          });
        }
      });
      if (item.uomBarCode == null && item.units.length == 1) {
        item.uomBarCode = item.units[0].uomBarCode;
        this.unitChanges(item);
      }
    });
  }

  unitChanges(item) {
    item.batchExpireList = [];
    item.batchExpire = null;
    item.batchNumber = null;
    item.expiryDate = null;
    if(this.model.returnId == null){
      item.qtyInUom = null;
      item.uomPrice = 0;
      item.uomDiscountValue = 0;
    }
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.uomBarCode == item.uomBarCode && ele.productId == item.product.productId) {
          item.batchExpireList.push({
            batchExpire: {
              batchNumber: ele.batchNumber,
              expiryDate: ele.expiryDate
            },
            batchNumber: ele.batchNumber,
            expiryDate: ele.expiryDate
          });
        }
      });
      if (item.batchExpire == null && item.batchExpireList.length == 1) {
        item.batchExpire = item.batchExpireList[0].batchExpire;
        item.batchNumber = item.batchExpire.batchNumber;
        item.expiryDate = item.batchExpire.expiryDate;
        this.batchExpireChange(item);
      }
    });
  }

  batchExpireChange(item) {
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.productId == item.product.productId && ele.uomBarCode == item.uomBarCode
          && ele.expiryDate == item.batchExpire.expiryDate
          && ele.batchNumber == item.batchExpire.batchNumber) {
          item.uomPrice = ele.uomPrice;
          item.uomDiscountValue = ele.uomDiscountValue;
          item.oldTotalAfterDiscount = ele.totalAfterDiscount;
          item.oldTotalVatValue = ele.totalVatValue;
          item.oldQtyInUom = ele.qtyInUom;
          item.qtyInStock = ele.qtyInStock;
          item.restQtyInInvoice = ele.restQtyInInvoice;
        }
      });
    });
  }

  clacItemvalues(item) {
      item.totalPrice = item.qtyInUom * item.uomPrice;
      item.totalDiscount = item.qtyInUom * item.uomDiscountValue;
      item.totalAfterDiscount = item.totalPrice - item.totalDiscount;
      item.vatPercentage = item.oldTotalVatValue * (item.qtyInUom / item.oldQtyInUom);
      item.totalVatValue = (item.totalAfterDiscount * item.vatPercentage) / 100;
      item.totalVatValue = Math.round(item.totalVatValue * 100) / 100;
  }

  itemOperation(data?: ReturnItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_ReturnItem == null) {
        this.model.stO_ReturnItem = [];
      }
      if (data == null) {
        this.model.stO_ReturnItem
          .push(new ReturnItem(null, null, this.model.stO_ReturnItem.length + 1));
      } else {
        this.editedItemQty = data.qtyInUom;
        this.returnItem = new ReturnItem(data.returnItemId, data.returnId,
          data.itemId, data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.oldQtyInUom, data.uomPrice, data.totalPrice, data.uomDiscountPercentage, data.uomDiscountValue,
          data.totalAfterDiscount, data.oldAfterDiscount, data.vatPercentage, data.totalVatValue, data.oldTotalVatValue,
          data.totalDiscount, data.product, data.unitId, data.units,
          data.selectedProductData, data.batchExpireList, data.batchExpire, data.qtyInStock,
          data.restQtyInInvoice, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('Return.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.returnItem;
      this.lockGridEdit = false;
      (data.returnItem == null) ? this.model.stO_ReturnItem.splice(index, 1) :
        this.model.stO_ReturnItem[index] = this.returnItem;
    } else {
      if (data.product == null) {
        this.translate.get('Return.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.batchExpire == null || data.batchExpire == '') {
        this.translate.get('Return.CompleteBatchNumberAndExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('Return.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom == 0) {
        this.translate.get('Return.EnterQtyInUom')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if ((data.qtyInUom >= data.restQtyInInvoice) && !(data.qtyInUom == this.editedItemQty)){
        this.translate.get('Return.NotAllowedQnty')
          .subscribe((val) => {
         this.global.notificationMessage(3, null, val);
        });
        data.qtyInUom = this.editedItemQty;
        this.clacItemvalues(data);
       return;
     }
      const prev = this.model.stO_ReturnItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber && s.product.productId == data.product.productId);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
        this.clacItemvalues(prev[0]);
        if (prev[0].qtyInUom >= data.restQtyInInvoice) {
          this.translate.get('Return.NotAllowedQnty')
           .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
         });
          return;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
      this.calcValues();
    }
  }

  calcValues() {
    this.model.total = 0;
    this.model.totalVAT = 0;
    this.model.totalAfterDiscount = 0;
    this.model.stO_ReturnItem.forEach(element => {
      this.model.total += element.totalPrice;
      this.model.totalVAT += Math.round(element.totalVatValue * 100) / 100;
      this.model.totalAfterDiscount += element.totalAfterDiscount;
    });
  }

  itemDelete(item, index) {
    if (item.stockReturnVoucherItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_ReturnItem.splice(index, 1);
    }
  }

  save() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_ReturnItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.returnSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  voidedTransaction(id) {
    this.returnSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  searchInProducts(event) {
    if (this.productList != null) {
      const barcodeBatchItem = this.productList.filter(s => s.barcodeBatchNumber == event.term)[0];
      const editedItem = this.model.stO_ReturnItem.filter(s => s.operation === 'e')[0];
      if (event.items.length === 0 && barcodeBatchItem != null) {
        editedItem.product = barcodeBatchItem.product;
        this.productChanges(editedItem);
        editedItem.uomBarCode = barcodeBatchItem.uomBarCode;
        this.unitChanges(editedItem);
        editedItem.batchNumber = barcodeBatchItem.batchNumber;
        editedItem.expiryDate = barcodeBatchItem.expiryDate;
        editedItem.batchExpire = {
          batchExpire: {
            batchNumber: barcodeBatchItem.batchNumber,
            expiryDate: barcodeBatchItem.expiryDate
          },
          batchNumber: barcodeBatchItem.batchNumber,
          expiryDate: barcodeBatchItem.expiryDate
        };
      } else if (event.items.length === 1 &&
        ((event.items[0].largeUomBarCode != null && event.items[0].largeUomBarCode == event.term) ||
          (event.items[0].mediumUomBarCode != null && event.items[0].mediumUomBarCode == event.term) ||
          (event.items[0].largeUomBarCode != null && event.items[0].largeUomBarCode == event.term))) {
        editedItem.product = event.items[0];
        this.productChanges(editedItem);
        editedItem.uomBarCode = event.term;
        this.unitChanges(editedItem);
      }
    } else {
      this.translate.get('Return.InvoiceNumber')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  customSearchFn(term: string, item?: any) {
    if (item.category != null && item.category.includes(term)) {
      return item;
    } else if (item.code != null && item.code.includes(term)) {
      return item;
    } else if (item.largeUomBarCode != null && item.largeUomBarCode.includes(term)) {
      return item;
    } else if (item.mediumUomBarCode != null && item.mediumUomBarCode.includes(term)) {
      return item;
    } else if (item.smallUomBarCode != null && item.smallUomBarCode.includes(term)) {
      return item;
    } else if (item.nameAr != null && item.nameAr.includes(term)) {
      return item;
    } else if (item.nameEn != null && item.nameEn.includes(term)) {
      return item;
    }
    return null;
  }
  PrintReceipt() {
    this.returnSer.PrintReceipt(this.model.returnId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
