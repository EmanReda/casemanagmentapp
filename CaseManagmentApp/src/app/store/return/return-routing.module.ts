import { ReturnOperationComponent } from './return-operation/return-operation.component';
import { ReturnListComponent } from './return-list/return-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: ReturnListComponent },
  { path: 'operation', component: ReturnOperationComponent },
  { path: 'operation/:id', component: ReturnOperationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReturnRoutingModule { }
