import { SharedModule } from './../../shared/shared.module';
import { ReturnOperationComponent } from './return-operation/return-operation.component';
import { ReturnListComponent } from './return-list/return-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnRoutingModule } from './return-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';
import { ClientModule} from './../../client/client.module';
@NgModule({
  declarations: [
    ReturnListComponent,
    ReturnOperationComponent,
  ],
  imports: [
    CommonModule,
    ReturnRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule,
    ClientModule
  ]
})
export class ReturnModule { }
