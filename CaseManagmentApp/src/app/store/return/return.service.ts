import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ReturnService {

  constructor(private http: HttpClient) { }
  
  getTransactionNo(date): Observable<any> {
    return this.http.get('Return/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`Return/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`Return/Operation`, model);
  }

  voided(id): Observable<any> {
    return this.http.delete(`Return/Voided/${id}`);
  }

  getAll(model): Observable<any> {
    return this.http.post(`Return/GetAll`, model);
  }
  getInvoiceData(transactionNumber:string):any{
    return this.http.get('Return/GetInvoiceData?transactionNumber='+transactionNumber);
  }
  PrintReceipt(id): any {
    return this.http.get('Return/ReturnReport/' + id);
  }
}
