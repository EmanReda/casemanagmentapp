import { Component } from '@angular/core';
import { FilterStockInDocumentList } from './stock-in-document-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockInDocumentService } from '../stock-in-document.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-in-document-list',
  templateUrl: './stock-in-document-list.component.html'
})

export class StockInDocumentListComponent {
  searchModel: FilterStockInDocumentList;
  total: number;
  stocks: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public stockInSer: StockInDocumentService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockInDocument.List', url: '/store/stock-in-document/list' }
      ],
      pageHeader: 'StockInDocument.List'
    };
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterStockInDocumentList();
    this.getAll();
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockInSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.stocks = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  resetSearch() {
    this.searchModel = new FilterStockInDocumentList();
    this.total = undefined;
    this.stocks = undefined;
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
