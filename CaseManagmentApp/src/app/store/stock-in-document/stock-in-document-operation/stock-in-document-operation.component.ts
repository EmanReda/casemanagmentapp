import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StockInDocumentService } from '../stock-in-document.service';
import { StockInDocumentModel, StockInDocumentItem } from './stock-in-document-operation.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ApiService } from 'src/app/shared/services/api.service';
import { DatePipe } from '../../../../../node_modules/@angular/common';

@Component({
  selector: 'app-stock-in-document-operation',
  templateUrl: './stock-in-document-operation.component.html',
  providers: [DatePipe]
})

export class StockInDocumentOperationComponent implements OnInit {
  model: StockInDocumentModel;
  parms: any;
  lockGridEdit = false;
  stockItem = new StockInDocumentItem();
  @ViewChild('operationError') operationError: SwalComponent;
  emptyList = false;
  checkDate = false;
  Temptoday: any;
  TempExpireDate: any;

  constructor(
    private datePipe: DatePipe,
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockInDocSer: StockInDocumentService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockInDocument.List', url: '/store/stock-in-document/list' }
      ],
      pageHeader: 'StockInDocument.StockInDocument'
    };
    this.model = new StockInDocumentModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockInDocument.Add', url: '/store/stock-in-document/operation' });
          this.fixed.subHeader.pageHeader = 'StockInDocument.Add';
          this.model.stockInDocumentId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'StockInDocument.Edit', url: '/store/stock-in-document/operation/' + id });
          this.fixed.subHeader.pageHeader = 'StockInDocument.Edit';
          this.model.stockInDocumentId = Number(id);
          this.getStockInDocument(Number(id));
        }
      });
  }

  getStockInDocument(id: number) {
    this.stockInDocSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.checkDate = true;
          this.model = Response;
          this.model.stO_StockInDocumentItem.forEach(item => {
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.generateUnitList(item);
            item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
          });
        } else {
          this.closePage();
        }
      }, () => {
        this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate(['store/stock-in-document-list']);
    }, 5000);
  }

  getTransactionNo(data) {
    if (data != null && this.model.stockInDocumentId == null) {
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
          if (this.checkDate == false) {
            this.model.transactionNo = null;
          } else {
            this.stockInDocSer.getTransactionNo(this.global.formatDate(data))
              .subscribe((res) => {
                this.model.transactionNo = res;
                this.checkDate = true;
              }, () => {
                this.global.notificationMessage(4);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: StockInDocumentItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_StockInDocumentItem == null) {
        this.model.stO_StockInDocumentItem = [];
      }
      if (data == null) {
        this.model.stO_StockInDocumentItem.push(new StockInDocumentItem(null, null, this.model.stO_StockInDocumentItem.length + 1));
      } else {
        this.stockItem = new StockInDocumentItem(data.stockInDocumentItemId, data.stockInDocumentId, data.itemId,
          data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.product, data.unitId, data.units, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('StockInDocument.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockItem;
      this.lockGridEdit = false;
      (data.stockInDocumentItemId == null) ? this.model.stO_StockInDocumentItem.splice(index, 1) :
        this.model.stO_StockInDocumentItem[index] = this.stockItem;
    } else {
      if (data.product == null) {
        this.translate.get('StockInDocument.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockInDocument.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockInDocument.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == false && !(data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockInDocument.HasNotBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.batchNumber = null;
          });
      }
      this.Temptoday = this.datePipe.transform(this.fixed.today, 'yyyy-MM-dd');
      this.TempExpireDate = this.datePipe.transform(data.expiryDate, 'yyyy-MM-dd');
      if (data.product.hasDateValidity == false && !(data.expiryDate == null || data.expiryDate == ''
        || this.TempExpireDate < this.Temptoday)) {
        this.translate.get('StockInDocument.HasNotExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.expiryDate = null;
          });
      }
      if (data.qtyInUom < 1) {
        this.translate.get('StockInDocument.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockInDocument.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomPrice <= 0 || data.uomPrice == null) {
        this.translate.get('StockTransferDocument.EnterValidUomPrice')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_StockInDocumentItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber);
      if (prev.length === 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        if (prev[0].isDeleted === true) {
          prev[0].qtyInUom = data.qtyInUom;
          prev[0].uomPrice = data.uomPrice;
          prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
          prev[0].totalPrice = Math.round(prev[0].totalPrice * 100) / 100;
          prev[0].isDeleted = null;
          prev[0].isEdited = true;
        } else {
          prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
          prev[0].uomPrice = Math.round(prev[0].uomPrice * 100) / 100;
          prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
          prev[0].totalPrice = Math.round(prev[0].totalPrice * 100) / 100;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    const len = this.model.stO_StockInDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  itemDelete(item, index) {
    if (item.stockInDocumentItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockInDocumentItem.splice(index, 1);
    }
    const len = this.model.stO_StockInDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
    this.calcTotal();
  }

  selectProduct(event, item: StockInDocumentItem) {
    if (event != null) {
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      this.generateUnitList(item);
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
      }
    } else {
      item.product = null;
      item.barcode = null;
      item.productId = null;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      item.units = null;
    }
  }

  generateUnitList(item) {
    item.units = [];
    if (item.product.smallUomNameId != null) {
      const unit = {
        unitId: item.product.smallUomNameId,
        name: item.product.smallUomName,
        barcode: item.product.smallUomBarCode,
        size: 's'
      };
      item.units.push(unit);
    }
    if (item.product.mediumUomNameId != null) {
      const unit = {
        unitId: item.product.mediumUomNameId,
        name: item.product.mediumUomName,
        barcode: item.product.mediumUomBarCode,
        size: 'm'
      };
      item.units.push(unit);
    }
    if (item.product.largeUomNameId != null) {
      const unit = {
        unitId: item.product.largeUomNameId,
        name: item.product.largeUomName,
        barcode: item.product.largeUomBarCode,
        size: 'l'
      };
      item.units.push(unit);
    }
  }

  unitChange(item) {
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
  }

  saveStockInDocument() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_StockInDocumentItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.stockInDocSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-in-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  calcTotal() {
    this.model.total = 0;
    this.model.stO_StockInDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).forEach(item => {
      this.model.total = this.model.total + (item.uomPrice * item.qtyInUom);
    });
  }

  voidedTransaction(id) {
    this.stockInDocSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-in-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  PrintReceipt() {
    this.stockInDocSer.PrintReceipt(this.model.stockInDocumentId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}

