import { StockInDocumentOperationComponent } from './stock-in-document-operation/stock-in-document-operation.component';
import { StockInDocumentListComponent } from './stock-in-document-list/stock-in-document-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockInDocumentListComponent },
  { path: 'operation', component: StockInDocumentOperationComponent },
  { path: 'operation/:id', component: StockInDocumentOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockInDocumentRoutingModule { }
