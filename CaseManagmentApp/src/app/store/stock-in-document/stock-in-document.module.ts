import { SharedModule } from './../../shared/shared.module';
import { StockInDocumentOperationComponent } from './stock-in-document-operation/stock-in-document-operation.component';
import { StockInDocumentListComponent } from './stock-in-document-list/stock-in-document-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockInDocumentRoutingModule } from './stock-in-document-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockInDocumentListComponent,
    StockInDocumentOperationComponent,
  ],
  imports: [
    CommonModule,
    StockInDocumentRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockInDocumentModule { }
