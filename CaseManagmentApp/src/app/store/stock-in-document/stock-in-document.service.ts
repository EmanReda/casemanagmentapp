import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockInDocumentService {

  constructor(private http: HttpClient) { }
  GetAll(model):Observable<any>{
    return this.http.post('StockInDocument/GetAll',model);
  }
  getTransactionNo(date): Observable<any> {
    return this.http.get('StockInDocument/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`StockInDocument/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`StockInDocument/Operation`, model);
  }
  voided(id): Observable<any> {
    return this.http.delete(`StockInDocument/Voided/${id}`);
  }
  PrintReceipt(id): any {
    return this.http.get('StockInDocument/DocumentReport/' + id);
  }
}

