import { Component } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FilterStoreOpening } from './stock-opening-list.model';
import { StockOpeningService } from '../stock-opening.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-opening-list',
  templateUrl: './stock-opening-list.component.html'
})
export class StockOpeningListComponent {
  searchModel: FilterStoreOpening;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private stockOpeningSer: StockOpeningService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockOpening.List', url: '/store/stock-opening/list' }
      ],
      pageHeader: 'StockOpening.StockOpening'
    };
    this.searchModel = new FilterStoreOpening();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.dateFrom = this.global.formatDate(this.searchModel.dateFrom);
    this.searchModel.dateTo = this.global.formatDate(this.searchModel.dateTo);
    this.stockOpeningSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterStoreOpening();
    this.total = undefined;
    this.dataList = undefined;
  }
}
