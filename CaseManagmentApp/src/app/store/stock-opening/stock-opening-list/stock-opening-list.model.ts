export class FilterStoreOpening {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public transactionNo: string = null,
        public stockId: number = null,
        public dateFrom: any = null,
        public dateTo: any = null,
        public isVoided: boolean = null) { }
}
