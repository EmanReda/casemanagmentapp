import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StockOpeningService } from '../stock-opening.service';
import { StockOpeningModel, StockOpeningItem } from './stock-opening-operation.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ApiService } from 'src/app/shared/services/api.service';
import { DatePipe } from '../../../../../node_modules/@angular/common';

@Component({
  selector: 'app-stock-opening-operation',
  templateUrl: './stock-opening-operation.component.html'
})

export class StockOpeningOperationComponent implements OnInit {
  model: StockOpeningModel;
  parms: any;
  lockGridEdit = false;
  stockItem = new StockOpeningItem();
  @ViewChild('operationError') operationError: SwalComponent;
  emptyList = false;
  checkDate = false;
  listPage = '/store/stock-opening/list';
  operationPage = '/store/stock-opening/operation/';

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockOpeningSer: StockOpeningService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockOpening.List', url: this.listPage }
      ],
      pageHeader: 'StockOpening.StockOpening'
    };
    this.model = new StockOpeningModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockOpening.Add', url: this.operationPage });
          this.fixed.subHeader.pageHeader = 'StockOpening.Add';
          this.model.stockOpeningId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'StockOpening.Edit', url: this.operationPage + id });
          this.fixed.subHeader.pageHeader = 'StockOpening.Edit';
          this.model.stockOpeningId = Number(id);
          this.getStockOpening(Number(id));
        }
      });
  }

  getStockOpening(id: number) {
    this.stockOpeningSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.checkDate = true;
          this.model = Response;
          this.model.stO_StockOpeningItem.forEach(item => {
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.generateUnitList(item);
            item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
          });
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate([this.listPage]);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  getTransactionNo(data) {
    if (data != null && this.model.stockOpeningId == null) {
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
          if (this.checkDate === false) {
            this.model.transactionNo = null;
          } else {
            this.stockOpeningSer.getTransactionNo(this.global.formatDate(data))
              .subscribe((respo) => {
                this.model.transactionNo = respo;
                this.checkDate = true;
              }, () => {
                this.global.notificationMessage(4);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: StockOpeningItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_StockOpeningItem == null) {
        this.model.stO_StockOpeningItem = [];
      }
      if (data == null) {
        this.model.stO_StockOpeningItem.push(new StockOpeningItem(null, null, this.model.stO_StockOpeningItem.length + 1));
      } else {
        this.stockItem = new StockOpeningItem(data.stockOpeningItemId, data.stockOpeningId, data.itemId,
          data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.product, data.unitId, data.units, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('StockOpening.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockItem;
      this.lockGridEdit = false;
      (data.stockOpeningItemId == null) ? this.model.stO_StockOpeningItem.splice(index, 1) :
        this.model.stO_StockOpeningItem[index] = this.stockItem;
    } else {
      if (data.product == null) {
        this.translate.get('StockOpening.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOpening.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockOpening.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == false && !(data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOpening.HasNotBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.batchNumber = null;
          });
      }
      if (data.product.hasDateValidity == false && !(data.expiryDate == null || data.expiryDate == ''
        || this.global.formatDate(this.fixed.today) < this.global.formatDate(data.expiryDate))) {
        this.translate.get('StockOpening.HasNotExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.expiryDate = null;
          });
        return;
      }
      if (data.qtyInUom < 1) {
        this.translate.get('StockOpening.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockOpening.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomPrice <= 0 || data.uomPrice == null) {
        this.translate.get('StockTransferDocument.EnterValidUomPrice')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      data.expiryDate = this.global.formatDate(data.expiryDate);
      const prev = this.model.stO_StockOpeningItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        if (prev[0].isDeleted === true) {
          prev[0].qtyInUom = data.qtyInUom;
          prev[0].uomPrice = data.uomPrice;
          prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
          prev[0].totalPrice = Math.round(prev[0].totalPrice * 100) / 100;
          prev[0].isDeleted = null;
          prev[0].isEdited = true;
        } else {
          prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
          prev[0].uomPrice = Math.round(prev[0].uomPrice * 100) / 100;
          prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
          prev[0].totalPrice = Math.round(prev[0].totalPrice * 100) / 100;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    const len = this.model.stO_StockOpeningItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  itemDelete(item, index) {
    if (item.stockOpeningItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockOpeningItem.splice(index, 1);
    }
    this.calTotal();
    const len = this.model.stO_StockOpeningItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  selectProduct(event, item: StockOpeningItem) {
    if (event != null) {
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      this.generateUnitList(item);
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
      }
    } else {
      item.product = null;
      item.barcode = null;
      item.productId = null;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      item.units = null;
    }
  }

  generateUnitList(item) {
    item.units = [];
    if (item.product.smallUomNameId != null) {
      const unit = {
        unitId: item.product.smallUomNameId,
        name: item.product.smallUomName,
        barcode: item.product.smallUomBarCode,
        size: 's'
      };
      item.units.push(unit);
    }
    if (item.product.mediumUomNameId != null) {
      const unit = {
        unitId: item.product.mediumUomNameId,
        name: item.product.mediumUomName,
        barcode: item.product.mediumUomBarCode,
        size: 'm'
      };
      item.units.push(unit);
    }
    if (item.product.largeUomNameId != null) {
      const unit = {
        unitId: item.product.largeUomNameId,
        name: item.product.largeUomName,
        barcode: item.product.largeUomBarCode,
        size: 'l'
      };
      item.units.push(unit);
    }
  }

  unitChange(item) {
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
  }

  saveStockOpening() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_StockOpeningItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.calTotal();
    this.stockOpeningSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  calTotal() {
    this.model.total = 0;
    const filter = this.model.stO_StockOpeningItem.filter(s => s.isDeleted == false || s.isDeleted == null);
    if (filter.length !== 0) {
      filter.forEach(item => {
        this.model.total = this.model.total + (item.uomPrice * item.qtyInUom);
      });
    }
  }

  voidedTransaction(id) {
    this.stockOpeningSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  PrintReport() {
    this.stockOpeningSer.PrintReport(this.model.stockOpeningId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
