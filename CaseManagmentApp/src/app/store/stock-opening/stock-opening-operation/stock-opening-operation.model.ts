export class StockOpeningModel {
    constructor(
        public stockOpeningId: number = null,
        public transactionCaseId: number = null,
        public transactionNo: string = null,
        public stockId: number = null,
        public date: any = null,
        public isPrinted: boolean = false,
        public total: number = null,
        public isVoided: boolean = false,
        public stO_StockOpeningItem: StockOpeningItem[] = []) { }
}



export class StockOpeningItem {
    constructor(
        public stockOpeningItemId: number = null,
        public stockOpeningId: number = null,
        public itemId: number = null,
        public productId: number = null,
        public barcode: string = null,
        public uomBarCode: string = null,
        public batchNumber: string = null,
        public expiryDate: any = null,
        public qtyInUom: number = 1,
        public uomPrice: number = 0,
        public totalPrice: number = 0,
        public product: any = null,
        public unitId: any = null,
        public units: any = null,
        public isEdited: boolean = false,
        public isDeleted: boolean = false,
        public operation: string = 'e') { }
}

