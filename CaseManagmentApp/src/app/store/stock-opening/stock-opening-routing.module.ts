import { StockOpeningOperationComponent } from './stock-opening-operation/stock-opening-operation.component';
import { StockOpeningListComponent } from './stock-opening-list/stock-opening-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockOpeningListComponent },
  { path: 'operation', component: StockOpeningOperationComponent },
  { path: 'operation/:id', component: StockOpeningOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockOpeningRoutingModule { }
