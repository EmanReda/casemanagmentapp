import { ProductModule } from './../product/product.module';
import { SharedModule } from './../../shared/shared.module';
import { StockOpeningOperationComponent } from './stock-opening-operation/stock-opening-operation.component';
import { StockOpeningListComponent } from './stock-opening-list/stock-opening-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockOpeningRoutingModule } from './stock-opening-routing.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockOpeningListComponent,
    StockOpeningOperationComponent
  ],
  imports: [
    CommonModule,
    StockOpeningRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockOpeningModule { }
