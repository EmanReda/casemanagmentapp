import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockOpeningService {

  constructor(private http: HttpClient) { }

  getTransactionNo(date): Observable<any> {
    return this.http.get('StockOpening/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`StockOpening/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`StockOpening/Operation`, model);
  }

  voided(id): Observable<any> {
    return this.http.delete(`StockOpening/Voided/${id}`);
  }

  getAll(model): Observable<any> {
    return this.http.post(`StockOpening/GetAll`, model);
  }
   PrintReport(id): any {
    return this.http.get('StockOpening/Report/' + id);
  }
}
