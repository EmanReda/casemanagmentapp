import { Component, OnInit } from '@angular/core';
import { FilterStockOutDocumentList } from './stock-out-document-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockOutDocumentService } from '../stock-out-document.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-out-document-list',
  templateUrl: './stock-out-document-list.component.html'
})

export class StockOutDocumentListComponent {
  searchModel: FilterStockOutDocumentList;
  total: number;
  stocks: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public stockInSer: StockOutDocumentService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockOutDocument.List', url: '/store/stock-out-document/list' }
      ],
      pageHeader: 'StockOutDocument.List'
    };
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterStockOutDocumentList();
    this.getAll();
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockInSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.stocks = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  resetSearch() {
    this.searchModel = new FilterStockOutDocumentList();
    this.total = undefined;
    this.stocks = undefined;
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
