export class FilterStockOutDocumentList{
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public fromDate:any=null,
        public toDate:any=null,
        public stockId:number=null,
        public transactionNo:string=null,
        public departmentModule:number=null,
        public departmentInvoiceNumber:string=null,
        public isVoided: boolean = null
    ){}
    }