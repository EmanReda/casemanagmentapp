import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StockOutDocumentService } from '../stock-out-document.service';
import { StockOutDocumentModel, StockOutDocumentItem } from './stock-out-document-operation.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ApiService } from '../../../shared/services/api.service';

@Component({
  selector: 'app-stock-out-document-operation',
  templateUrl: './stock-out-document-operation.component.html'
})

export class StockOutDocumentOperationComponent implements OnInit {
  model: StockOutDocumentModel;
  parms: any;
  lockGridEdit = false;
  stockItem = new StockOutDocumentItem();
  @ViewChild('operationError') operationError: SwalComponent;
  emptyList = false;
  checkDate = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockOutDocSer: StockOutDocumentService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockOutDocument.List', url: '/store/stock-out-document/list' }
      ],
      pageHeader: 'StockOutDocument.StockOutDocument'
    };
    this.model = new StockOutDocumentModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockOutDocument.Add', url: '/store/stock-out-document/operation' });
          this.fixed.subHeader.pageHeader = 'StockOutDocument.Add';
          this.model.stockOutDocumentId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'StockInDocument.Edit', url: '/store/stock-out-document/operation/' + id });
          this.fixed.subHeader.pageHeader = 'StockOutDocument.Edit';
          this.model.stockOutDocumentId = Number(id);
          this.getStockOutDocument(Number(id));
        }
      });
  }

  getStockOutDocument(id: number) {
    this.stockOutDocSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.model.stO_StockOutDocumentItem.forEach(item => {
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.generateUnitList(item);
            item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
            this.loadBatchExpireList(item, 'getById');
          });
        } else {
          this.closePage();
        }
      }, () => {
        this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate(['store/stock-out-document/list']);
    }, 5000);
  }

  getTransactionNo(data) {
    if (data != null && this.model.stockOutDocumentId == null) {
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
          if (this.checkDate == false) {
            this.model.transactionNo = null;
          } else {
            this.stockOutDocSer.getTransactionNo(this.global.formatDate(data))
              .subscribe((res) => {
                this.model.transactionNo = res;
                this.checkDate = true;
              }, () => {
                this.global.notificationMessage(4);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: StockOutDocumentItem) {
    if (!this.lockGridEdit && this.model.stockId != null) {
      this.lockGridEdit = true;
      if (this.model.stO_StockOutDocumentItem == null) {
        this.model.stO_StockOutDocumentItem = [];
      }
      if (data == null) {
        this.model.stO_StockOutDocumentItem.push(new StockOutDocumentItem(null, null, this.model.stO_StockOutDocumentItem.length + 1));
      } else {
        this.stockItem = new StockOutDocumentItem(data.stockOutDocumentItemId, data.stockOutDocumentId, data.itemId,
          data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.product, data.unitId, data.units, data.batchExpireList,
          data.batchExpire, data.qtyInStock, data.stockId, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else if (this.lockGridEdit) {
      this.translate.get('StockOutDocument.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    } else if (this.model.stockId == null) {
      this.translate.get('StockOutDocument.SelectStock')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockItem;
      this.lockGridEdit = false;
      (data.stockOutDocumentItemId == null) ? this.model.stO_StockOutDocumentItem.splice(index, 1) :
        this.model.stO_StockOutDocumentItem[index] = this.stockItem;
    } else {
      if (this.model.stockId == null) {
        this.translate.get('StockOutDocument.SelectStock')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product == null) {
        this.translate.get('StockOutDocument.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOutDocument.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockOutDocument.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == false && !(data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOutDocument.HasNotBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.batchNumber = null;
          });
      }
      if (data.product.hasDateValidity == false && !(data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockOutDocument.HasNotExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.expiryDate = null;
          });
      }
      if (data.qtyInUom < 1) {
        this.translate.get('StockOutDocument.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom > data.maxQty) {
        this.translate.get('StockOutDocument.QuantityGreaterThanMax')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val + '' + data.maxQty);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockOutDocument.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomPrice <= 0 || data.uomPrice == null) {
        this.translate.get('StockTransferDocument.EnterValidUomPrice')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_StockOutDocumentItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        if (prev[0].isDeleted === true) {
          prev[0].qtyInUom = data.qtyInUom;
          prev[0].uomPrice = data.uomPrice;
          prev[0].isDeleted = null;
          prev[0].isEdited = true;
        } else {
          prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
          prev[0].uomPrice = Math.round(prev[0].uomPrice * 100) / 100;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    const len = this.model.stO_StockOutDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  itemDelete(item, index) {
    if (item.stockOutDocumentItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockOutDocumentItem.splice(index, 1);
    }
    this.calcTotal();
    const len = this.model.stO_StockOutDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;

  }

  selectProduct(event, item: StockOutDocumentItem, from?) {
    if (event != null) {
      item.units = [];
      item.batchExpireList = [];
      if (item.operation === 'e') {
        item.unitId = null;
        item.uomBarCode = null;
        item.batchExpire = null;
        item.batchNumber = null;
        item.expiryDate = null;
        item.qtyInUom = 1;
        item.uomPrice = 0.0;
        item.totalPrice = 0.0;
      }
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      if (event.patch != null) {
        item.batchExpire = event.patch;
        item.expiryDate = event.patch.expiryDate;
        item.batchNumber = event.patch.batchNumber;
      } else if (from === 'shared' && event.patch == null) {
        item.expiryDate = null;
        item.batchNumber = null;
      }
      this.generateUnitList(item);
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          this.loadBatchExpireList(item);
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          this.loadBatchExpireList(item);
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          this.loadBatchExpireList(item);
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
        this.unitChange(item);
      }
    }
  }

  generateUnitList(item) {
    item.units = [];
    if (item.product.smallUomNameId != null) {
      const unit = {
        unitId: item.product.smallUomNameId,
        name: item.product.smallUomName,
        barcode: item.product.smallUomBarCode,
        size: 's'
      };
      item.units.push(unit);
    }
    if (item.product.mediumUomNameId != null) {
      const unit = {
        unitId: item.product.mediumUomNameId,
        name: item.product.mediumUomName,
        barcode: item.product.mediumUomBarCode,
        size: 'm'
      };
      item.units.push(unit);
    }
    if (item.product.largeUomNameId != null) {
      const unit = {
        unitId: item.product.largeUomNameId,
        name: item.product.largeUomName,
        barcode: item.product.largeUomBarCode,
        size: 'l'
      };
      item.units.push(unit);
    }
  }

  unitChange(item) {
    item.batchExpire = null;
    item.uomBarCode = null;
    item.qtyInUom = 1;
    item.uomPrice = 0;
    item.totalPrice = 0;
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
    item.expiryDate = null;
    item.batchNumber = null;
    this.loadBatchExpireList(item);
  }


  loadBatchExpireList(item, from?) {
    if (item.productId != null && this.model.stockId != null && item.uomBarCode != null) {
      this.stockOutDocSer.loadBatchExpireList(item.productId, this.model.stockId, item.uomBarCode)
        .subscribe((response) => {
          if (response != null && response.length > 0) {
            response.forEach(element => {
              element.expiryDate = (item.expiryDate != null) ? element.expiryDate.split('T')[0] : element.expiryDate;
            });
            item.batchExpireList = response;
            if (response.length === 1) {
              item.batchExpire = response[0];
              this.batchExpireChange(item);
            }
            if (from === 'getById') {
              item.batchExpire = response
                .filter(s => s.expiryDate == item.expiryDate && s.batchNumber == item.batchNumber)[0];
            }
          } else {
            this.translate.get('Invoice.NotFoundQnty')
              .subscribe((val) => {
                this.global.notificationMessage(3, null, val);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }

  }

  batchExpireChange(item) {
    item.expiryDate = item.batchExpire.expiryDate;
    item.batchNumber = item.batchExpire.batchNumber;
    item.maxQty = item.batchExpire.qtyInUom;
    item.uomPrice = item.batchExpire.priceCost;
    item.totalPrice = item.uomPrice * item.qtyInUom;
  }

  saveStockOutDocument() {
    this.stockOutDocSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-out-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  calcTotal() {
    this.model.total = 0;
    this.model.stO_StockOutDocumentItem.forEach(item => {
      this.model.total = this.model.total + (item.uomPrice * item.qtyInUom);
    });
  }

  voidedTransaction(id) {
    this.stockOutDocSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-out-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  PrintReport() {
    this.stockOutDocSer.PrintReport(this.model.stockOutDocumentId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}

