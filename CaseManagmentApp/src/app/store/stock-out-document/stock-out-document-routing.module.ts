import { StockOutDocumentOperationComponent } from './stock-out-document-operation/stock-out-document-operation.component';
import { StockOutDocumentListComponent } from './stock-out-document-list/stock-out-document-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockOutDocumentListComponent },
  { path: 'operation', component: StockOutDocumentOperationComponent },
  { path: 'operation/:id', component: StockOutDocumentOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockOutDocumentRoutingModule { }
