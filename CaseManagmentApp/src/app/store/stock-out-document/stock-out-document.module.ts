import { SharedModule } from './../../shared/shared.module';
import { StockOutDocumentOperationComponent } from './stock-out-document-operation/stock-out-document-operation.component';
import { StockOutDocumentListComponent } from './stock-out-document-list/stock-out-document-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockOutDocumentRoutingModule } from './stock-out-document-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockOutDocumentListComponent,
    StockOutDocumentOperationComponent,
  ],
  imports: [
    CommonModule,
    StockOutDocumentRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockOutDocumentModule { }
