import { Component } from '@angular/core';
import { FilterStockReceiveVoucher } from './stock-receive-voucher-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockReceiveVoucherService } from '../stock-receive-voucher.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-receive-voucher-list',
  templateUrl: './stock-receive-voucher-list.component.html'
})
export class StockReceiveVoucherListComponent {
  searchModel: FilterStockReceiveVoucher;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private stockReceiveVoucherSer: StockReceiveVoucherService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockReceiveVoucher.List', url: '/store/stock-receive-voucher/list' }
      ],
      pageHeader: 'StockReceiveVoucher.List'
    };
    this.searchModel = new FilterStockReceiveVoucher();
    this.getAll();
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockReceiveVoucherSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterStockReceiveVoucher();
    this.total = undefined;
    this.dataList = undefined;
  }
}
