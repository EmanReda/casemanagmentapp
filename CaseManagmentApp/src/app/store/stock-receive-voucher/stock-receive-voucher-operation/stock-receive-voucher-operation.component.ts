import { Component, OnInit, ViewChild } from '@angular/core';
import { StockReceiveVoucherModel, StockReceiveVoucherItem } from './stock-receive-voucher-operation.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { StockReceiveVoucherService } from '../stock-receive-voucher.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-stock-receive-voucher-operation',
  templateUrl: './stock-receive-voucher-operation.component.html'
})

export class StockReceiveVoucherOperationComponent implements OnInit {
  model: StockReceiveVoucherModel;
  parms: any;
  lockGridEdit = false;
  stockReceiveVoucherItem = new StockReceiveVoucherItem();
  //stockItem = new stockReceiveVoucherItem();
  @ViewChild('operationError') operationError: SwalComponent;
  listPage = '/store/stock-receive-voucher/list';
  emptyList = false;
  checkDate = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockReceiveVoucherSer: StockReceiveVoucherService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockReceiveVoucher.List', url: this.listPage }
      ],
      pageHeader: 'StockReceiveVoucher.Add'
    };
    this.model = new StockReceiveVoucherModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockReceiveVoucher.Add', url: '/store/stock-opening/operation' });
          this.fixed.subHeader.pageHeader = 'StockReceiveVoucher.Add';
          this.model.stockReceiveVoucherId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'StockReceiveVoucher.Edit', url: '/store/stock-opening/operation/' + id });
          this.fixed.subHeader.pageHeader = 'StockReceiveVoucher.Edit';
          this.model.stockReceiveVoucherId = Number(id);
          this.getStockReceiveVoucher(Number(id));
        }
      });
  }

  getTransactionNo(data) {
    if (data != null && this.model.stockReceiveVoucherId == null) {
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
          if (this.checkDate === false) {
            this.model.transactionNo = null;
          } else {
            this.stockReceiveVoucherSer.getTransactionNo(this.global.formatDate(data))
              .subscribe((respo) => {
                this.model.transactionNo = respo;
                this.checkDate = true;
              }, () => {
                this.global.notificationMessage(4);
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  getStockReceiveVoucher(id: number) {
    this.stockReceiveVoucherSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.model.stO_StockReceiveVoucherItem.forEach(item => {
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.generateUnitList(item);
            item.unitId = item.units.filter(s => s.barcode == item.uomBarCode)[0].unitId;
            item.uomDiscountPercentage = (item.uomPrice === 0) ? 0 :
              Math.round(((item.uomDiscountValue * 100) / item.uomPrice) * 100) / 100;
          });
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate([this.listPage]);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  itemOperation(data?: StockReceiveVoucherItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_StockReceiveVoucherItem == null) {
        this.model.stO_StockReceiveVoucherItem = [];
      }
      if (data == null) {
        this.model.stO_StockReceiveVoucherItem
          .push(new StockReceiveVoucherItem(null, null, this.model.stO_StockReceiveVoucherItem.length + 1));
      } else {
        this.stockReceiveVoucherItem = new StockReceiveVoucherItem(data.stockReceiveVoucherItemId, data.stockReceiveVoucherId,
          data.itemId, data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.uomDiscountPercentage, data.uomDiscountValue,
          data.totalAfterDiscount, data.vatPercentage, data.totalVatValue, data.stockItemCost, data.purchaseItemCost,
          data.totalDiscount, data.product, data.unitId, data.units, data.qtyInStock, data.restQtyInInvoice, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('StockOpening.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockReceiveVoucherItem;
      this.lockGridEdit = false;
      (data.stockReceiveVoucherItemId == null) ? this.model.stO_StockReceiveVoucherItem.splice(index, 1) :
        this.model.stO_StockReceiveVoucherItem[index] = this.stockReceiveVoucherItem;
    } else {
      if (data.product == null) {
        this.translate.get('StockOpening.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOpening.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockOpening.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == false && !(data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockOpening.HasNotBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.batchNumber = null;
          });
      }
      if (data.product.hasDateValidity == false && !(data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockOpening.HasNotExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.expiryDate = null;
          });
      }
      if (data.qtyInUom < 1) {
        this.translate.get('StockOpening.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockOpening.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_StockReceiveVoucherItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        if (prev[0].isDeleted === true) {
          prev[0].qtyInUom = data.qtyInUom;
          prev[0].uomPrice = data.uomPrice;
          prev[0].isDeleted = null;
          prev[0].isEdited = true;
        } else {
          prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
          prev[0].uomPrice = Math.round(prev[0].uomPrice * 100) / 100;
        }
        this.calculatePrice(prev[0]);
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    const len = this.model.stO_StockReceiveVoucherItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;

  }


  itemDelete(item, index) {
    if (item.stockReceiveVoucherItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockReceiveVoucherItem.splice(index, 1);

    }
    this.calTotal();
    const len = this.model.stO_StockReceiveVoucherItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  selectProduct(event, item: StockReceiveVoucherItem) {
    if (event != null) {
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      item.vatPercentage = event.element.vat;
      item.totalVatValue = (item.totalAfterDiscount * item.vatPercentage) / 100;
      item.totalVatValue = Math.round(item.totalVatValue * 100) / 100;
      this.calTotal();
      this.generateUnitList(item);
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
      }
    } else {
      item.product = null;
      item.barcode = null;
      item.productId = null;
      item.uomBarCode = null;
      item.unitId = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.totalPrice = 0.0;
      item.uomPrice = 0.0;
      item.units = null;
    }
  }

  generateUnitList(item) {
    item.units = [];
    if (item.product.smallUomNameId != null) {
      const unit = {
        unitId: item.product.smallUomNameId,
        name: item.product.smallUomName,
        barcode: item.product.smallUomBarCode,
        size: 's'
      };
      item.units.push(unit);
    }
    if (item.product.mediumUomNameId != null) {
      const unit = {
        unitId: item.product.mediumUomNameId,
        name: item.product.mediumUomName,
        barcode: item.product.mediumUomBarCode,
        size: 'm'
      };
      item.units.push(unit);
    }
    if (item.product.largeUomNameId != null) {
      const unit = {
        unitId: item.product.largeUomNameId,
        name: item.product.largeUomName,
        barcode: item.product.largeUomBarCode,
        size: 'l'
      };
      item.units.push(unit);
    }
  }

  unitChange(item) {
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
  }

  calculatePrice(item: StockReceiveVoucherItem) {
    item.totalAfterDiscount = (item.uomPrice - item.uomDiscountValue) * item.qtyInUom;
    item.totalPrice = item.uomPrice * item.qtyInUom;
    item.totalPrice = Math.round(item.totalPrice * 100) / 100;
    item.totalVatValue = (item.totalAfterDiscount * item.vatPercentage) / 100;
    item.totalVatValue = Math.round(item.totalVatValue * 100) / 100;
    this.calTotal();
  }

  calTotal() {
    this.model.totalAfterDiscount = 0;
    this.model.totalVAT = 0;
    const list = this.model.stO_StockReceiveVoucherItem
      .filter(s => s.isDeleted == false || s.isDeleted == null);
    if (list.length !== 0) {
      list.forEach(element => {
        this.model.totalAfterDiscount += element.totalAfterDiscount;
        this.model.totalVAT += element.totalVatValue;
      });
    }
    this.model.totalAfterDiscount = Math.round(this.model.totalAfterDiscount * 100) / 100;
    this.model.totalVAT = Math.round(this.model.totalVAT * 100) / 100;
    this.model.total = this.model.totalAfterDiscount + this.model.totalVAT - this.model.invoiceDiscountValue;
    this.model.total = Math.round(this.model.total * 100) / 100;
    this.calTotalDiscountForElement();
  }

  calTotalDiscountForElement() {
    if (this.model.totalAfterDiscount != 0) {
      this.model.stO_StockReceiveVoucherItem.forEach(element => {
        element.totalDiscount = (element.qtyInUom * element.uomDiscountValue) +
          (element.totalAfterDiscount * this.model.invoiceDiscountValue / this.model.totalAfterDiscount);
        element.purchaseItemCost = element.uomPrice - (element.totalDiscount / element.qtyInUom);
        element.purchaseItemCost = Math.round(element.purchaseItemCost * 100) / 100;
        element.totalDiscount = Math.round(element.totalDiscount * 100) / 100;
        if (element.stockReceiveVoucherItemId != null) {
          element.isEdited = true;
        }
      });
    }
  }

  calDiscount(item) {
    item.uomDiscountPercentage = (item.uomPrice === 0) ? 0 :
      Math.round(((item.uomDiscountValue * 100) / item.uomPrice) * 100) / 100;
    this.calculatePrice(item);
  }

  calDiscountPer(item) {
    item.uomDiscountValue = (item.uomDiscountPercentage * item.uomPrice) / 100;
    this.calculatePrice(item);
  }

  calVatPer(item) {
    item.vatPercentage = (item.totalAfterDiscount === 0) ? 0 :
      Math.round((100 * item.totalVatValue) / item.totalAfterDiscount);
    this.calTotal();
  }

  checkVat() {
    this.model.stO_StockReceiveVoucherItem.forEach(element => {
      if (element.product != null) {
        element.vatPercentage = element.product.vat;
        element.totalVatValue = (element.totalAfterDiscount * element.vatPercentage) / 100;
      }
    });
  }

  saveStockReceiveVoucher() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_StockReceiveVoucherItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.stockReceiveVoucherSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  voidedTransaction(id) {
    this.stockReceiveVoucherSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  PrintReceipt() {
    this.stockReceiveVoucherSer.PrintReceipt(this.model.stockReceiveVoucherId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
