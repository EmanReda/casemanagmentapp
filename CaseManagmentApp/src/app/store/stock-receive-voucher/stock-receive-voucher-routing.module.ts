import { StockReceiveVoucherOperationComponent } from './stock-receive-voucher-operation/stock-receive-voucher-operation.component';
import { StockReceiveVoucherListComponent } from './stock-receive-voucher-list/stock-receive-voucher-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockReceiveVoucherListComponent },
  { path: 'operation', component: StockReceiveVoucherOperationComponent },
  { path: 'operation/:id', component: StockReceiveVoucherOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockReceiveVoucherRoutingModule { }
