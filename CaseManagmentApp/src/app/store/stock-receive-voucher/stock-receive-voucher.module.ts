import { SharedModule } from './../../shared/shared.module';
import { StockReceiveVoucherOperationComponent } from './stock-receive-voucher-operation/stock-receive-voucher-operation.component';
import { StockReceiveVoucherListComponent } from './stock-receive-voucher-list/stock-receive-voucher-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockReceiveVoucherRoutingModule } from './stock-receive-voucher-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockReceiveVoucherListComponent,
    StockReceiveVoucherOperationComponent,

  ],
  imports: [
    CommonModule,
    StockReceiveVoucherRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockReceiveVoucherModule { }
