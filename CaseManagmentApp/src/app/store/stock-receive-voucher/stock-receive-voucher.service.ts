import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockReceiveVoucherService {

  constructor(private http: HttpClient) { }

  getTransactionNo(date): Observable<any> {
    return this.http.get('StockReceiveVoucher/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`StockReceiveVoucher/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`StockReceiveVoucher/Operation`, model);
  }

  voided(id): Observable<any> {
    return this.http.delete(`StockReceiveVoucher/Voided/${id}`);
  }

  getAll(model): Observable<any> {
    return this.http.post(`StockReceiveVoucher/GetAll`, model);
  }

  PrintReceipt(id): any {
    return this.http.get('StockReceiveVoucher/ReceiptReport/' + id);
  }
}
