import { Component } from '@angular/core';
import { FilterStockReturnVoucher } from './stock-return-voucher-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockReturnVoucherService } from '../stock-return-voucher.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-return-voucher-list',
  templateUrl: './stock-return-voucher-list.component.html'
})
export class StockReturnVoucherListComponent {
  searchModel: FilterStockReturnVoucher;
  total: any;
  dataList: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private stockReturnVoucherSer: StockReturnVoucherService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockReturnVoucher.List', url: '/store/stock-return-voucher/list' }
      ],
      pageHeader: 'StockReturnVoucher.List'
    };
    this.searchModel = new FilterStockReturnVoucher();
    this.getAll();
    this.global.appLangChanged.subscribe(
      (data) => {
        this.localeService.use(this.fixed.activeLang.code);
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockReturnVoucherSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.dataList = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  resetSearch() {
    this.searchModel = new FilterStockReturnVoucher();
    this.total = undefined;
    this.dataList = undefined;
  }
}
