import { Component, OnInit, ViewChild } from '@angular/core';
import { StockReturnVoucherModel, StockReturnVoucherItem } from './stock-return-voucher-operation.model';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { StockReturnVoucherService } from '../stock-return-voucher.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { ApiService } from '../../../shared/services/api.service';
import {
  StockReceiveVoucherModel
} from '../../stock-receive-voucher/stock-receive-voucher-operation/stock-receive-voucher-operation.model';

@Component({
  selector: 'app-stock-return-voucher-operation',
  templateUrl: './stock-return-voucher-operation.component.html'
})

export class StockReturnVoucherOperationComponent implements OnInit {
  //beforeEditReturnQty: any = 0;
  model: StockReturnVoucherModel;
  invoicemodel: StockReceiveVoucherModel;
  productList: any[];
  distinctProducts: any[];
  unitBarCodeList: any[];
  parms: any;
  trans: any;
  lockGridEdit = false;
  stockReturnVoucherItem = new StockReturnVoucherItem();
  @ViewChild('operationError') operationError: SwalComponent;
  listPage = '/store/stock-return-voucher/list';
  emptyList = false;
  checkDate = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public apiSer: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockReturnVoucherSer: StockReturnVoucherService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockReturnVoucher.List', url: this.listPage }
      ],
      pageHeader: 'StockReturnVoucher.Add'
    };
    this.model = new StockReturnVoucherModel();
    this.invoicemodel = new StockReceiveVoucherModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockReturnVoucher.Add', url: '/store/stock-return/operation' });
          this.fixed.subHeader.pageHeader = 'StockReturnVoucher.Add';
          this.model.stockReturnVoucherId = null;
          this.itemOperation();
        } else {
          this.fixed.subHeader.data.push({ name: 'StockReturnVoucher.Edit', url: '/store/stock-return/operation/' + id });
          this.fixed.subHeader.pageHeader = 'StockReturnVoucher.Edit';
          this.model.stockReturnVoucherId = Number(id);
          this.getStockReturnVoucher(Number(id));
        }
      });
  }


  getTransactionNo(data) {
    if (data != null && this.model.stockReturnVoucherId == null) {
      this.stockReturnVoucherSer.getTransactionNo(this.global.formatDate(data))
        .subscribe((response) => {
          this.model.transactionNo = response;
        }, () => {
          this.global.notificationMessage(4);
        });
      this.apiSer.CheckTransactionDate(this.global.formatDate(data))
        .subscribe((response) => {
          this.checkDate = response;
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  getStockReturnVoucher(id: number) {
    this.stockReturnVoucherSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.invoicemodel = Response.invoiceTransaction;
          this.invoiceOperation(this.invoicemodel);
          this.model.stO_StockReturnVoucherItem.forEach(item => {
            item.beforeEditReturnQty = item.qtyInUom;
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            this.productChanges(item, 'ts');
            this.unitChanges(item, 'ts');
            item.batchExpire = {
              batchExpire: {
                batchNumber: item.batchNumber,
                expiryDate: item.expiryDate
              },
              batchNumber: item.batchNumber,
              expiryDate: item.expiryDate
            };
            this.batchExpireChange(item);
          });
          this.checkDate = true;
        } else {
          this.closePage();
        }
      }, () => {
        this.closePage();
      });
  }

  closePage() {
    this.operationError.show();
    setTimeout(() => {
      const m = this.operationError.close;
      this.router.navigate([this.listPage]);
    }, 5000);
  }

  
  getInvoiceData(transactionNumber: string) {
    this.trans =transactionNumber;
    this.stockReturnVoucherSer.getInvoiceData(transactionNumber)
      .subscribe((Response) => {
        if (Response != null&&Response.transactionNo == this.trans) {
          if (this.global.formatDate(Response.date) > this.global.formatDate(this.model.date)) {
            this.model.date = null;
          }
          this.invoicemodel = Response;
          
          this.invoiceOperation(this.invoicemodel);
        } else {
          this.invoicemodel.transactionNo = '';
            this.invoicemodel.vendorName= '';
            this.invoicemodel.stockName= '';
          this.translate.get('StockReturnVoucher.UnfoundInvoice')
            .subscribe((val) => {
              this.global.notificationMessage(3, null, val);
            });
        }   
      }, () => {
        this.global.notificationMessage(4);
      });

  }
  onTextChange(value)
  {
    this.model.invoiceTransactionNo = value;
    if(this.model.invoiceTransactionNo == '')
    {
      this.invoicemodel.vendorName= '';
      this.invoicemodel.stockName= '';
    } 
  }

  invoiceOperation(Response: any) {
    Response.date = new Date(Response.date);
    this.model.invoiceTransactionNo = Response.transactionNo;
    this.model.stockId = Response.stockId;
    this.model.vendorId = Response.vendorId;
    this.model.stockReceiveVoucherId = Response.stockReceiveVoucherId;
    this.distinctProducts = [];
    Response.stO_StockReceiveVoucherItem.forEach(element => {
      const exist = this.distinctProducts.filter(s => s.productId == element.productId);
      if (exist.length == 0) {
        this.distinctProducts.push(element.product);
      }
    });
    this.productList = Response.stO_StockReceiveVoucherItem;
  }

  productChanges(item, from?) {
    item.selectedProductData = [];
    item.units = [];
    item.batchExpireList = [];
    if (from == null) {
      item.uomBarCode = null;
      item.uomDiscountValue = 0;
    }
    item.productId = item.product.productId;
    item.selectedProductData.push(this.productList.filter(s => s.productId === item.product.productId));
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.uomBarCode === ele.product.smallUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.smallUomBarCode,
            uomBarCodeName: ele.product.smallUomName
          });
        } else if (ele.uomBarCode === ele.product.mediumUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.mediumUomBarCode,
            uomBarCodeName: ele.product.mediumUomName
          });
        } else if (ele.uomBarCode === ele.product.largeUomBarCode) {
          item.units.push({
            uomBarCode: ele.product.largeUomBarCode,
            uomBarCodeName: ele.product.largeUomName
          });
        }
      });
      if (item.uomBarCode == null && item.units.length == 1) {
        item.uomBarCode = item.units[0].uomBarCode;
        this.unitChanges(item);
      }
    });
  }

  unitChanges(item, from?) {
    item.batchExpireList = [];
    if (from == null) {
      item.batchExpire = null;
      item.batchNumber = null;
      item.expiryDate = null;
      item.uomPrice = 0;
      item.uomDiscountValue = 0;
    }
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.uomBarCode == item.uomBarCode && ele.productId == item.product.productId) {
          item.batchExpireList.push({
            batchExpire: {
              batchNumber: ele.batchNumber,
              expiryDate: ele.expiryDate
            },
            batchNumber: ele.batchNumber,
            expiryDate: ele.expiryDate
          });
        }
      });
      if (item.batchExpire == null && item.batchExpireList.length == 1) {
        item.batchExpire = item.batchExpireList[0].batchExpire;
        item.batchNumber = item.batchExpire.batchNumber;
        item.expiryDate = this.global.formatDate(item.batchExpire.expiryDate);
        this.batchExpireChange(item);
      }
    });
  }

  batchExpireChange(item) {
    item.selectedProductData.forEach(element => {
      element.forEach(ele => {
        if (ele.productId == item.product.productId && ele.uomBarCode == item.uomBarCode
          && ele.expiryDate == item.batchExpire.expiryDate
          && ele.batchNumber == item.batchExpire.batchNumber) {
          item.uomPrice = ele.uomPrice;
          item.uomDiscountValue = ele.uomDiscountValue;
          item.oldTotalAfterDiscount = ele.totalAfterDiscount;
          item.oldTotalVatValue = ele.totalVatValue;
          item.oldQtyInUom = ele.qtyInUom;
          item.purchaseItemCost = ele.purchaseItemCost;
          item.qtyInStock = ele.qtyInStock;
          item.restQtyInInvoice = ele.restQtyInInvoice;
        }
      });
    });
  }

  checkValidQuantity(item): boolean {
    if ((item.qtyInUom <= (item.restQtyInInvoice+item.beforeEditReturnQty)) && (item.qtyInUom <= (item.qtyInStock+item.beforeEditReturnQty))) {
      item.totalPrice = item.qtyInUom * item.uomPrice;
      item.totalDiscount = item.qtyInUom * item.uomDiscountValue;
      item.totalAfterDiscount = item.totalPrice - item.totalDiscount;
      item.vatPercentage = item.oldTotalVatValue * (item.qtyInUom / item.oldQtyInUom);
      item.totalVatValue = (item.totalAfterDiscount * item.vatPercentage) / 100;
      item.totalVatValue = Math.round(item.totalVatValue * 100) / 100;
      return true;
    } else {
      this.translate.get('Invoice.NotFoundQnty')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      setTimeout(() => {
        item.qtyInUom = 0;
        item.totalPrice = 0;
        item.totalDiscount = 0;
        item.totalAfterDiscount = 0;
        item.totalVatValue = 0;
      }, 1000);
      return false;
    }
  }

  itemOperation(data?: StockReturnVoucherItem) {
    if (!this.lockGridEdit) {
      this.lockGridEdit = true;
      if (this.model.stO_StockReturnVoucherItem == null) {
        this.model.stO_StockReturnVoucherItem = [];
      }
      if (data == null) {
        this.model.stO_StockReturnVoucherItem
          .push(new StockReturnVoucherItem(null, null, this.model.stO_StockReturnVoucherItem.length + 1));
      } else {
        this.stockReturnVoucherItem = new StockReturnVoucherItem(data.stockReturnVoucherItemId, data.stockReturnVoucherId,
          data.itemId, data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.oldQtyInUom, data.uomPrice, data.totalPrice, data.uomDiscountPercentage, data.uomDiscountValue,
          data.totalAfterDiscount, data.oldAfterDiscount, data.vatPercentage, data.totalVatValue, data.oldTotalVatValue,
          data.stockItemCost, data.purchaseItemCost, data.totalDiscount, data.product, data.unitId, data.units,
          data.selectedProductData, data.batchExpireList, data.batchExpire, data.qtyInStock,
          data.restQtyInInvoice, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else {
      this.translate.get('StockReturnVoucher.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockReturnVoucherItem;
      this.lockGridEdit = false;
      (data.stockReturnVoucherItemId == null) ? this.model.stO_StockReturnVoucherItem.splice(index, 1) :
        this.model.stO_StockReturnVoucherItem[index] = this.stockReturnVoucherItem;
      const len = this.model.stO_StockReturnVoucherItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
      this.emptyList = (len === 0) ? true : false;
    } else {
      if (data.product == null) {
        this.translate.get('StockReturnVoucher.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.batchExpire == null || data.batchExpire == '') {
        this.translate.get('StockReturnVoucher.CompleteBatchNumberAndExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockReturnVoucher.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom == 0) {
        this.translate.get('StockReturnVoucher.EnterQtyInUom')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_StockReturnVoucherItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber && s.product.productId == data.product.productId);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
        if (!this.checkValidQuantity(prev[0])) {
          return;
        }
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
      this.calcValues();
    }
  }

  calcValues() {
    this.model.total = 0;
    this.model.totalVAT = 0;
    this.model.totalAfterDiscount = 0;
    this.model.stO_StockReturnVoucherItem.forEach(element => {
      this.model.total += element.totalPrice;
      this.model.totalVAT += Math.round(element.totalVatValue * 100) / 100;
      this.model.totalAfterDiscount += element.totalAfterDiscount;
      //this.model.total = this.model.totalAfterDiscount + this.model.totalVAT;
      
    });
  }

  itemDelete(item, index) {
    if (item.stockReturnVoucherItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockReturnVoucherItem.splice(index, 1);
    }
    const len = this.model.stO_StockReturnVoucherItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
  }

  saveStockReturnVoucher() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_StockReturnVoucherItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.stockReturnVoucherSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  voidedTransaction(id) {
    this.stockReturnVoucherSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate([this.listPage]);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  searchInProducts(event) {
    if (this.productList != null) {
      const barcodeBatchItem = this.productList.filter(s => s.barcodeBatchNumber == event.term)[0];
      const editedItem = this.model.stO_StockReturnVoucherItem.filter(s => s.operation === 'e')[0];
      if (event.items.length === 0 && barcodeBatchItem != null) {
        editedItem.product = barcodeBatchItem.product;
        this.productChanges(editedItem);
        editedItem.uomBarCode = barcodeBatchItem.uomBarCode;
        this.unitChanges(editedItem);
        editedItem.batchNumber = barcodeBatchItem.batchNumber;
        editedItem.expiryDate = barcodeBatchItem.expiryDate;
        editedItem.batchExpire = {
          batchExpire: {
            batchNumber: barcodeBatchItem.batchNumber,
            expiryDate: barcodeBatchItem.expiryDate
          },
          batchNumber: barcodeBatchItem.batchNumber,
          expiryDate: barcodeBatchItem.expiryDate
        };
        this.batchExpireChange(editedItem);
      } else if (event.items.length === 1 &&
        ((event.items[0].largeUomBarCode != null && event.items[0].largeUomBarCode == event.term) ||
          (event.items[0].mediumUomBarCode != null && event.items[0].mediumUomBarCode == event.term) ||
          (event.items[0].largeUomBarCode != null && event.items[0].largeUomBarCode == event.term))) {
        editedItem.product = event.items[0];
        this.productChanges(editedItem);
        editedItem.uomBarCode = event.term;
        this.unitChanges(editedItem);
      }
    } else {
      this.translate.get('StockReturnVoucher.PurchaseInvoiceNumber')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    }
  }

  customSearchFn(term: string, item?: any) {
    if (item.category != null && item.category.includes(term)) {
      return item;
    } else if (item.code != null && item.code.includes(term)) {
      return item;
    } else if (item.largeUomBarCode != null && item.largeUomBarCode.includes(term)) {
      return item;
    } else if (item.mediumUomBarCode != null && item.mediumUomBarCode.includes(term)) {
      return item;
    } else if (item.smallUomBarCode != null && item.smallUomBarCode.includes(term)) {
      return item;
    } else if (item.nameAr != null && item.nameAr.includes(term)) {
      return item;
    } else if (item.nameEn != null && item.nameEn.includes(term)) {
      return item;
    }
    return null;
  }

  PrintReceipt() {
    this.stockReturnVoucherSer.PrintReceipt(this.model.stockReturnVoucherId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
