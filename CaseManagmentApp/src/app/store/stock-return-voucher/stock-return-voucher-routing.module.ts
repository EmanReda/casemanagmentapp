import { StockReturnVoucherOperationComponent } from './stock-return-voucher-operation/stock-return-voucher-operation.component';
import { StockReturnVoucherListComponent } from './stock-return-voucher-list/stock-return-voucher-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockReturnVoucherListComponent },
  { path: 'operation', component: StockReturnVoucherOperationComponent },
  { path: 'operation/:id', component: StockReturnVoucherOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockReturnVoucherRoutingModule { }
