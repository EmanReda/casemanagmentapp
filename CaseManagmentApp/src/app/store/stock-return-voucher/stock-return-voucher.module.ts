import { SharedModule } from './../../shared/shared.module';
import { StockReturnVoucherOperationComponent } from './stock-return-voucher-operation/stock-return-voucher-operation.component';
import { StockReturnVoucherListComponent } from './stock-return-voucher-list/stock-return-voucher-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockReturnVoucherRoutingModule } from './stock-return-voucher-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockReturnVoucherListComponent,
    StockReturnVoucherOperationComponent,
  ],
  imports: [
    CommonModule,
    StockReturnVoucherRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockReturnVoucherModule { }
