import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockReturnVoucherService {

  constructor(private http: HttpClient) { }

  getTransactionNo(date): Observable<any> {
    return this.http.get('StockReturnVoucher/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`StockReturnVoucher/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`StockReturnVoucher/Operation`, model);
  }

  voided(id): Observable<any> {
    return this.http.delete(`StockReturnVoucher/Voided/${id}`);
  }

  getAll(model): Observable<any> {
    return this.http.post(`StockReturnVoucher/GetAll`, model);
  }
  getInvoiceData(transactionNumber: string): any {
    return this.http.get('StockReturnVoucher/GetReceiveVoucherData?transactionNumber=' + transactionNumber);
  }
  PrintReceipt(id): any {
    return this.http.get('StockReturnVoucher/ReturnReport/' + id);
  }
}
