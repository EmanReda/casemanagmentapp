import { Component } from '@angular/core';
import { FilterStockTransferDocumentList } from './stock-transfer-document-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockTransferDocumentService } from '../stock-transfer-document.service';
import { BsLocaleService } from 'ngx-bootstrap';

@Component({
  selector: 'app-stock-transfer-document-list',
  templateUrl: './stock-transfer-document-list.component.html'
})

export class StockTransferDocumentListComponent {
  searchModel: FilterStockTransferDocumentList;
  total: number;
  stocks: any;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public stockTransferSer: StockTransferDocumentService,
    private localeService: BsLocaleService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockTransferDocument.List', url: '/store/stock-transfer-document/list' }
      ],
      pageHeader: 'StockTransferDocument.List'
    };
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterStockTransferDocumentList();
    this.getAll();
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockTransferSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.stocks = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  resetSearch() {
    this.searchModel = new FilterStockTransferDocumentList();
    this.total = undefined;
    this.stocks = undefined;
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

}
