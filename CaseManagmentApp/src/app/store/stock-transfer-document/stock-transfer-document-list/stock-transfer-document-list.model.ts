export class FilterStockTransferDocumentList{
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public fromDate:any=null,
        public toDate:any=null,
        public fromStockId:number=null,
        public toStockId:number=null,
        public transactionNo:string=null,
        public invoiceNumber:string=null,
        public isVoided: boolean = null
    ){}
    }