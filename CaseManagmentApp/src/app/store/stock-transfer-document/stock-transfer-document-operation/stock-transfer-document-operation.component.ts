import { Component, OnInit, ViewChild } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StockTransferDocumentService } from '../stock-transfer-document.service';
import { StockTransferDocumentItem, StockTransferDocumentModel } from './stock-transfer-document-operation.model';
import { BsLocaleService } from 'ngx-bootstrap';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-stock-transfer-document-operation',
  templateUrl: './stock-transfer-document-operation.component.html'
})

export class StockTransferDocumentOperationComponent implements OnInit {
  model: StockTransferDocumentModel;
  parms: any;
  lockGridEdit = false;
  stockItem = new StockTransferDocumentItem();
  checkDate: boolean;
  @ViewChild('operationError') operationError: SwalComponent;
  emptyList = false;
  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private stockTransferDocSer: StockTransferDocumentService,
    private localeService: BsLocaleService) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'StockTransferDocument.List', url: '/store/stock-transfer-document/list' }
      ],
      pageHeader: 'StockTransferDocument.StockTransferDocumentStockTransferDocument'
    };
    this.model = new StockTransferDocumentModel();
  }

  ngOnInit() {
    this.parms = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'StockTransferDocument.Add', url: '/store/stock-transfer-document/operation' });
          this.fixed.subHeader.pageHeader = 'StockTransferDocument.Add';
          this.model.stockTransferDocumentId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'StockTransferDocument.Show', url: '/store/stock-transfer-document/operation/' + id });
          this.fixed.subHeader.pageHeader = 'StockTransferDocument.Show';
          this.model.stockTransferDocumentId = Number(id);
          this.getStockTransferDocument(Number(id));
        }
      });
  }

  getStockTransferDocument(id: number) {
    this.stockTransferDocSer.getById(id)
      .subscribe((Response) => {
        if (Response != null) {
          this.model = Response;
          this.model.stO_StockTransferDocumentItem.forEach(item => {
            item.expiryDate = (item.expiryDate != null) ? item.expiryDate.split('T')[0] : item.expiryDate;
            item.operation = 's';
            const event = { code: item.uomBarCode, element: item.product };
            this.selectProduct(event, item);
          });
        } else {
          this.operationError.show();
          setTimeout(() => {
            const m = this.operationError.close;
            this.router.navigate(['store/stock-transfer-document-list']);
          }, 5000);
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  getTransactionNo(data) {
    if (data != null && this.model.stockTransferDocumentId == null) {
      this.stockTransferDocSer.getTransactionNo(this.global.formatDate(data))
        .subscribe((response) => {
          if (response == null) {
            this.model.transactionNo = null;
            this.checkDate = false;
          } else {
            this.model.transactionNo = response;
            this.checkDate = true;
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }

  itemOperation(data?: StockTransferDocumentItem) {
    if (!this.lockGridEdit && this.model.fromStockId != null) {
      this.lockGridEdit = true;
      if (this.model.stO_StockTransferDocumentItem == null) {
        this.model.stO_StockTransferDocumentItem = [];
      }
      if (data == null) {
        this.model.stO_StockTransferDocumentItem.push(
          new StockTransferDocumentItem(null, null, this.model.stO_StockTransferDocumentItem.length + 1));
      } else {
        this.stockItem = new StockTransferDocumentItem(data.stockTransferDocumentItem, data.stockTransferDocumentId, data.itemId,
          data.productId, data.barcode, data.uomBarCode, data.batchNumber, data.expiryDate,
          data.qtyInUom, data.uomPrice, data.totalPrice, data.product, data.unitId, data.units, data.batchExpireList,
          data.batchExpire, data.qtyInStock, data.isEdited, false, 's');
        data.operation = 'e';
        data.isEdited = true;
      }
    } else if (this.lockGridEdit) {
      this.translate.get('StockTransferDocument.CannotModify')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
    } else if (this.model.fromStockId == null) {
      this.translate.get('StockTransferDocument.ChooseStock')
        .subscribe((val) => {
          this.global.notificationMessage(3, null, val);
        });
      return;
    }
  }

  itemFinish(type, data, index?) {
    if (type === 'close') {
      data = this.stockItem;
      this.lockGridEdit = false;
      (data.StockTransferDocumentItem == null) ? this.model.stO_StockTransferDocumentItem.splice(index, 1) :
        this.model.stO_StockTransferDocumentItem[index] = this.stockItem;
      const len = this.model.stO_StockTransferDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
      this.emptyList = (len === 0) ? true : false;
    } else {
      if (this.model.fromStockId == null) {
        this.translate.get('StockTransferDocument.ChooseStock')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product == null) {
        this.translate.get('StockTransferDocument.ChooseProduct')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == true && (data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockTransferDocument.CompleteBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasDateValidity == true && (data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockTransferDocument.CompleteExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.product.hasBatchNumber == false && !(data.batchNumber == null || data.batchNumber == '')) {
        this.translate.get('StockTransferDocument.HasNotBatchNumber')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.batchNumber = null;
          });
      }
      if (data.product.hasDateValidity == false && !(data.expiryDate == null || data.expiryDate == '')) {
        this.translate.get('StockTransferDocument.HasNotExpiryDate')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
            data.expiryDate = null;
          });
      }
      if (data.qtyInUom < 1) {
        this.translate.get('StockTransferDocument.Quantity')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.qtyInUom > data.maxQty) {
        this.translate.get('StockTransferDocument.QuantityGreaterThanMax')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val + '' + data.maxQty);
          });
        return;
      }
      if (data.uomBarCode == null) {
        this.translate.get('StockTransferDocument.UnitIdRequired')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      if (data.uomPrice <= 0 || data.uomPrice == null) {
        this.translate.get('StockTransferDocument.EnterValidUomPrice')
          .subscribe((val) => {
            this.global.notificationMessage(3, null, val);
          });
        return;
      }
      const prev = this.model.stO_StockTransferDocumentItem.filter(s => s.uomBarCode == data.uomBarCode
        && this.global.formatDate(s.expiryDate) == this.global.formatDate(data.expiryDate)
        && s.batchNumber == data.batchNumber);
      if (prev.length == 1) {
        this.lockGridEdit = false;
        data.operation = 's';
      } else {
        prev[0].qtyInUom = prev[0].qtyInUom + data.qtyInUom;
        prev[0].totalPrice = prev[0].qtyInUom * prev[0].uomPrice;
        this.itemDelete(data, index);
        this.lockGridEdit = false;
      }
    }
    this.calcTotal();
  }

  itemDelete(item, index) {
    if (item.StockOutDocumentItemId > 0) {
      item.isDeleted = true;
    } else {
      this.model.stO_StockTransferDocumentItem.splice(index, 1);
    }
    const len = this.model.stO_StockTransferDocumentItem.filter(s => s.isDeleted == false || s.isDeleted == null).length;
    this.emptyList = (len === 0) ? true : false;
    this.calcTotal();
  }

  selectProduct(event, item: StockTransferDocumentItem, from?) {
    if (event != null) {
      item.units = [];
      item.unitId = null;
      item.batchExpireList = [];
      if (item.operation == 'e') {
        item.uomBarCode = null;
        item.batchExpire = null;
        item.batchNumber = null;
        item.expiryDate = null;
        item.qtyInUom = 1;
        item.uomPrice = 0.0;
        item.totalPrice = 0.0;
      }
      item.product = event.element;
      item.barcode = event.element.code;
      item.productId = event.element.productId;
      if (event.patch != null) {
        item.batchExpire = event.patch;
        item.expiryDate = event.patch.expiryDate;
        item.batchNumber = event.patch.batchNumber;
      } else if (from === 'shared' && event.patch == null) {
        item.expiryDate = null;
        item.batchNumber = null;
      }
      if (item.product.smallUomNameId != null) {
        const unit = {
          unitId: item.product.smallUomNameId,
          name: item.product.smallUomName,
          barcode: item.product.smallUomBarCode,
          size: 's'
        };
        item.units.push(unit);
      }
      if (item.product.mediumUomNameId != null) {
        const unit = {
          unitId: item.product.mediumUomNameId,
          name: item.product.mediumUomName,
          barcode: item.product.mediumUomBarCode,
          size: 'm'
        };
        item.units.push(unit);
      }
      if (item.product.largeUomNameId != null) {
        const unit = {
          unitId: item.product.largeUomNameId,
          name: item.product.largeUomName,
          barcode: item.product.largeUomBarCode,
          size: 'l'
        };
        item.units.push(unit);
      }
      switch (event.code) {
        case (item.product.smallUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.smallUomNameId;
          this.loadBatchExpireList(item);
          break;
        case (item.product.mediumUomBarCode):
          item.uomBarCode = event.code;
          item.unitId = item.product.mediumUomNameId;
          this.loadBatchExpireList(item);
          break;
        case (item.product.largeUomName):
          item.uomBarCode = event.code;
          item.unitId = item.product.largeUomNameId;
          this.loadBatchExpireList(item);
          break;
      }
      if (item.uomBarCode == null && item.units.length == 1) {
        item.unitId = item.units[0].unitId;
        item.uomBarCode = item.units[0].barcode;
        this.unitChange(item);
      }
    }
  }

  unitChange(item) {
    item.batchExpire = null;
    item.uomBarCode = null;
    item.qtyInUom = 1;
    item.uomPrice = 0;
    item.totalPrice = 0;
    item.uomBarCode = item.units.filter(s => s.unitId == item.unitId)[0].barcode;
    item.expiryDate = null;
    item.batchNumber = null;
    this.loadBatchExpireList(item);
  }


  loadBatchExpireList(item) {
    if (item.productId != null && this.model.fromStockId != null && item.uomBarCode != null) {
      this.stockTransferDocSer.loadBatchExpireList(item.productId, this.model.fromStockId, item.uomBarCode)
        .subscribe((response) => {
          if (response != null && response.length > 0) {
            item.batchExpireList = response;
            if (response.length === 1) {
              item.batchExpire = response[0];
              this.batchExpireChange(item);
            }
          } else {
            this.translate.get('Invoice.NotFoundQnty')
              .subscribe((val) => {
                this.global.notificationMessage(3, null, val);
                item.maxQty = 0;
              });
          }
        }, () => {
          this.global.notificationMessage(4);
        });
    }

  }

  batchExpireChange(item) {
    item.expiryDate = item.batchExpire.expiryDate;
    item.batchNumber = item.batchExpire.batchNumber;
    item.maxQty = item.batchExpire.qtyInUom;
    item.uomPrice = item.batchExpire.priceCost;
    item.totalPrice = item.uomPrice * item.qtyInUom;
  }
  saveStockTransferDocument() {
    this.model.date = this.global.formatDate(this.model.date);
    this.model.stO_StockTransferDocumentItem.forEach(element => {
      element.expiryDate = this.global.formatDate(element.expiryDate);
    });
    this.stockTransferDocSer.save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-transfer-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  calcTotal() {
    this.model.total = 0;
    this.model.stO_StockTransferDocumentItem.forEach(item => {
      this.model.total = this.model.total + (item.uomPrice * item.qtyInUom);
    });
  }

  voidedTransaction(id) {
    this.stockTransferDocSer.voided(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.router.navigate(['/store/stock-transfer-document/list']);
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  PrintReport() {
    this.stockTransferDocSer.PrintReport(this.model.stockTransferDocumentId)
      .subscribe((response) => {
        this.global.openReport(response);
      }, () => {
        this.global.notificationMessage(4);
      });
  }

}

