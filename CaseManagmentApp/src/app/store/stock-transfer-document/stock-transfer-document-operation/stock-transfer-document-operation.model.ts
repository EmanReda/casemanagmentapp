export class StockTransferDocumentModel{
    constructor(
        public stockTransferDocumentId: number = null,
        public transactionCaseId: number = null,
        public transactionNo: string = null,
        public fromStockId: number = null,
        public toStockId: number = null,
        public invoiceNumber: string = null,
        public description: string = null,
        public date: any = null,
        public isPrinted: boolean = false,
        public total: number = null,
        public isVoided: boolean = false,
        public stO_StockTransferDocumentItem: StockTransferDocumentItem[] = []
    ){}
}

export class StockTransferDocumentItem{
   constructor(
    public stockTransferDocumentItem: number = null,
    public stockTransferDocumentId: number = null,
    public itemId: number = null,
    public productId: number = null,
    public barcode: string = null,
    public uomBarCode: string = null,
    public batchNumber: string = null,
    public expiryDate: any = null,
    public qtyInUom: number = 1,
    public uomPrice: number = 0,
    public totalPrice: number = 0,
    public product: any = null,
    public unitId: any = null,
    public units: any = null,
    public batchExpireList: any = null,
    public batchExpire: any = null,
    public qtyInStock: number = 0,
    public isEdited: boolean = false,
    public isDeleted: boolean = false,
    public operation: string = 'e'
  ){}
}