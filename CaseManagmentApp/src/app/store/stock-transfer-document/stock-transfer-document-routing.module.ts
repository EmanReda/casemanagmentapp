import { StockTransferDocumentOperationComponent } from './stock-transfer-document-operation/stock-transfer-document-operation.component';
import { StockTransferDocumentListComponent } from './stock-transfer-document-list/stock-transfer-document-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockTransferDocumentListComponent },
  { path: 'operation', component: StockTransferDocumentOperationComponent },
  { path: 'operation/:id', component: StockTransferDocumentOperationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockTransferDocumentRoutingModule { }
