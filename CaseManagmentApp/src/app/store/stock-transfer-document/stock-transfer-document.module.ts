import { SharedModule } from './../../shared/shared.module';
import { StockTransferDocumentOperationComponent } from './stock-transfer-document-operation/stock-transfer-document-operation.component';
import { StockTransferDocumentListComponent } from './stock-transfer-document-list/stock-transfer-document-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockTransferDocumentRoutingModule } from './stock-transfer-document-routing.module';
import { ProductModule } from '../product/product.module';
import { StockModule } from '../stock/stock.module';

@NgModule({
  declarations: [
    StockTransferDocumentListComponent,
    StockTransferDocumentOperationComponent,
  ],
  imports: [
    CommonModule,
    StockTransferDocumentRoutingModule,
    SharedModule.forRoot(),
    StockModule,
    ProductModule
  ]
})
export class StockTransferDocumentModule { }
