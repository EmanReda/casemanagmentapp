import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockTransferDocumentService {

  constructor(private http: HttpClient) { }

  GetAll(model): Observable<any> {
    return this.http.post('StockTransferDocument/GetAll', model);
  }

  getTransactionNo(date): Observable<any> {
    return this.http.get('StockTransferDocument/getTransactionNo?date=' + date);
  }

  getById(id: number): any {
    return this.http.get(`StockTransferDocument/GetById/${id}`);
  }

  save(model: any): Observable<any> {
    return this.http.post(`StockTransferDocument/Operation`, model);
  }

  voided(id): Observable<any> {
    return this.http.delete(`StockTransferDocument/Voided/${id}`);
  }

  PrintReport(id): any {
    return this.http.get('StockTransferDocument/Report/' + id);
  }

  loadBatchExpireList(ProductId, StockId, BarCode): Observable<any> {
    return this.http.get(`Invoice/loadBatchExpireList?ProductId=` + ProductId + '&StockId=' + StockId + '&BarCode=' + BarCode);
  }
}
