import { Component } from '@angular/core';
import { FilterStock } from './stock-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockService } from '../stock.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html'
})
export class StockListComponent {
  searchModel: FilterStock;
  total: number;
  stocks: any;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public stockSer: StockService,
    private translate: TranslateService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Stock.List', url: '/store/stock/list' }
      ],
      pageHeader: 'Stock.List'
    };
    this.searchModel = new FilterStock();
    this.getAll();
  }

  getAll() {
    this.stockSer.GetAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.stocks = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  delete(id) {
    this.stockSer.deleteStock(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }
}
