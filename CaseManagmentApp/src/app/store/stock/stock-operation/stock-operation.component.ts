import { Component, OnInit } from '@angular/core';
import { StockModel } from './stock-operation.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { ActivatedRoute } from '@angular/router';
import { StockService } from '../stock.service';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-stock-operation',
  templateUrl: './stock-operation.component.html'
})
export class StockOperationComponent implements OnInit {
  model: StockModel;
  sub: any;
  oldCode: any;
  checkCode = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    private stockSer: StockService,
    private location: Location,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Store', url: null },
        { name: 'Stock.List', url: '/store/stock/list' }
      ],
      pageHeader: ''
    };
    this.model = new StockModel();
  }

  ngOnInit() {
    this.sub = this.activatedRoute.params
      .subscribe(params => {
        const id = params.id;
        if (id == null) {
          this.fixed.subHeader.data.push({ name: 'Stock.Add', url: '/store/stock/operation' });
          this.fixed.subHeader.pageHeader = 'Stock.Add';
          this.model.stockId = null;
        } else {
          this.fixed.subHeader.data.push({ name: 'Stock.Edit', url: '/store/stock/operation/' + id });
          this.fixed.subHeader.pageHeader = 'Stock.Edit';
          this.model.stockId = Number(id);
          this.getStock(Number(id));
        }
      });
  }

  getStock(id: number) {
    this.stockSer.GetStockById(id)
      .subscribe((Response) => {
        this.model = Response;
        this.oldCode = this.model.code;
        this.moduleTreeChange({
          moduleTreeId: Response.stockModuleId,
          acC_ModuleAccount: Response.stockTreeList
        }, 'stock');
        this.moduleTreeChange({
          moduleTreeId: Response.costModuleId,
          acC_ModuleAccount: Response.costTreeList
        }, 'cost');
        this.moduleTreeChange({
          moduleTreeId: Response.salesModuleId,
          acC_ModuleAccount: Response.salesTreeList
        }, 'sales');
        this.moduleTreeChange({
          moduleTreeId: Response.returnSalesModuleId,
          acC_ModuleAccount: Response.returnSalesTreeList
        }, 'returnSales');
        this.moduleTreeChange({
          moduleTreeId: Response.purchasesModuleId,
          acC_ModuleAccount: Response.purchasesTreeList
        }, 'purchases');
        this.moduleTreeChange({
          moduleTreeId: Response.returnPurchasesModuleId,
          acC_ModuleAccount: Response.returnPurchasesTreeList
        }, 'returnPurchases');
        this.moduleTreeChange({
          moduleTreeId: Response.receptionModuleId,
          acC_ModuleAccount: Response.receptionTreeList
        }, 'reception');
        this.moduleTreeChange({
          moduleTreeId: Response.exitModuleId,
          acC_ModuleAccount: Response.exitTreeList
        }, 'exit');
        this.moduleTreeChange({
          moduleTreeId: Response.vatModuleId,
          acC_ModuleAccount: Response.vatTreeList
        }, 'vat');
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkStockCode(code) {
    if (!(code == null || code == '' || code == this.oldCode)) {
      this.stockSer.CheckStockCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, (error) => {
            this.global.notificationMessage(4);
          });
    }
  }

  moduleTreeChange(event, target: string, from?) {
    const account = target + 'AccountId';
    const module = target + 'ModuleId';
    const tree = target + 'TreeList';
    this.model[module] = (event == null) ? null : event.moduleTreeId;
    const list = [];
    if (event != null && event.acC_ModuleAccount != null) {
      event.acC_ModuleAccount.forEach(element => {
        list.push(element.accountTreeId);
      });
    }
    this.model[tree] = (event == null) ? undefined : list;
    if (from === 'html') {
      this.model[account] = null;
    }
  }

  saveStock() {
    this.stockSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteStock(id) {
    this.stockSer.deleteStock(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.location.back();
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }
}
