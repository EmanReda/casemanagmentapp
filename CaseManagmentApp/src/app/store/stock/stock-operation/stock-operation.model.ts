export class StockModel {
    constructor(
        public stockId: number = null,
        public code: string = null,
        public name: string = null,
        public companyId: number = null,
        public stockAccountId: number = null,
        public stockModuleId: number = null,
        public stockTreeList: any = null,
        public costAccountId: number = null,
        public costModuleId: number = null,
        public costTreeList: any = null,
        public salesAccountId: number = null,
        public salesModuleId: number = null,
        public salesTreeList: any = null,
        public returnSalesAccountId: number = null,
        public returnSalesModuleId: number = null,
        public returnSalesTreeList: any = null,
        public purchasesAccountId: number = null,
        public purchasesModuleId: number = null,
        public purchasesTreeList: any = null,
        public returnPurchasesAccountId: number = null,
        public returnPurchasesModuleId: number = null,
        public returnPurchasesTreeList: any = null,
        public receptionAccountId: number = null,
        public receptionModuleId: number = null,
        public receptionTreeList: any = null,
        public exitAccountId: number = null,
        public exitModuleId: number = null,
        public exitTreeList: any = null,
        public vatAccountId: number = null,
        public vatModuleId: number = null,
        public vatTreeList: any = null) {
    }
}
