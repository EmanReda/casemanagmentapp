import { Component, OnInit } from '@angular/core';
import { FilterStockProduct } from './stock-product-list.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { StockService } from '../stock.service';
import { BsLocaleService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-stock-product-list',
  templateUrl: './stock-product-list.component.html'
})

export class StockProductListComponent implements OnInit {
  searchModel: FilterStockProduct;
  total: number;
  stocks: any;
  sub: any;
  onlyStock = false;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public stockProdSer: StockService,
    private localeService: BsLocaleService,
    private activatedRoute: ActivatedRoute) {
    this.global.appLangChanged.subscribe(
      () => {
        this.localeService.use(this.fixed.activeLang.code);
      });
    this.searchModel = new FilterStockProduct();

  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params
      .subscribe(params => {
        const stockId = params.stockId;
        if (stockId != null && stockId > 0) {
          this.searchModel.stockId = Number(stockId);
          const stockName = params.stockName;
          this.onlyStock = true;
          this.fixed.subHeader = {
            display: true,
            data: [
              { name: 'Menu.Store', url: null },
              { name: 'StockProduct.Stocktaking', url: '/store/stock/product-list/' + stockId + '/' + stockName },
              { name: stockName, url: '/store/stock/product-list/' + stockId + '/' + stockName }
            ],
            pageHeader: 'StockProduct.Stocktaking'
          };
        } else {
          this.fixed.subHeader = {
            display: true,
            data: [
              { name: 'Menu.Store', url: null },
              { name: 'StockProduct.List', url: '/store/stock/product-list' }
            ],
            pageHeader: 'StockProduct.List'
          };
        }
        this.getAll();
      });
  }

  getAll() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    this.stockProdSer.GetAllStockProduct(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.stocks = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }
  resetSearch() {
    this.searchModel = new FilterStockProduct();
    this.total = undefined;
    this.stocks = undefined;
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  PrintReport() {
    this.searchModel.fromDate = this.global.formatDate(this.searchModel.fromDate);
    this.searchModel.toDate = this.global.formatDate(this.searchModel.toDate);
    if (this.onlyStock) {
      this.stockProdSer.PrintReportFromStock(this.searchModel)
        .subscribe((response) => {
          this.global.openReport(response);
        }, () => {
          this.global.notificationMessage(4);
        });
    } else {
      this.stockProdSer.PrintReport(this.searchModel)
        .subscribe((response) => {
          this.global.openReport(response);
        }, () => {
          this.global.notificationMessage(4);
        });
    }
  }
}
