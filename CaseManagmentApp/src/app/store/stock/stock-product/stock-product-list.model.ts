export class FilterStockProduct {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public stockId: number = null,
        public productId: number = null,
        public barcodeBatchNumber: string = null,
        public barcode: string = null,
        public batchNumber: string = null,
        public fromDate: any = null,
        public toDate: any = null,
        public qtyInUom: number = null) {
    }
}
