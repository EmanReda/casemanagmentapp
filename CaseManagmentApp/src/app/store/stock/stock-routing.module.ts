import { StockProductListComponent } from './stock-product/stock-product-list.component';
import { StockOperationComponent } from './stock-operation/stock-operation.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'list', component: StockListComponent },
  { path: 'operation', component: StockOperationComponent },
  { path: 'operation/:id', component: StockOperationComponent },
  { path: 'product-list', component: StockProductListComponent },
  { path: 'product-list/:stockId/:stockName', component: StockProductListComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }
