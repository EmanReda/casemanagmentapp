import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { StockService } from '../stock.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
    selector: 'app-stock-shared',
    templateUrl: './stock-shared.component.html'
})

export class StockSharedComponent {
    stocks: any;
    modalRef: BsModalRef;
    search: any;
    @Input() disable: boolean;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();
    @Input() filterStock: any;

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private stockSer: StockService,
        private modalService: BsModalService) {
        this.getAll();
    }

    getAll() {
        this.stockSer.GetSharedData()
            .subscribe((response) => {
                this.stocks = response;
                this.fixed.store.Stock = this.stocks;
            }, () => {
                this.global.notificationMessage(4);
            });
    }

    openModal(template: any) {
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
}
