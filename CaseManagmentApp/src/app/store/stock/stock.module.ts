import { CategoryModule } from './../category/category.module';
import { StoreSharedModule } from './../store-shared/store-shared.module';
import { ProductModule } from './../product/product.module';
import { SharedModule } from './../../shared/shared.module';
import { StockSharedComponent } from './stock-shared/stock-shared.component';
import { StockProductListComponent } from './stock-product/stock-product-list.component';
import { StockOperationComponent } from './stock-operation/stock-operation.component';
import { StockListComponent } from './stock-list/stock-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockRoutingModule } from './stock-routing.module';

@NgModule({
  declarations: [
    StockListComponent,
    StockOperationComponent,
    StockProductListComponent,
    StockSharedComponent
  ],
  imports: [
    CommonModule,
    StockRoutingModule,
    SharedModule.forRoot(),
    StoreSharedModule,
    ProductModule,
    CategoryModule
  ], 
  exports: [
    StockSharedComponent
  ]
})
export class StockModule { }
