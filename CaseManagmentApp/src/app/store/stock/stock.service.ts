import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class StockService {

  constructor(private http: HttpClient) { }

  GetAll(model): Observable<any> {
    return this.http.post('Stock/GetAll', model);
  }

  GetAllStockProduct(model): Observable<any> {
    return this.http.post('Stock/GetAllStockProduct', model);
  }

  GetSharedData(): Observable<any> {
    return this.http.get('Stock/GetSharedData');
  }

  GetStockById(id: number): Observable<any> {
    return this.http.get(`Stock/GetStockById/${id}`);
  }

  CheckStockCode(Code): Observable<any> {
    return this.http.get('Stock/CheckStockCode?Code=' + Code);
  }

  Save(model): Observable<any> {
    return this.http.post('Stock/Operation', model);
  }

  deleteStock(id): Observable<any> {
    return this.http.delete(`Stock/Delete/${id}`);
  }

  PrintReport(model): any {
    return this.http.post('Stock/Report', model);
  }
  
  PrintReportFromStock(model): any {
    return this.http.post('Stock/ReportFromStock', model);
  }
}
