import { StoreComponent } from './store.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    component: StoreComponent,
    children : [
      {
        path: 'i',
        loadChildren: './invoice/invoice.module#InvoiceModule'
      },
      {
        path: 'stock',
        loadChildren: './stock/stock.module#StockModule'
      },
      {
        path: 'stock-in-document',
        loadChildren: './stock-in-document/stock-in-document.module#StockInDocumentModule'
      },
      {
        path: 'stock-opening',
        loadChildren: './stock-opening/stock-opening.module#StockOpeningModule'
      },
      {
        path: 'stock-out-document',
        loadChildren: './stock-out-document/stock-out-document.module#StockOutDocumentModule'
      },
      {
        path: 'stock-receive-voucher',
        loadChildren: './stock-receive-voucher/stock-receive-voucher.module#StockReceiveVoucherModule'
      },
      {
        path: 'stock-return-voucher',
        loadChildren: './stock-return-voucher/stock-return-voucher.module#StockReturnVoucherModule'
      },
      {
        path: 'stock-transfer-document',
        loadChildren: './stock-transfer-document/stock-transfer-document.module#StockTransferDocumentModule'
      },
      {
        path: 'return',
        loadChildren: './return/return.module#ReturnModule'
      },
      {
        path: 'receivable',
        loadChildren: './receivable/receivable.module#ReceivableModule'
      },
      {
        path: 'product',
        loadChildren: './product/product.module#ProductModule'
      },
      {
        path: 'payable',
        loadChildren: './payable/payable.module#PayableModule'
      },
      {
        path: 'incentive',
        loadChildren: './incentive/incentive.module#IncentiveModule'
      },
      {
        path: 'category',
        loadChildren: './category/category.module#CategoryModule'
      },
      {
        path: 'payment-method',
        loadChildren: './payment-method/payment-method.module#PaymentMethodModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
