import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Country } from './country.model';
import { CountryService } from './country.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html'
})
export class CountryComponent  {
    countryOperation: boolean;
    Countries: any;
    modalRef: BsModalRef;
    search: any;
    model: Country;
    checkcode:any;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();
  
    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private countrySer: CountryService,
        private modalService: BsModalService) {
        this.getAll();
    }
  
    getAll(){
        this.countrySer.getAll().subscribe((data)=>{
            this.Countries = data;
        },()=>{
        this.global.notificationMessage(4);
        });
    }
    checkCode(){
        if(!(this.model.code == null || this.model.code =='')){
            this.countrySer.checkCode(this.model)
            .subscribe((data)=>{
                this.checkcode = data;
            },
            ()=>{
              this.global.notificationMessage(4);
            });
    }}
    openModal(template: any) {
        this.countryOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
    
    operation(data?) {
        this.model = (data != null) 
        ? new Country(data.countryId, data.name,data.code, data.isDeleted) 
        :new Country();
        this.countryOperation = true;
    }

    saveCountry() {
        this.countrySer.operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.countryOperation = false;
                        this.model = new Country();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.countrySer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.countryOperation = false;
                        this.model = new Country();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

  
}