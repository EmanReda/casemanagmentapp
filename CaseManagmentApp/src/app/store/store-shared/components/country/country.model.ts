export class Country{
    constructor(
        public countryId:number=null,
        public name:string =null,
        public code:string=null,
        public isDeleted:boolean=null
    ){}
}