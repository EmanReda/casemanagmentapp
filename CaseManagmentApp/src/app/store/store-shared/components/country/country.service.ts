import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class CountryService {
    constructor(private http:HttpClient){};
getAll():Observable<any>{
  return  this.http.get('Country/GetAll');
}
operation(model):Observable<any>{
    return this.http.post('Country/Operation',model);
}
delete(id:number):Observable<any>{
    return this.http.delete('Country/Delete/'+id);
}
checkCode(model):any{
    return this.http.post('Country/CheckCode',model);
}
}