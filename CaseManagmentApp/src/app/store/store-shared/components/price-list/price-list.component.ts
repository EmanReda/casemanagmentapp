import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PriceList } from './price-list.model';
import { PriceListService } from './price-list.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
    selector: 'app-price-list',
    templateUrl: './price-list.component.html'
})
export class PriceListComponent {
    priceOperation: boolean;
    modalRef: BsModalRef;
    search: any;
    model: PriceList;
    checkcode: any;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private priceListSer: PriceListService,
        private modalService: BsModalService) {
        this.priceListSer.getAll();
    }

    openModal(template: any) {
        this.priceOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }

    operation(data?) {
        this.model = (data != null)
            ? new PriceList(data.priceListId, data.code, data.name, data.isDeleted)
            : new PriceList();
        this.priceOperation = true;
    }
    checkCode() {
        if (!(this.model.code == null || this.model.code == '')) {
            this.priceListSer.checkCode(this.model)
                .subscribe((data) => {
                    this.checkcode = data;
                },
                    () => {
                        this.global.notificationMessage(4);
                    });
        }
    }
    saveUnit() {
        this.priceListSer.operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.priceOperation = false;
                        this.model = new PriceList();
                        this.fixed.store.PriceList = [];
                        this.priceListSer.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.priceListSer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.priceOperation = false;
                        this.model = new PriceList();
                        this.fixed.store.PriceList = [];
                        this.priceListSer.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }
}
