export class PriceList {
    constructor(
        public priceListId: number = null,
        public code: string = null,
        public name: string = null,
        public isDeleted: boolean = null) { }
}
