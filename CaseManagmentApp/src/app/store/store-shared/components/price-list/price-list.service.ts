import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalService } from 'src/app/shared/services/global.service';
import { FixedService } from 'src/app/shared/services/fixed.service';


@Injectable({ providedIn: 'root' })
export class PriceListService {
    constructor(
        private http: HttpClient,
        public global: GlobalService,
        public fixed: FixedService) { }

    getAll() {
        if (this.fixed.store.PriceList.length === 0) {
            this.http.get('PriceList/GetAll')
                .subscribe((data) => {
                    this.fixed.store.PriceList = data;
                }, () => {
                    this.global.notificationMessage(4);
                });
        }
    }

    operation(model): Observable<any> {
        return this.http.post('PriceList/Operation', model);
    }

    delete(id: number): Observable<any> {
        return this.http.delete('PriceList/Delete/' + id);
    }

    checkCode(model): any {
        return this.http.post('PriceList/CheckCode', model);
    }
}
