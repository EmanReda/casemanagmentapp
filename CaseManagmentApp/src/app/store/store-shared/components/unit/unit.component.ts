import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Unit } from './unit.model';
import { UnitService } from './unit.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
    selector: 'app-unit',
    templateUrl: './unit.component.html'
})
export class UnitComponent {
    unitOperation: boolean;
    Units: any;
    modalRef: BsModalRef;
    search: any;
    model: Unit;
    checkcode: any;
    @Input() selectedValue: number;
    @Output() selectedValueChanges = new EventEmitter<any>();

    constructor(
        public global: GlobalService,
        public fixed: FixedService,
        private translate: TranslateService,
        private unitSer: UnitService,
        private modalService: BsModalService) {
        this.getAll();
    }

    getAll() {
        this.unitSer.getAll().subscribe((data) => {
            this.Units = data;
        }, () => {
            this.global.notificationMessage(4);
        });
    }

    openModal(template: any) {
        this.unitOperation = false;
        this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }

    operation(data?) {
        this.model = (data != null)
            ? new Unit(data.unitId, data.code, data.name, data.isDeleted)
            : new Unit();
        this.unitOperation = true;
    }
    checkCode() {
        if (!(this.model.code == null || this.model.code == '')) {
            this.unitSer.checkCode(this.model)
                .subscribe((data) => {
                    this.checkcode = data;
                },
                    () => {
                        this.global.notificationMessage(4);
                    });
        }
    }
    saveUnit() {
        this.unitSer.operation(this.model)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.unitOperation = false;
                        this.model = new Unit();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }

    delete(id) {
        this.unitSer.delete(id)
            .subscribe(
                (response) => {
                    if (response.type === ResponseEnum.Success) {
                        this.unitOperation = false;
                        this.model = new Unit();
                        this.getAll();
                    }
                    if (response.name == null) {
                        this.global.notificationMessage(response.type);
                    } else {
                        this.translate.get(response.name)
                            .subscribe(
                                (val) => {
                                    this.global.notificationMessage(response.type, null, val);
                                });
                    }
                }, (error) => {
                    this.global.notificationMessage(4);
                });
    }
}
