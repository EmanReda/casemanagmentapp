export class Unit{
    constructor(
        public unitId:number=null,
        public code:string =null,
        public name:string =null,
        public isDeleted:boolean=null
    ){}
}