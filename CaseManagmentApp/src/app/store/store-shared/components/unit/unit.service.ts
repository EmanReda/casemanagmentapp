import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UnitService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<any> {
        return this.http.get('Unit/GetAll');
    }

    operation(model): Observable<any> {
        return this.http.post('Unit/Operation', model);
    }

    delete(id: number): Observable<any> {
        return this.http.delete('Unit/Delete/' + id);
    }

    checkCode(model): any {
        return this.http.post('Unit/CheckCode', model);
    }
}
