import { Component,Output,Input ,Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { UOMModel } from './uom.model';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'app-uom',
  templateUrl: './uom.component.html'
})
export class UOMComponent  {
  list: any;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private http:HttpClient) {
      this.getAll();
  }

  getAll() {
     this.http.get('Unit/GetAllUOM') 
     .subscribe((response) => {
        this.list = response;
        this.fixed.store.UOM = this.list;
    }, () => {
        this.global.notificationMessage(4);
    });
  }
}
