export class UOMModel {
    constructor(
      public uomId: number = null,
      public nameEn: string = null,
      public nameAr: string = null
    ) { }
  }
  