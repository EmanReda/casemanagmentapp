import { SharedModule } from './../../shared/shared.module';

import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnitComponent } from './components/unit/unit.component';
import { PriceListComponent } from './components/price-list/price-list.component';
import { UOMComponent } from './components/uom/uom.component';
import { CountryComponent } from './components/country/country.component';

@NgModule({
  declarations: [
    UnitComponent,
    PriceListComponent,
    UOMComponent,
    CountryComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    UnitComponent,
    PriceListComponent,
    UOMComponent,
    CountryComponent
  ]
})
export class StoreSharedModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StoreSharedModule
    };
  }
}
