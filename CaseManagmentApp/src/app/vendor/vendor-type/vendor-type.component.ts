import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { VendorTypeModel } from './vendor-type.model';
import { VendorTypeService } from './vendor-type.service';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { TranslateService } from '@ngx-translate/core';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';

@Component({
  selector: 'app-vendor-type',
  templateUrl: './vendor-type.component.html'
})
export class VendorTypeComponent  {
  vendorOperation: boolean;
  vendorTypes: any;
  modalRef: BsModalRef;
  search: any;
  model: VendorTypeModel;
  @Input() selectedValue: number;
  @Output() selectedValueChanges = new EventEmitter<any>();

  constructor(
      public global: GlobalService,
      public fixed: FixedService,
      private translate: TranslateService,
      private vendorTypeSer: VendorTypeService,
      private modalService: BsModalService) {
      this.getAll();
  }

  getAll() {
      this.vendorTypeSer.GetAll()
          .subscribe((response) => {
              this.vendorTypes = response;
          }, () => {
              this.global.notificationMessage(4);
          });
  }

  openModal(template: any) {
      this.vendorOperation = false;
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  saveVendor() {
      this.vendorTypeSer.Operation(this.model)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.vendorOperation = false;
                      this.model = new VendorTypeModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  delete(id) {
      this.vendorTypeSer.delete(id)
          .subscribe(
              (response) => {
                  if (response.type === ResponseEnum.Success) {
                      this.vendorOperation = false;
                      this.model = new VendorTypeModel();
                      this.getAll();
                  }
                  if (response.name == null) {
                      this.global.notificationMessage(response.type);
                  } else {
                      this.translate.get(response.name)
                          .subscribe(
                              (val) => {
                                  this.global.notificationMessage(response.type, null, val);
                              });
                  }
              }, (error) => {
                  this.global.notificationMessage(4);
              });
  }

  operation(data?) {
      this.model = (data != null) 
      ? new VendorTypeModel(data.vendorTypeId, data.name, data.notes, data.isDeleted) 
      :new VendorTypeModel();
      this.vendorOperation = true;
  }
}
