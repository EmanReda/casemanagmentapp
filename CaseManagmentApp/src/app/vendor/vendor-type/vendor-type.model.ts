export class VendorTypeModel {
    constructor(
      public vendorTypeId: number = null,
      public name: string = null,
      public notes: string = null,
      public isDeleted: boolean = null
    ) { }
  }
  