import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class VendorTypeService {

    constructor(private http: HttpClient) {}
  
    GetAll(): Observable<any> {
      return this.http.get('VendorType/GetAll');
    }
  
    Operation(data): Observable<any> {
      return this.http.post('VendorType/Operation' , data);
    }
  
    delete(id: number): Observable<any> {
      return this.http.delete('VendorType/delete/' + id);
    }
  
  }
  

