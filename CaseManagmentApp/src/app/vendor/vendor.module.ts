import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorRoutingModule } from './vendor-routing.module';
import { SharedModule } from '../shared/shared.module';
import { VendorTypeComponent } from './vendor-type/vendor-type.component';
import { VendorComponent } from './vendor/vendor.component';
@NgModule({
  declarations: [
    VendorTypeComponent,
    VendorComponent
  ],
  imports: [
    CommonModule,
    VendorRoutingModule,
    SharedModule.forRoot(),
  ]
})
export class VendorModule { }
