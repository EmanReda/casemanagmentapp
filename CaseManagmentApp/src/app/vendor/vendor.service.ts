import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class VendorService {

    constructor(private http: HttpClient) { }
  
    CheckVendorCode(Code): any {
      return this.http.get('Vendor/CheckVendorCode?Code=' + Code);
    }
  
    getAll(model): Observable<any> {
      return this.http.post(`Vendor/GetAll`, model);
    }
    Save(model): any {
      return this.http.post('Vendor/Operation', model);
    }
  
    Delete(id): any {
      return this.http.delete('Vendor/Delete/' + id);
    }
    Report(model):any{
      return this.http.post('Vendor/ExportReport',model);
    }
  }
  