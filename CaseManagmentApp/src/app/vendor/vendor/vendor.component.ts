import { Component, TemplateRef } from '@angular/core';
import { FixedService } from 'src/app/shared/services/fixed.service';
import { GlobalService } from 'src/app/shared/services/global.service';
import { VendorService } from '../vendor.service';
import { VendorModel, FilterVendor } from './vendor.model';
import { ResponseEnum } from 'src/app/shared/enums/response.enum';
import { TranslateService } from '@ngx-translate/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { TreeType } from 'src/app/shared/enums/tree-type.enum';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html'
})
export class VendorComponent {
  searchModel: FilterVendor;
  treeType: any;
  model: VendorModel;
  checkCode = false;
  vendors: any;
  modalRef: BsModalRef;
  total: number;

  constructor(
    public fixed: FixedService,
    public global: GlobalService,
    public vendorSer: VendorService,
    private translate: TranslateService,
    private modalService: BsModalService) {
    this.fixed.subHeader = {
      display: true,
      data: [
        { name: 'Menu.Vendor', url: null },
        { name: 'Vendor.PageHeader', url: '/vendor/list' }
      ],
      pageHeader: 'Vendor.PageHeader'
    };
    this.searchModel = new FilterVendor();
    this.getAll();
    this.treeType = TreeType;
  }

  openModal(template: TemplateRef<any>, item: VendorModel) {
    this.model = new VendorModel();
    if (item != null) {
      this.model = new VendorModel(item.vendorId, item.moduleTreeId, item.vendorTypeId, item.code, item.name, item.notes);
    }
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }

  getAll() {
    this.vendorSer.getAll(this.searchModel)
      .subscribe((response) => {
        this.total = response.total;
        this.vendors = response.data;
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  checkVendorCode(code) {
    if (!(code == null || code == '')) {
      this.vendorSer.CheckVendorCode(code)
        .subscribe(
          (data) => {
            this.checkCode = data;
          }, () => {
            this.global.notificationMessage(4);
          });
    }
  }

  saveVendor(): void {
    this.vendorSer.Save(this.model)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  deleteVendor(id) {
    this.vendorSer.Delete(id)
      .subscribe((response) => {
        if (response.type === ResponseEnum.Success) {
          this.getAll();
          if (this.modalRef != null) { this.modalRef.hide(); }
        }
        if (response.name == null) {
          this.global.notificationMessage(response.type);
        } else {
          this.translate.get(response.name)
            .subscribe(
              (val) => {
                this.global.notificationMessage(response.type, null, val);
              });
        }
      }, () => {
        this.global.notificationMessage(4);
      });
  }

  changePagination(data) {
    this.searchModel.recordPerPage = data.recordPerPage;
    this.searchModel.page = data.currentPage;
    this.getAll();
  }

  exportReport() {
    this.vendorSer.Report(this.searchModel)
      .subscribe((response) => {
        // const url = environment.reportUrl + JSON.stringify(response);
        // window.open(url);
        this.global.notificationMessage(1);
      }, (error) => {
        this.global.notificationMessage(4);
      });
  }
}
