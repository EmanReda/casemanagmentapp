export class VendorModel {
    constructor(
        public vendorId: number = null,
        public moduleTreeId: number = null,
        public vendorTypeId: number = null,
        public code: string = null,
        public name: string = null,
        public notes: string = null) {
    }
}

export class FilterVendor {
    constructor(
        public page: number = 1,
        public recordPerPage: number = 20,
        public name: any = null,
        public vendorTypeId: number = 0,
        public moduleTreeId: number = 0) {
    }
}