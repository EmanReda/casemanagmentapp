﻿using OnServer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace OnServer.BusinessLayer
{
    public class ActionTakenBLL
    {
        public List<ActionTaken> GetAll()
        {
            using (OnEntities db = new OnEntities())
            {
                return db.ActionTakens.OrderByDescending(p => p.id).ToList();
            }
        }
        public string Operation(ActionTaken model)
        {
            using (OnEntities db = new OnEntities())
            {
                ActionTaken old = db.ActionTakens.Where(p => p.id == model.id).FirstOrDefault();
                if (old != null)
                {
                    old.NameAr = model.NameAr;
                    old.NameEn = model.NameEn;
                }
                db.ActionTakens.AddOrUpdate(model);

                try
                {
                    db.SaveChanges();
                    return "Operation has been Done ";
                }
                catch (Exception e)
                {
                    return " An error in Operation";
                }
            }
        }
        public string Delete(int id)
        {
            using (OnEntities db = new OnEntities())
            {
                ActionTaken model = db.ActionTakens.Where(p => p.id == id).FirstOrDefault();
               
                db.ActionTakens.AddOrUpdate(model);
                try
                {
                    db.SaveChanges();
                    return "Record Has been deleted";
                }
                catch (Exception)
                {
                    return "an error occurred will deletion ";
                }
            }
        }
    }

}