﻿using OnServer.BusinessLayer;
using OnServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnServer.Controllers
{
    public class ActionTakenController : ApiController
    {
        ActionTakenBLL obj;
        public ActionTakenController()
        {
            obj = new ActionTakenBLL();
        }
        [HttpGet]
        public IHttpActionResult GetAll() => Ok(obj.GetAll());
        [HttpPost]
        public IHttpActionResult Operation(ActionTaken model)
           => Ok(obj.Operation(model));
        [HttpDelete]
        public IHttpActionResult Delete(int id)
            => Ok(obj.Delete(id));
    }
}
